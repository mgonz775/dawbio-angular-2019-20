import { Component, OnInit } from '@angular/core';

import { GenomeService } from '../services/genome.service'
import { Genome } from '../model/genome';
import { Kingdom } from '../model/kingdom';
import { Phylo } from '../model/phylo';
import { debug } from 'util';

@Component({
  selector: 'app-genome-management',
  templateUrl: './genome-management.component.html',
  styleUrls: ['./genome-management.component.css']
})
export class genomeManagementComponent implements OnInit {
  genomes: Genome[] = [];
  genomesFiltered: Genome[]=[];
  genomesSelected: Genome[]=[];
  genomeSelected: Genome;

  kingdoms: Kingdom[] = [];
  phylos: Phylo[] = [];

   //Filter properties
   sizeFilter: number = 991585;
   phyloFilter: Phylo;
   kingdomFilter: Kingdom[] = [];
   startDateFilter: Date;
   endDateFilter: Date;
   
   // Pagination properties
   currentPage: number = 0;
   itemsPerPage: number = 10;
 
   checkStatus: boolean = false;
   
  constructor(private genomeService: GenomeService) {
  }

  ngOnInit() {

    this.genomes = this.genomeService.generateGenomeRandom();    
    this.genomesFiltered  = Object.assign([], this.genomes);

    this.phylos = this.genomeService.phylos;
    this.kingdoms = this.genomeService.kingdoms;

    this.startDateFilter = new Date("01/01/1996");
    this.endDateFilter = new Date("02/03/2020");
  }

  filter(): void{
    this.genomesFiltered = this.genomes.
      filter(genome => {
        let sizeFilterValid = false;
        let phyloFilterValid = false;
        let startDateFilterValid = false;
        let endDateFilterValid = false;
       
        sizeFilterValid = Number(genome.size) <=
          Number(this.sizeFilter);
        
        if(!this.phyloFilter){
          phyloFilterValid = true;
        }else{
          phyloFilterValid = (genome.phylo.id == this.phyloFilter.id); 
        }

        startDateFilterValid = genome.releaseDate >= new Date(this.startDateFilter);
        endDateFilterValid = genome.releaseDate <= new Date(this.endDateFilter);
       
        return sizeFilterValid && phyloFilterValid && startDateFilterValid && endDateFilterValid;
      });
     
  }

  delete(genome: Genome){
    this.genomes.splice(this.genomes.indexOf(genome),1);
    this.genomesFiltered.splice(this.genomesFiltered.indexOf(genome),1);  
  }

  changeCheckStatus(){
    this.checkStatus = !this.checkStatus;

    if(this.checkStatus){
      this.genomesSelected = Object.assign([], this.genomesFiltered);
    }else{
      this.genomesSelected = [];
    }
  }

  deleteAll(){    
    debugger;
    for (let genome of this.genomesSelected){
      this.genomes.splice(this.genomes.indexOf(genome),1);
      this.genomesFiltered.splice(this.genomesFiltered.indexOf(genome),1);  
    }

    this.genomesSelected=[];
      
    this.checkStatus = false;
    
  }
}
