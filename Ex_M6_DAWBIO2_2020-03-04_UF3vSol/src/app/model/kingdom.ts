export class Kingdom{
    private _id: number;
    private _kingdom: string;

	constructor(id: number, kingdom: string) {
		this._id = id;
		this._kingdom = kingdom;
	}
   

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter kingdom
     * @return {string}
     */
	public get kingdom(): string {
		return this._kingdom;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter kingdom
     * @param {string} value
     */
	public set kingdom(value: string) {
		this._kingdom = value;
	}

}