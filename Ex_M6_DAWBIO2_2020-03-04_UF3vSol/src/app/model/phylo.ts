export class Phylo{
    private _id: number;
    private _phylo: string;
    

	constructor(id: number, phylo: string) {
		this._id = id;
		this._phylo = phylo;
	}


    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter phylo
     * @return {string}
     */
	public get phylo(): string {
		return this._phylo;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter phylo
     * @param {string} value
     */
	public set phylo(value: string) {
		this._phylo = value;
	}


}