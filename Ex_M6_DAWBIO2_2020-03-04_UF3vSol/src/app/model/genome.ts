import { Kingdom } from './kingdom'
import { Phylo } from '../model/phylo'

export class Genome {
	private _id: number;
	private _organism: string;
    private _kingdom: Kingdom;
    private _phylo: Phylo;
    private _size: number;
    private _releaseDate: Date;
 



	constructor(id: number, organism: string, kingdom: Kingdom, phylo: Phylo, size: number, releaseDate: Date) {
		this._id = id;
		this._organism = organism;
		this._kingdom = kingdom;
		this._phylo = phylo;
		this._size = size;
		this._releaseDate = releaseDate;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter organism
     * @return {string}
     */
	public get organism(): string {
		return this._organism;
	}

    /**
     * Getter kingdom
     * @return {Kingdom}
     */
	public get kingdom(): Kingdom {
		return this._kingdom;
	}

    /**
     * Getter phylo
     * @return {Phylo}
     */
	public get phylo(): Phylo {
		return this._phylo;
	}

    /**
     * Getter size
     * @return {number}
     */
	public get size(): number {
		return this._size;
	}

    /**
     * Getter releaseDate
     * @return {Date}
     */
	public get releaseDate(): Date {
		return this._releaseDate;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter organism
     * @param {string} value
     */
	public set organism(value: string) {
		this._organism = value;
	}

    /**
     * Setter kingdom
     * @param {Kingdom} value
     */
	public set kingdom(value: Kingdom) {
		this._kingdom = value;
	}

    /**
     * Setter phylo
     * @param {Phylo} value
     */
	public set phylo(value: Phylo) {
		this._phylo = value;
	}

    /**
     * Setter size
     * @param {number} value
     */
	public set size(value: number) {
		this._size = value;
	}

    /**
     * Setter releaseDate
     * @param {Date} value
     */
	public set releaseDate(value: Date) {
		this._releaseDate = value;
	}


}    