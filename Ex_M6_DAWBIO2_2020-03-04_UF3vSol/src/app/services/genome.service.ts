import { Genome } from '../model/genome';
import { Kingdom } from '../model/kingdom';
import { Phylo } from '../model/phylo';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenomeService {

  kingdoms: Kingdom[]=[];
  phylos: Phylo[]=[];

  arrTaxonomy: any[] = [
    {
      Organism: "'Brassica napus' phytoplasma",
      Phylo: 9,
      Size: 0.743598
    },
    {
      Organism: "'Candidatus Kapabacteria' thiocyanatum",
      Phylo: 2,
      Size: 3.27299
    },
    {
      Organism: "'Catharanthus roseus' aster yellows phytoplasma",
      Phylo: 9,
      Size: 0.603949
    },
    {
      Organism: "'Chrysanthemum coronarium' phytoplasma",
      Phylo: 9,
      Size: 0.739592
    },
    {
      Organism: "'Cynodon dactylon' phytoplasma",
      Phylo: 9,
      Size: 0.483935
    },
    {
      Organism: "'Echinacea purpurea' witches'-broom phytoplasma",
      Phylo: 9,
      Size: 0.545427
    },
    {
      Organism: "'Osedax' symbiont bacterium Rs2_46_30_T18",
      Phylo: 11,
      Size: 4.02183
    },
    {
      Organism: "'Saccharum officinarum' phytoplasma SCGS",
      Phylo: 9,
      Size: 0.505173
    },
    {
      Organism: "'Sphingomonas ginsengisoli' Hoang et al. 2012",
      Phylo: 6,
      Size: 3.03639
    },
    {
      Organism: "ANME-1 cluster archaeon AG-394-G06",
      Phylo: 1,
      Size: 1.5313
    },
    {
      Organism: "ANME-1 cluster archaeon AG-394-G21",
      Phylo: 1,
      Size: 1.86817
    },
    {
      Organism: "ANME-1 cluster archaeon ex4572_4",
      Phylo: 1,
      Size: 1.01808
    },
    {
      Organism: "ANME-2 cluster archaeon",
      Phylo: 1,
      Size: 3.60934
    },
    {
      Organism: "ANME-2 cluster archaeon HR1",
      Phylo: 1,
      Size: 2.19546
    },
    {
      Organism: "ANMV-1 virus",
      Phylo: 10,
      Size: 0.038465
    },
    {
      Organism: "Aaosphaeria arxii",
      Phylo: 3,
      Size: 38901
    },
    {
      Organism: "Abaca bunchy top virus",
      Phylo: 4,
      Size: 0.006422
    },
    {
      Organism: "Abalone herpesvirus Victoria/AUS/2009",
      Phylo: 4,
      Size: 0.211518
    },
    {
      Organism: "Abalone shriveling syndrome-associated virus",
      Phylo: 4,
      Size: 0.034952
    },
    {
      Organism: "Abditibacterium utsteinense",
      Phylo: 9,
      Size: 3.60633
    },
    {
      Organism: "Abelson murine leukemia virus",
      Phylo: 4,
      Size: 0.005894
    },
    {
      Organism: "Abeoforma whisleri",
      Phylo: 7,
      Size: 101554
    },
    {
      Organism: "Abiotrophia",
      Phylo: 9,
      Size: 2.02078
    },
    {
      Organism: "Abiotrophia defectiva",
      Phylo: 9,
      Size: 2.04344
    },
    {
      Organism: "Abisko virus",
      Phylo: 4,
      Size: 0.010187
    },
    {
      Organism: "Abrus precatorius",
      Phylo: 5,
      Size: 347.23
    },
    {
      Organism: "Absidia glauca",
      Phylo: 3,
      Size: 48.7464
    },
    {
      Organism: "Absidia repens",
      Phylo: 3,
      Size: 47.4229
    },
    {
      Organism: "Absiella",
      Phylo: 9,
      Size: 4.73933
    },
    {
      Organism: "Absiella argi",
      Phylo: 9,
      Size: 2.9963
    },
    {
      Organism: "Absiella dolichum",
      Phylo: 9,
      Size: 2.19105
    },
    {
      Organism: "Abutilon Brazil virus",
      Phylo: 4,
      Size: 0.005271
    },
    {
      Organism: "Abutilon golden mosaic virus",
      Phylo: 4,
      Size: 0.002629
    },
    {
      Organism: "Abutilon mosaic Bolivia virus",
      Phylo: 4,
      Size: 0.005399
    },
    {
      Organism: "Abutilon mosaic Brazil virus",
      Phylo: 4,
      Size: 0.005282
    },
    {
      Organism: "Abutilon mosaic virus",
      Phylo: 4,
      Size: 0.005217
    },
    {
      Organism: "Abutilon yellows virus",
      Phylo: 4,
      Size: 0.001311
    },
    {
      Organism: "Abyssibacter profundi",
      Phylo: 6,
      Size: 3.74151
    },
    {
      Organism: "Abyssicoccus albus",
      Phylo: 9,
      Size: 1.79618
    },
    {
      Organism: "Abyssisolibacter fermentans",
      Phylo: 9,
      Size: 4.78636
    },
    {
      Organism: "Acanthamoeba astronyxis",
      Phylo: 7,
      Size: 83.4325
    },
    {
      Organism: "Acanthamoeba castellanii",
      Phylo: 7,
      Size: 42.0198
    },
    {
      Organism: "Acanthamoeba castellanii mamavirus",
      Phylo: 4,
      Size: 1.19169
    },
    {
      Organism: "Acanthamoeba castellanii mimivirus",
      Phylo: 4,
      Size: 1.18285
    },
    {
      Organism: "Acanthamoeba comandoni",
      Phylo: 7,
      Size: 86.2147
    },
    {
      Organism: "Acanthamoeba culbertsoni",
      Phylo: 7,
      Size: 55.5438
    },
    {
      Organism: "Acanthamoeba divionensis",
      Phylo: 7,
      Size: 84.7694
    },
    {
      Organism: "Acanthamoeba healyi",
      Phylo: 7,
      Size: 75.3182
    },
    {
      Organism: "Acanthamoeba lenticulata",
      Phylo: 7,
      Size: 75.5694
    },
    {
      Organism: "Acanthamoeba lugdunensis",
      Phylo: 7,
      Size: 99.4171
    },
    {
      Organism: "Acanthamoeba mauritaniensis",
      Phylo: 7,
      Size: 106836
    },
    {
      Organism: "Acanthamoeba palestinensis",
      Phylo: 7,
      Size: 103483
    },
    {
      Organism: "Acanthamoeba pearcei",
      Phylo: 7,
      Size: 115614
    },
    {
      Organism: "Acanthamoeba polyphaga",
      Phylo: 7,
      Size: 49.3537
    },
    {
      Organism: "Acanthamoeba polyphaga lentillevirus",
      Phylo: 4,
      Size: 1.19343
    },
    {
      Organism: "Acanthamoeba polyphaga mimivirus",
      Phylo: 4,
      Size: 1.25866
    },
    {
      Organism: "Acanthamoeba polyphaga moumouvirus",
      Phylo: 4,
      Size: 1.02135
    },
    {
      Organism: "Acanthamoeba quina",
      Phylo: 7,
      Size: 83589
    },
    {
      Organism: "Acanthamoeba rhysodes",
      Phylo: 7,
      Size: 75.8213
    },
    {
      Organism: "Acanthamoeba royreba",
      Phylo: 7,
      Size: 79.5441
    },
    {
      Organism: "Acanthaster planci",
      Phylo: 0,
      Size: 383.86
    },
    {
      Organism: "Acanthisitta chloris",
      Phylo: 0,
      Size: 1035.88
    },
    {
      Organism: "Acanthochaenus luetkenii",
      Phylo: 0,
      Size: 545759
    },
    {
      Organism: "Acanthocheilonema viteae",
      Phylo: 0,
      Size: 77.3509
    },
    {
      Organism: "Acanthochromis polyacanthus",
      Phylo: 0,
      Size: 991585
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus Br0604L",
      Phylo: 4,
      Size: 0.295545
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus Can0610SP",
      Phylo: 4,
      Size: 0.306752
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus Canal-1",
      Phylo: 4,
      Size: 0.293004
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus GM0701.1",
      Phylo: 4,
      Size: 0.315239
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus MN0810.1",
      Phylo: 4,
      Size: 0.327406
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus MO0605SPH",
      Phylo: 4,
      Size: 0.289449
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus NE-JV-2",
      Phylo: 4,
      Size: 0.319583
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus NE-JV-3",
      Phylo: 4,
      Size: 0.298622
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus NTS-1",
      Phylo: 4,
      Size: 0.323517
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus OR0704.3",
      Phylo: 4,
      Size: 0.311647
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus TN603.4.2",
      Phylo: 4,
      Size: 0.32088
    },
    {
      Organism: "Acanthocystis turfacea Chlorella virus WI0606",
      Phylo: 4,
      Size: 0.289373
    },
    {
      Organism: "Acanthocystis turfacea chlorella virus 1",
      Phylo: 4,
      Size: 0.288047
    },
    {
      Organism: "Acanthoscurria geniculata",
      Phylo: 0,
      Size: 7178.4
    },
    {
      Organism: "Acaricomes phytoseiuli",
      Phylo: 9,
      Size: 2.41952
    },
    {
      Organism: "Acaromyces ingoldii",
      Phylo: 3,
      Size: 19.3301
    },
    {
      Organism: "Acartia tonsa",
      Phylo: 0,
      Size: 989164
    },
    {
      Organism: "Acartia tonsa copepod circovirus",
      Phylo: 4,
      Size: 0.00167
    },
    {
      Organism: "Acaryochloris",
      Phylo: 9,
      Size: 7.87548
    },
    {
      Organism: "Acaryochloris marina",
      Phylo: 9,
      Size: 8.3616
    },
    {
      Organism: "Acaryochloris phage A-HIS1",
      Phylo: 4,
      Size: 0.055653
    },
    {
      Organism: "Acaryochloris phage A-HIS2",
      Phylo: 4,
      Size: 0.057391
    },
    {
      Organism: "Acaulopage tetraceros",
      Phylo: 3,
      Size: 11.1768
    },
    {
      Organism: "Accipiter nisus",
      Phylo: 0,
      Size: 1190.65
    },
    {
      Organism: "Acer yangbiense",
      Phylo: 5,
      Size: 665888
    },
    {
      Organism: "Acetanaerobacterium elongatum",
      Phylo: 9,
      Size: 2.91621
    },
    {
      Organism: "Acetatifactor muris",
      Phylo: 9,
      Size: 6.01365
    },
    {
      Organism: "Acetitomaculum ruminis",
      Phylo: 9,
      Size: 3.08272
    },
    {
      Organism: "Acetivibrio ethanolgignens",
      Phylo: 9,
      Size: 3.66025
    },
    {
      Organism: "Acetoanaerobium noterae",
      Phylo: 9,
      Size: 2.81423
    },
    {
      Organism: "Acetoanaerobium sticklandii",
      Phylo: 9,
      Size: 2.71546
    },
    {
      Organism: "Acetobacter",
      Phylo: 6,
      Size: 3.66419
    },
    {
      Organism: "Acetobacter aceti",
      Phylo: 6,
      Size: 3.72504
    },
    {
      Organism: "Acetobacter ascendens",
      Phylo: 6,
      Size: 2.99922
    },
    {
      Organism: "Acetobacter cerevisiae",
      Phylo: 6,
      Size: 3.32358
    },
    {
      Organism: "Acetobacter cibinongensis",
      Phylo: 6,
      Size: 3.10509
    },
    {
      Organism: "Acetobacter fabarum",
      Phylo: 6,
      Size: 3.09543
    },
    {
      Organism: "Acetobacter ghanensis",
      Phylo: 6,
      Size: 2.84394
    },
    {
      Organism: "Acetobacter indonesiensis",
      Phylo: 6,
      Size: 3.41677
    },
    {
      Organism: "Acetobacter malorum",
      Phylo: 6,
      Size: 4.03941
    },
    {
      Organism: "Acetobacter nitrogenifigens",
      Phylo: 6,
      Size: 4.27052
    },
    {
      Organism: "Acetobacter oeni",
      Phylo: 6,
      Size: 4.06347
    },
    {
      Organism: "Acetobacter okinawensis",
      Phylo: 6,
      Size: 3.16624
    },
    {
      Organism: "Acetobacter orientalis",
      Phylo: 6,
      Size: 2.91069
    },
    {
      Organism: "Acetobacter orleanensis",
      Phylo: 6,
      Size: 3.01364
    },
    {
      Organism: "Acetobacter oryzifermentans",
      Phylo: 6,
      Size: 3.12745
    },
    {
      Organism: "Acetobacter oryzoeni",
      Phylo: 6,
      Size: 3.15318
    },
    {
      Organism: "Acetobacter papayae",
      Phylo: 6,
      Size: 3.04108
    },
    {
      Organism: "Acetobacter pasteurianus",
      Phylo: 6,
      Size: 3.76598
    },
    {
      Organism: "Acetobacter peroxydans",
      Phylo: 6,
      Size: 2.71279
    },
    {
      Organism: "Acetobacter persici",
      Phylo: 6,
      Size: 3.75668
    },
    {
      Organism: "Acetobacter pomorum",
      Phylo: 6,
      Size: 3.33212
    },
    {
      Organism: "Acetobacter senegalensis",
      Phylo: 6,
      Size: 3.96991
    },
    {
      Organism: "Acetobacter syzygii",
      Phylo: 6,
      Size: 2.97425
    },
    {
      Organism: "Acetobacter tropicalis",
      Phylo: 6,
      Size: 4.13966
    },
    {
      Organism: "Acetobacteraceae bacterium",
      Phylo: 6,
      Size: 7.74829
    },
    {
      Organism: "Acetobacteraceae bacterium AT-5844",
      Phylo: 6,
      Size: 5.19055
    },
    {
      Organism: "Acetobacteraceae bacterium DB1506",
      Phylo: 6,
      Size: 5.82308
    },
    {
      Organism: "Acetobacteraceae bacterium KEBCLARHB70R",
      Phylo: 6,
      Size: 4.78811
    },
    {
      Organism: "Acetobacteraceae bacterium SCN 69-10",
      Phylo: 6,
      Size: 2.10074
    },
    {
      Organism: "Acetobacteraceae bacterium UBA6159",
      Phylo: 6,
      Size: 4.62817
    },
    {
      Organism: "Acetobacterium",
      Phylo: 9,
      Size: 3.98837
    },
    {
      Organism: "Acetobacterium bakii",
      Phylo: 9,
      Size: 4.13563
    },
    {
      Organism: "Acetobacterium dehalogenans",
      Phylo: 9,
      Size: 4.05007
    },
    {
      Organism: "Acetobacterium paludosum",
      Phylo: 9,
      Size: 3.69113
    },
    {
      Organism: "Acetobacterium tundrae",
      Phylo: 9,
      Size: 3.56308
    },
    {
      Organism: "Acetobacterium wieringae",
      Phylo: 9,
      Size: 3.89583
    },
    {
      Organism: "Acetobacterium woodii",
      Phylo: 9,
      Size: 4.04478
    },
    {
      Organism: "Acetobacteroides hydrogenigenes",
      Phylo: 2,
      Size: 3.8611
    },
    {
      Organism: "Acetohalobium arabaticum",
      Phylo: 9,
      Size: 2.4696
    },
    {
      Organism: "Acetomicrobium",
      Phylo: 8,
      Size: 1.98275
    },
    {
      Organism: "Acetomicrobium flavidum",
      Phylo: 8,
      Size: 2.0217
    },
    {
      Organism: "Acetomicrobium hydrogeniformans",
      Phylo: 8,
      Size: 2.12392
    },
    {
      Organism: "Acetomicrobium mobile",
      Phylo: 8,
      Size: 2.1607
    },
    {
      Organism: "Acetomicrobium thermoterrenum",
      Phylo: 8,
      Size: 1.96758
    },
    {
      Organism: "Acetonema longum",
      Phylo: 9,
      Size: 4.32301
    },
    {
      Organism: "Acetothermia bacterium 64_32",
      Phylo: 11,
      Size: 1.8102
    },
    {
      Organism: "Acetothermia bacterium SCGC AAA255-C06",
      Phylo: 11,
      Size: 0.669384
    },
    {
      Organism: "Acetothermia bacterium UBA2187",
      Phylo: 11,
      Size: 0.930847
    },
    {
      Organism: "Acetothermia bacterium UBA3560",
      Phylo: 11,
      Size: 1.87253
    },
    {
      Organism: "Acetothermia bacterium UBA3563",
      Phylo: 11,
      Size: 1.51927
    },
    {
      Organism: "Acetothermia bacterium UBA3564",
      Phylo: 11,
      Size: 1.49784
    },
    {
      Organism: "Acetothermia bacterium UBA3565",
      Phylo: 11,
      Size: 1.83394
    },
    {
      Organism: "Acetothermia bacterium UBA3571",
      Phylo: 11,
      Size: 1.84257
    },
    {
      Organism: "Acetothermia bacterium UBA3574",
      Phylo: 11,
      Size: 1.57678
    },
    {
      Organism: "Acetothermia bacterium UBA4777",
      Phylo: 11,
      Size: 1.15066
    },
    {
      Organism: "Acetothermia bacterium UBA4801",
      Phylo: 11,
      Size: 1.7278
    },
    {
      Organism: "Acetothermia bacterium UBA7521",
      Phylo: 11,
      Size: 1.79071
    },
    {
      Organism: "Achatina immaculata",
      Phylo: 0,
      Size: 1653.15
    },
    {
      Organism: "Acheta domestica densovirus",
      Phylo: 4,
      Size: 0.005425
    },
    {
      Organism: "Acheta domestica mini ambidensovirus",
      Phylo: 4,
      Size: 0.004945
    },
    {
      Organism: "Acheta domesticus volvovirus",
      Phylo: 12,
      Size: 0.002517
    },
    {
      Organism: "Achimota virus 1",
      Phylo: 4,
      Size: 0.015624
    },
    {
      Organism: "Achimota virus 2",
      Phylo: 4,
      Size: 0.015504
    },
    {
      Organism: "Achipteria coleoptrata",
      Phylo: 0,
      Size: 88.4439
    },
    {
      Organism: "Achlya hypogyna",
      Phylo: 7,
      Size: 43.3985
    },
    {
      Organism: "Acholeplasma",
      Phylo: 9,
      Size: 1.7213
    },
    {
      Organism: "Acholeplasma axanthum",
      Phylo: 9,
      Size: 1.80712
    },
    {
      Organism: "Acholeplasma brassicae",
      Phylo: 9,
      Size: 1.87779
    },
    {
      Organism: "Acholeplasma equifetale",
      Phylo: 9,
      Size: 1.28237
    },
    {
      Organism: "Acholeplasma granularum",
      Phylo: 9,
      Size: 1.45354
    },
    {
      Organism: "Acholeplasma hippikon",
      Phylo: 9,
      Size: 1.43045
    },
    {
      Organism: "Acholeplasma laidlawii",
      Phylo: 9,
      Size: 1.49699
    },
    {
      Organism: "Acholeplasma modicum",
      Phylo: 9,
      Size: 1.21918
    },
    {
      Organism: "Acholeplasma multilocale",
      Phylo: 9,
      Size: 0.924993
    },
    {
      Organism: "Acholeplasma oculi",
      Phylo: 9,
      Size: 1.58712
    },
    {
      Organism: "Acholeplasma palmae",
      Phylo: 9,
      Size: 1.55423
    },
    {
      Organism: "Acholeplasma virus L2",
      Phylo: 4,
      Size: 0.011965
    },
    {
      Organism: "Acholeplasma virus L51",
      Phylo: 4,
      Size: 0.004491
    },
    {
      Organism: "Acholeplasmataceae bacterium",
      Phylo: 9,
      Size: 2.19606
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA2284",
      Phylo: 9,
      Size: 1.53008
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA4853",
      Phylo: 9,
      Size: 1.44715
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA4870",
      Phylo: 9,
      Size: 1.17805
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA4904",
      Phylo: 9,
      Size: 1.19007
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA4950",
      Phylo: 9,
      Size: 1.28065
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5000",
      Phylo: 9,
      Size: 1.13567
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5222",
      Phylo: 9,
      Size: 1.14766
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5250",
      Phylo: 9,
      Size: 1.23054
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5264",
      Phylo: 9,
      Size: 1.15898
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5282",
      Phylo: 9,
      Size: 1.25103
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5354",
      Phylo: 9,
      Size: 1.10042
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5386",
      Phylo: 9,
      Size: 1.09662
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5409",
      Phylo: 9,
      Size: 1.29511
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5430",
      Phylo: 9,
      Size: 1.16703
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5453",
      Phylo: 9,
      Size: 0.998003
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5468",
      Phylo: 9,
      Size: 1.27505
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5491",
      Phylo: 9,
      Size: 1.27951
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5617",
      Phylo: 9,
      Size: 1.50177
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5635",
      Phylo: 9,
      Size: 1.59563
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA5769",
      Phylo: 9,
      Size: 1.76018
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA6150",
      Phylo: 9,
      Size: 1.2855
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA6181",
      Phylo: 9,
      Size: 1.36058
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA6190",
      Phylo: 9,
      Size: 1.67015
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA6235",
      Phylo: 9,
      Size: 1.63881
    },
    {
      Organism: "Acholeplasmataceae bacterium UBA7643",
      Phylo: 9,
      Size: 1.23828
    }
  ];

  constructor() {
    this.createKingdoms();
    this.createPhylos();
   }

  createKingdoms(): void{
    let arrKingdomAux: string[]= 
        ["Animal", "Fungi", "Protists", "Archaebacteria",
        "Eubacteria"];
    let kingdom: Kingdom; //Temp variable

    for(let i: number = 0; i< arrKingdomAux.length; i++  ){
      kingdom = new Kingdom(i,
        arrKingdomAux[i]);
      this.kingdoms.push(kingdom);
    }  
  }

  createPhylos(): void{
    let arrPhylosAux: string[]=
        ["Animal", "Euryarchaeota", "FCB group", "Fungi", "Other", 
        "Plants", "Proteobacteria", "Protists", "Synergistetes", 
        "Terrabacteria group", "unclassified archaeal viruses",
        "unclassified Bacteria", "unclassified viruses"
        ];
    let phylo: Phylo; //Temp variable

    for(let i: number = 0; i< arrPhylosAux.length; i++  ){
      phylo = new Phylo(i,
        arrPhylosAux[i]);
      this.phylos.push(phylo);
    }  
  }

  generateGenomeRandom(): Genome[]{
    let randomKingdom: number;
    let randomReleaseDate: Date;
    let genomes: Genome[]=[];

    //let today = new Date();
    let genome: Genome;

    for ( let i=0; i< this.arrTaxonomy.length; i++){
      randomKingdom = Math.floor(Math.random()*5);

      randomReleaseDate = this.generateRandomDate("01/01/1996", "02/03/2020");
    
      genome = new Genome(i,
                  this.arrTaxonomy[i].Organism,
                  this.kingdoms[randomKingdom],
                  this.phylos[this.arrTaxonomy[i].Phylo],
                  this.arrTaxonomy[i].Size,
                  randomReleaseDate);
    
      genomes.push(genome);
     
    }
    
    return genomes;
  }

  generateRandomDate(date1, date2): any{

    var date1 = date1 || '01-01-1970'
    var date2 = date2 || new Date().toLocaleDateString()
    date1 = new Date(date1).getTime()
    date2 = new Date(date2).getTime()
    if( date1>date2){
        return new Date(this.randomValueBetween(date2,date1)) 
    } else{
        return new Date(this.randomValueBetween(date1, date2))

    }
  }

  randomValueBetween(min, max): number {
    return Math.random() * (max - min) + min;
  }
}
