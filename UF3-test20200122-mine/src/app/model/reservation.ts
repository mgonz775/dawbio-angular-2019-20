import {ReservationTime} from "./reservationTime"
import {TablePreference} from "./tablePreference"
import {SpecialRequest} from "./specialRequest"

export class Reservation {

  //properties
  private id: number;
  private name: string;
  private surname: string;
  private email: string;
  private phone: string;
  private reservationDate: any;
  private reservationTime: ReservationTime;
  private tablePreference: TablePreference;
  private specialRequests: SpecialRequest[];
  private totalPrice:number;


	constructor($id?: number, $name?: string, $surname?: string, $email?: string, $phone?: string, $reservationDate?: any, $reservationTime?: ReservationTime, $tablePreference?: TablePreference, $specialRequests?: SpecialRequest[], $totalPrice?: number) {
		this.id = $id;
		this.name = $name;
		this.surname = $surname;
		this.email = $email;
		this.phone = $phone;
		this.reservationDate = $reservationDate;
		this.reservationTime = $reservationTime;
		this.tablePreference = $tablePreference;
		this.specialRequests = $specialRequests;
		this.totalPrice = $totalPrice;
	}
  

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $name
     * @return {string}
     */
	public get $name(): string {
		return this.name;
	}

    /**
     * Getter $surname
     * @return {string}
     */
	public get $surname(): string {
		return this.surname;
	}

    /**
     * Getter $email
     * @return {string}
     */
	public get $email(): string {
		return this.email;
	}

    /**
     * Getter $phone
     * @return {string}
     */
	public get $phone(): string {
		return this.phone;
	}

    /**
     * Getter $reservationDate
     * @return {any}
     */
	public get $reservationDate(): any {
		return this.reservationDate;
	}

    /**
     * Getter $reservationTime
     * @return {ReservationTime}
     */
	public get $reservationTime(): ReservationTime {
		return this.reservationTime;
	}

    /**
     * Getter $tablePreference
     * @return {TablePreference}
     */
	public get $tablePreference(): TablePreference {
		return this.tablePreference;
	}

    /**
     * Getter $specialRequests
     * @return {SpecialRequest[]}
     */
	public get $specialRequests(): SpecialRequest[] {
		return this.specialRequests;
	}

    /**
     * Getter $totalPrice
     * @return {number}
     */
	public get $totalPrice(): number {
		return this.totalPrice;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $name
     * @param {string} value
     */
	public set $name(value: string) {
		this.name = value;
	}

    /**
     * Setter $surname
     * @param {string} value
     */
	public set $surname(value: string) {
		this.surname = value;
	}

    /**
     * Setter $email
     * @param {string} value
     */
	public set $email(value: string) {
		this.email = value;
	}

    /**
     * Setter $phone
     * @param {string} value
     */
	public set $phone(value: string) {
		this.phone = value;
	}

    /**
     * Setter $reservationDate
     * @param {any} value
     */
	public set $reservationDate(value: any) {
		this.reservationDate = value;
	}

    /**
     * Setter $reservationTime
     * @param {ReservationTime} value
     */
	public set $reservationTime(value: ReservationTime) {
		this.reservationTime = value;
	}

    /**
     * Setter $tablePreference
     * @param {TablePreference} value
     */
	public set $tablePreference(value: TablePreference) {
		this.tablePreference = value;
	}

    /**
     * Setter $specialRequests
     * @param {SpecialRequest[]} value
     */
	public set $specialRequests(value: SpecialRequest[]) {
		this.specialRequests = value;
	}

    /**
     * Setter $totalPrice
     * @param {number} value
     */
	public set $totalPrice(value: number) {
		this.totalPrice = value;
	}

}
