import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';

import { Reservation } from '../model/Reservation';
import { ReservationTipo } from '../model/ReservationTipo';
import { TableSexo } from '../model/TableSexo';
//import { SpecialRequests } from '../model/SpecialRequests';

import {ReservationService} from '../services/reservation.service';

import { CookieService } from 'ngx-cookie-service';
import { from } from 'rxjs';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css'],
  providers:[DatePipe]
})
export class ReservationEntryComponent implements OnInit {
  //Properties
  reservation: Reservation;
  reservationTipos: ReservationTipo[] = [];
  tableSexos: TableSexo[] = [];
  //specialRequests : SpecialRequests[] = [];

  @ViewChild('reservationEntryForm', null) reservationEntryForm: //Accedeme a esta variable y pon el tipo HTMLFormElement.
  HTMLFormElement;

  constructor(private datePipe : DatePipe,
    private cookieService: CookieService,
    private reservationService: ReservationService) { }

  ngOnInit() {
    this.reservationTipos = this.reservationService.createReservationTipos();
    this.tableSexos = this.reservationService.createReservationSexos();
    //this.specialRequests = this.reservationService.createSpecialRequests();
    this.initializeForm();    
    this.getCookie();
  }

  reservationEntry(): void {
    this.cookieService.set("reservation", JSON.stringify(this.reservation));
    console.log(this.reservation);
  }

  

  initializeForm(){ 
    this.reservationEntryForm.reset(); //(en teoria)Cada vez que le demos a recargar se borran los campos pero no las cookies.
    //this.reservationEntryForm.childNodes.item()
    this.reservationEntryForm.form.markAsPristine();
    this.reservationEntryForm.form.markAsUntouched();


    this.reservation = new Reservation();

    this.reservation.reservationDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.reservation.reservationTipo = this.reservationTipos[2];
    this.reservation.tableSexo = this.tableSexos[0];
    //this.reservation.specialRequests=[];
   
  }

  getCookie(){
    if(this.cookieService.check("reservation")){
      let cookieObj: any = JSON.parse(this.cookieService.get("reservation"));
      /**
     * Copy the values of all of the enumerable own properties from one or more source objects to a
     * target object. Returns the target object.
     * @param target The target object to copy to.
     * @param source The source object from which to copy properties.
     */
      Object.assign(this.reservation, cookieObj);

      this.reservation.reservationTipo =
        this.reservationTipos[cookieObj._reservationTipo.id];
      
      this.reservation.tableSexo =
        this.tableSexos[cookieObj._tableSexo.id];

      /*this.reservation.specialRequests = [];
      for(let spAux of cookieObj._specialRequests){
        this.reservation.specialRequests.push(
          this.specialRequests[spAux.id]);
      }*/
      
    }
  }
}




