import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
from '@angular/forms';

@Directive({
  selector: '[inputMinLength]',
  providers: [{provide: NG_VALIDATORS,
    useExisting: InputValidationDirective,
    multi:true}]
})
export class InputValidationDirective implements Validator{

  constructor() { }

  //This method recibes the control to validate: formFieldValidate
  validate(formFieldValidate: AbstractControl):
    {[key: string]: any}
    {
      let validInput: boolean = false;

      if(formFieldValidate && formFieldValidate.value && formFieldValidate.value.length > 2){ //Aqui para poner la longitud del nombre
          validInput = true;
        }

      return validInput? null:{'isNotCorrect':true};
    }
    //returns an object with the errors of the validation
    //or null in case the validation was correct

}
