import {ReservationTipo} from "./ReservationTipo"
import {TableSexo} from "./TableSexo"
//import {SpecialRequests} from "./SpecialRequests"

export class Reservation{
    // Properties
    private _id: number;
    private _name: string;
    private _surname: string;
    private _email: string;
    private _phone: string;
    private _reservationDate: string;
    private _reservationTipo: ReservationTipo;
    private _tableSexo: TableSexo;
    private _password: string;
    //private _specialRequests: SpecialRequests[];


	constructor(id?: number, name?: string, surname?: string, email?: string, phone?: string, reservationDate?: string, reservationTipo?: ReservationTipo, tableSexo?: TableSexo, password?: string/*, specialRequests?: SpecialRequests[], totalPrice?: number*/) {
		this._id = id;
		this._name = name;
		this._surname = surname;
		this._email = email;
		this._phone = phone;
		this._reservationDate = reservationDate;
		this._reservationTipo = reservationTipo;
        this._tableSexo = tableSexo;
        this._password = password;
		//this._specialRequests = specialRequests;
		//this._totalPrice = totalPrice;
	}


    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter surname
     * @return {string}
     */
	public get surname(): string {
		return this._surname;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter phone
     * @return {string}
     */
	public get phone(): string {
		return this._phone;
	}

    /**
     * Getter reservationDate
     * @return {Date}
     */
	public get reservationDate(): string {
		return this._reservationDate;
	}

    /**
     * Getter reservationTipo
     * @return {ReservationTipo}
     */
	public get reservationTipo(): ReservationTipo {
		return this._reservationTipo;
	}

    /**
     * Getter tableSexo
     * @return {TableSexo}
     */
	public get tableSexo(): TableSexo {
		return this._tableSexo;
    }
    
    /**
     * Getter password
     * @return {string}
     */
	public get password(): string {
		return this._password;
    }
    
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {string} value
     */
	public set surname(value: string) {
		this._surname = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter phone
     * @param {string} value
     */
	public set phone(value: string) {
		this._phone = value;
	}

    /**
     * Setter reservationDate
     * @param {Date} value
     */
	public set reservationDate(value: string) {
		this._reservationDate = value;
	}

    /**
     * Setter reservationTipo
     * @param {ReservationTipo} value
     */
	public set reservationTipo(value: ReservationTipo) {
		this._reservationTipo = value;
	}

    /**
     * Setter tableSexo
     * @param {TableSexo} value
     */
	public set tableSexo(value: TableSexo) {
		this._tableSexo = value;
    }
    
    /**
     * Setter password
     * @param {string} value
     */
	public set password(value: string) {
		this._password = value;
	}

}