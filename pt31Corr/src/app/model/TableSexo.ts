export class TableSexo{
    private id: number;
    private sexo: string;
    private price: number;


	constructor($id: number, $sexo: string, $price: number) {
		this.id = $id;
		this.sexo = $sexo;
		this.price = $price;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $sexo
     * @return {string}
     */
	public get $sexo(): string {
		return this.sexo;
	}

    /**
     * Getter $price
     * @return {number}
     */
	public get $price(): number {
		return this.price;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $sexo
     * @param {string} value
     */
	public set $sexo(value: string) {
		this.sexo = value;
	}

    /**
     * Setter $price
     * @param {number} value
     */
	public set $price(value: number) {
		this.price = value;
	}

}