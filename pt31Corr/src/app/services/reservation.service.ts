import { Injectable } from '@angular/core';
import { ReservationTipo } from '../model/ReservationTipo';
import { TableSexo } from '../model/TableSexo';
import { Placeholder } from '@angular/compiler/src/i18n/i18n_ast';
import { Cadena } from '../model/Cadena';
import { $ } from 'protractor';
//import { SpecialRequests } from '../model/SpecialRequests';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor() { }

  createReservationTipos(): ReservationTipo[] {
    let reservationTipos: ReservationTipo[] = [];
    //A temporary array of ReservationTipos is builded due to the lack of database
    let reservationTiposAux: string[] = ["ADN", "ARN"];

    for (let i: number = 0; i < reservationTiposAux.length; i++) {
      reservationTipos.push(new ReservationTipo(i, reservationTiposAux[i]));
    }
    return reservationTipos;
  }

  createReservationSexos(): TableSexo[] {
    let tableSexos: TableSexo[] = [];
    let tableSexosAux: string[] =
      ["Hombre", "Mujer"];
    let tableSexo: TableSexo; //Temp variable

    for (let i: number = 0; i < tableSexosAux.length; i++) {
      tableSexo = new TableSexo(i,
        tableSexosAux[i], i * 2 + 3);
      tableSexos.push(tableSexo);
    }
    return tableSexos;
  }

  //Hacer la conversion y todo si puedes, tu pots felipe
  createReservationCadenas(){
    let Cadenas: Cadena[] = [];
    let CadenasAux: string[] = ["ADN", "ARN"];

    let Cadena: Cadena;

    for(let i: number = 0; i < CadenasAux.length; i++){
      if(CadenasAux[i] == "ADN"){
      }
    }

    
  }

}
