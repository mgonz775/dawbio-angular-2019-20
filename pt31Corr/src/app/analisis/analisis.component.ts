import { Component, OnInit, ViewChild } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationTipo } from '../model/ReservationTipo';
import { ReservationService } from '../services/reservation.service';
//importar la cadena


@Component({
  selector: 'app-analisis',
  templateUrl: './analisis.component.html',
  styleUrls: ['./analisis.component.css']
})
export class AnalisisComponent implements OnInit {
  reservation: Reservation;
  reservationTipos: ReservationTipo[] = [];


  @ViewChild('reservationEntryForm', null) reservationEntryForm: //Accedeme a esta variable y pon el tipo HTMLFormElement.
    HTMLFormElement;


  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    this.reservationTipos = this.reservationService.createReservationTipos();
    this.initializeForm();
  }

  reservationEntry(): void {
    console.log(this.reservation);
  }

  initializeForm() {
    this.reservationEntryForm.reset(); //(en teoria)Cada vez que le demos a recargar se borran los campos pero no las cookies.
    //this.reservationEntryForm.childNodes.item()
    this.reservationEntryForm.form.markAsPristine();
    this.reservationEntryForm.form.markAsUntouched();


    this.reservation = new Reservation();

    this.reservation.reservationTipo = this.reservationTipos[2];

  }

}
