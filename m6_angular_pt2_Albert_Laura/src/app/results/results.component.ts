//This is the controller of the results.component.html
import { Component, OnInit, ViewChild, Input, ChangeDetectorRef } from '@angular/core';

import { Results } from '../model/Results';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';

import { ResultsService } from '../services/results.service';
//import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  //Properties
  @Input("objResultsInput") set selectedResults(value: Results) {
    this.objResults = value;  
  }
  objResults: Results;
  resultsPlaces: ResultsPlace[] = [];
  resultsDates: ResultsDate[] = [];
  resultsCopies: ResultsCopies[] = [];

  //cookieObj: any;

  @ViewChild('resultsForm', null)
  resultsForm: HTMLFormElement;

  /**
   *Creates an instance of ResultsComponent.
   * @param {CookieService} cookieService
   * @param {ResultsService} resultsService
   * @memberof ResultsComponent
   */
  constructor(//private cookieService: CookieService,
     private resultsService: ResultsService, private changeDetectorRef: ChangeDetectorRef) { }

  //This function starts when the page is loaded
  ngOnInit() {
    this.resultsPlaces = this.resultsService.createPlaces();
    this.resultsDates = this.resultsService.createDates();
    this.resultsCopies = this.resultsService.createCopies();
  }

  ngOnChanges(changes: any){  
    this.initializeForm();
    //this.getCookie();

    setTimeout(() => {
      this.calculateTotalPrice();
    });
  }

  //This function prints the object Results in console
  results(): void {
    //this.cookieService.set("objResults", JSON.stringify(this.objResults));
    console.log(this.objResults);
  }

  //This function initializes the variables when the form is loaded
  initializeForm() {
    this.resultsForm.reset();
    this.resultsForm.form.markAsPristine();
    this.resultsForm.form.markAsUntouched();

    //If this.objResults is null is because no input param has been received. Otherwise, we should be careful not to smash the parameter
    if (!this.objResults) {
      this.objResults = new Results();

      this.objResults.resultsPlace = this.resultsPlaces[0]; //the first place of the array will appear selected as default
      this.objResults.resultsDate = this.resultsDates[0]; //the first date of the array will appear marked as default
      this.objResults.resultsCopies = this.resultsCopies[0]; //the first answer of the array will appear selected as default
    // } else {    
    //   this.objResults.resultsPlace = new ResultsPlace(this.objResults.resultsPlace.id, this.objResults.resultsPlace.description, this.objResults.resultsPlace.price);
    //   this.objResults.resultsDate = new ResultsDate(this.objResults.resultsDate.id, this.objResults.resultsDate.description, this.objResults.resultsDate.price);
    //   this.objResults.resultsCopies = new ResultsCopies(this.objResults.resultsCopies.id, this.objResults.resultsCopies.description, this.objResults.resultsCopies.price);
    }
  }

  /*getCookie() {
    if (this.cookieService.check("objResults")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objResults"));
      Object.assign(this.objResults, this.cookieObj);

      if (this.cookieObj.resultsPlace) {
        this.objResults.resultsPlace =
          this.resultsPlaces[this.cookieObj.resultsPlace.id];
      }

      if (this.cookieObj.resultsDate) {
        this.objResults.resultsDate =
          this.resultsDates[this.cookieObj.resultsDate.id];
      }

      if (this.cookieObj.resultsCopies) {
        this.objResults.resultsCopies =
          this.resultsCopies[this.cookieObj.resultsCopies.id];
      }
    }
  }*/

  //This function calculates the total price
  calculateTotalPrice(): void {   
        
    this.objResults.totalPrice = 97;
    this.objResults.totalPrice += this.objResults.resultsPlace.price + this.objResults.resultsDate.price + this.objResults.resultsCopies.price;
  }

  comparePlaces(place1: ResultsPlace, place2: ResultsPlace){     
    return place1 && place2 && place1.id == place2.id;
  }

  compareCopies(copy1: ResultsCopies, copy2: ResultsCopies){
    return copy1 && copy2 && copy1.id == copy2.id;
  }

  resultsDateChange($event){
    this.objResults.resultsDate = this.resultsDates[$event.target.value];
    this.calculateTotalPrice()
  }
}
