export class ResultsPlace {

    //Properties
    private _id: number;
    private _description: string;
    private _price: number;

	/**
     *Creates an instance of ResultPlace.
     * @param {number} id
     * @param {string} description
     * @param {number} price
     * @memberof ResultPlace
     */
    constructor(id: number, description: string, price: number) {
		this._id = id;
		this._description = description;
		this._price = price;
    }

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter description
     * @return {string}
     */
	public get description(): string {
		return this._description;
	}

    /**
     * Getter price
     * @return {number}
     */
	public get price(): number {
		return this._price;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter description
     * @param {string} value
     */
	public set description(value: string) {
		this._description = value;
	}

    /**
     * Setter price
     * @param {number} value
     */
	public set price(value: number) {
		this._price = value;
	}
}