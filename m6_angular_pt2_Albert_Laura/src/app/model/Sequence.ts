import { SequenceType } from './SequenceType';

export class Sequence {

    //Properties
    private _sequenceType: SequenceType;
    private _sequence: string;
    private _file: File;

	/**
     *Creates an instance of Sequence.
     * @param {SequenceType} sequenceType
     * @param {string} sequence
     * @param {File} file
     * @memberof Sequence
     */
    constructor(sequenceType?: SequenceType, sequence?: string, file?: File) {
		this._sequenceType = sequenceType;
		this._sequence = sequence;
		this._file = file;
    }

    /**
     * Getter sequenceType
     * @return {SequenceType}
     */
	public get sequenceType(): SequenceType {
		return this._sequenceType;
	}

    /**
     * Getter sequence
     * @return {string}
     */
	public get sequence(): string {
		return this._sequence;
	}

    /**
     * Getter file
     * @return {File}
     */
	public get file(): File {
		return this._file;
	}

    /**
     * Setter sequenceType
     * @param {SequenceType} value
     */
	public set sequenceType(value: SequenceType) {
		this._sequenceType = value;
	}

    /**
     * Setter sequence
     * @param {string} value
     */
	public set sequence(value: string) {
		this._sequence = value;
	}

    /**
     * Setter file
     * @param {File} value
     */
	public set file(value: File) {
		this._file = value;
	}
}
