import { ResultsPlace } from './ResultsPlace';
import { ResultsDate } from './ResultsDate';
import { ResultsCopies } from './ResultsCopies';

export class Results {

    //Properties
    private _resultsPlace: ResultsPlace;
    private _resultsDate: ResultsDate;
    private _resultsCopies: ResultsCopies;
    private _resultsNumCopies: number;
    private _totalPrice: number;

	constructor(resultsPlace: ResultsPlace, resultsDate: ResultsDate, resultsCopies: ResultsCopies, resultsNumCopies: number, totalPrice: number) {
		this._resultsPlace = resultsPlace;
		this._resultsDate = resultsDate;
		this._resultsCopies = resultsCopies;
		this._resultsNumCopies = resultsNumCopies;
		this._totalPrice = totalPrice;
	}

    /**
     * Getter resultsPlace
     * @return {ResultsPlace}
     */
	public get resultsPlace(): ResultsPlace {
		return this._resultsPlace;
	}

    /**
     * Getter resultsDate
     * @return {ResultsDate}
     */
	public get resultsDate(): ResultsDate {
		return this._resultsDate;
	}

    /**
     * Getter resultsCopies
     * @return {ResultsCopies}
     */
	public get resultsCopies(): ResultsCopies {
		return this._resultsCopies;
	}

    /**
     * Getter resultsNumCopies
     * @return {number}
     */
	public get resultsNumCopies(): number {
		return this._resultsNumCopies;
	}

    /**
     * Getter totalPrice
     * @return {number}
     */
	public get totalPrice(): number {
		return this._totalPrice;
	}

    /**
     * Setter resultsPlace
     * @param {ResultsPlace} value
     */
	public set resultsPlace(value: ResultsPlace) {
		this._resultsPlace = value;
	}

    /**
     * Setter resultsDate
     * @param {ResultsDate} value
     */
	public set resultsDate(value: ResultsDate) {
		this._resultsDate = value;
	}

    /**
     * Setter resultsCopies
     * @param {ResultsCopies} value
     */
	public set resultsCopies(value: ResultsCopies) {
		this._resultsCopies = value;
	}

    /**
     * Setter resultsNumCopies
     * @param {number} value
     */
	public set resultsNumCopies(value: number) {
		this._resultsNumCopies = value;
	}

    /**
     * Setter totalPrice
     * @param {number} value
     */
	public set totalPrice(value: number) {
		this._totalPrice = value;
    }
}
