import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsmanagementComponent } from './resultsmanagement.component';

describe('ResultsmanagementComponent', () => {
  let component: ResultsmanagementComponent;
  let fixture: ComponentFixture<ResultsmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
