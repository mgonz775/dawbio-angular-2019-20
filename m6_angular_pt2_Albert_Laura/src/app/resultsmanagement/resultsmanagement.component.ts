//This is the controller of the resultsManagement.html
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Results } from '../model/Results';
import { ResultsService } from '../services/results.service';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';

@Component({
  selector: 'app-resultsmanagement',
  templateUrl: './resultsmanagement.component.html',
  styleUrls: ['./resultsmanagement.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsmanagementComponent implements OnInit {

  //Filter properties
  priceFilter: number = 0;
  
  //Pagination properties
  currentPage: number = 1;
  itemsPerPage: number = 10;

  results: Results[] = [];
  resultsFiltered: Results[] = [];
  resultSelected: Results;

  /**
   *Creates an instance of ResultsmanagementComponent.
   * @param {ResultsService} resultsService
   * @memberof ResultsmanagementComponent
   */
  constructor(private resultsService: ResultsService) {
  }

  //This function starts when the page is loaded
  ngOnInit() {
    this.results = this.resultsService.generateResultsRandom();
    this.resultsFiltered = this.results;
    this.resultSelected = new Results(new ResultsPlace(-1, "", 0), new ResultsDate(0, "",0),new ResultsCopies(0,"",0), 0, 0);

    this.priceFilter = 121;
    this.itemsPerPage = 10;
    this.currentPage = 1;
  }

  //This function filters the results according to the params selected
  filter() {
    this.resultsFiltered = this.results.filter(result => {
      let priceValid: boolean = false;
      priceValid = (result.totalPrice <= this.priceFilter);
      
      return priceValid;
    })
  }

  //This function shows the result selected
  onClick(rv: Results) {
    Object.assign(this.resultSelected, rv);
    //this.resultSelected = rv;
    console.log(rv);
  }
  
  //This function removes the result selected
  removeResult(rv: Results) {
    this.results.splice(this.results.indexOf(rv), 1);
    this.resultsFiltered.splice(this.resultsFiltered.indexOf(rv), 1);
  }
}
