import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl} 
  from '@angular/forms';

@Directive({
  selector: '[passwordMinLength]',
  providers: [{provide: NG_VALIDATORS, 
    useExisting: PasswordMinLengthDirective,
    multi: true }]
})
export class PasswordMinLengthDirective implements Validator{

  constructor() { }
  
  /**
     * @description
     * Method that performs synchronous validation against the provided control.
     *
     * @param control The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */
  validate(formFieldToValidate: AbstractControl): {[key:string]: any}{
    let validInput: boolean = false;

    if(formFieldToValidate && formFieldToValidate.value 
      && formFieldToValidate.value.length >= 8){
      validInput = true;
    }

    return validInput? null: {'isNotCorrect':true};
  }
}