//This is the controller of the user-entry.component.html
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';

import { User } from '../model/User';

import { UserService } from '../services/user.service';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-user-entry',
  templateUrl: './user-entry.component.html',
  styleUrls: ['./user-entry.component.css'],
  providers:[DatePipe]
})
export class UserEntryComponent implements OnInit {

  //Properties
  objUser: User;
  
  cookieObj: any;

  @ViewChild('userEntryForm', null)
  userEntryForm: HTMLFormElement;
  

  /**
   *Creates an instance of UserEntryComponent.
   * @param {DatePipe} datePipe
   * @param {CookieService} cookieService
   * @param {UserService} userService
   * @memberof UserEntryComponent
   */
  constructor(private datePipe : DatePipe,
    private cookieService: CookieService,
    private userService: UserService) { }

  //This function starts when the page is loaded
  ngOnInit() {
    this.initializeForm();
    this.getCookie();
  }

  //This function prints the cookies in the form and the object User in console
  userEntry(): void{
    this.cookieService.set("objUser", JSON.stringify(this.objUser));
    console.log(this.objUser); //it prints in console the object User
  }

  //This function initializes the variables when the form is loaded
  initializeForm() {
    this.userEntryForm.reset();
    this.userEntryForm.form.markAsPristine();
    this.userEntryForm.form.markAsUntouched();

    this.objUser = new User();
    this.objUser.birthdate = this.datePipe.transform(new Date(), 'yyyy-MM-dd'); //this is to initialize the birthdate to todays's date
  }
  
  //This fuction gets the values of the varibales and inserts them in the cookie object
  getCookie(){
    if(this.cookieService.check("objUser")){
      this.cookieObj = JSON.parse(this.cookieService.get("objUser"));
      Object.assign(this.objUser, this.cookieObj);
    }
  }
  
  alertEvent(event) {
    alert("The form have been send");
    console.log(event);
 }
 resetEvent(event){
  alert("Form reseted");
  this.userEntryForm.reset();
 }
}
