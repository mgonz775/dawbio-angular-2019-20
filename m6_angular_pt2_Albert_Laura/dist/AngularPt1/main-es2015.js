(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container well\">\n  <nav>\n      <a routerLink=\"/user-entry\">User entry</a> -- \n      <a routerLink=\"/sequence-entry\">Sequence entry</a> --\n      <a routerLink=\"/price-calculate\">Calculate price</a> -- \n      <a routerLink=\"/feedback\">Feedback</a> --\n      <a routerLink=\"/resultsmanagement\">Results Management</a>\n  </nav><br>\n  <router-outlet></router-outlet>\n</div>\n\n<!-- Without routing -->\n<!--\n<app-user-entry></app-user-entry>\n<app-sequence-entry></app-sequence-entry>\n<app-results></app-results>\n<app-feedback></app-feedback>\n-->\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feedback/feedback.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feedback/feedback.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This is the main page of the feedback -->\n<form #feedbackForm=\"ngForm\" action=\"\" (submit)=\"feedback()\">\n    <div class=\"jumbotron\">\n        <div class=\"col-xs-12 text-center\">\n            <h2>Feedback</h2>\n        </div>\n    </div>\n    <br>\n\n    <div class=\"jumbotron\">\n        <div class=\"col-xs-12 text-center\">\n            <h3>Personal data</h3>\n        </div>\n        <br>\n\n        <!-- Name -->\n        <label class=\"col-xs-12\">Name *</label>\n        <div class=\"col-xs-12\">\n            <input #fName=\"ngModel\" [(ngModel)]=\"objFeedback.name\" name=\"fName\" type=\"text\" class=\"form-control\"\n                required />\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!fName.pristine && \n              fName.errors?.required\">\n            <span class=\"text-danger\">Please, enter a name</span>\n        </div>\n        <br>\n\n        <!-- Surname -->\n        <label class=\"col-xs-12\">Surname *</label>\n        <div class=\"col-xs-12\">\n            <input #fSurname=\"ngModel\" [(ngModel)]=\"objFeedback.surname\" name=\"fSurname\" type=\"text\"\n                class=\"form-control\" required />\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!fSurname.pristine && \n              fSurname.errors?.required\">\n            <span class=\"text-danger\">Please, enter a surname</span>\n        </div>\n        <br>\n\n        <!-- Email -->\n        <label class=\"col-xs-12\">Email *</label>\n        <div class=\"col-xs-12\">\n            <input #fEmail=\"ngModel\" [(ngModel)]=\"objFeedback.email\" name=\"fEmail\" type=\"email\" class=\"form-control\"\n                placeholder=\"hola@hola.com\" pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" required />\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!fEmail.pristine && \n          fEmail.errors?.required\">\n            <span class=\"text-danger\">Please, enter an email</span>\n        </div>\n        <!-- Error div when error email -->\n        <div class=\"col-xs-12\" *ngIf=\"!fEmail.errors?.required &&\n           fEmail.errors?.pattern\">\n            <span class=\"text-danger\">Please, enter an email with a valid format</span>\n        </div>\n        <br><br>\n\n        <div class=\"col-xs-12 text-center\">\n            <h3>Feedback</h3>\n        </div>\n        <br>\n\n        <label class=\"col-xs-12\">How did you know this webpage? (You can choose more than one option)</label>\n        <div class=\"col-xs-12\" #divCheckBox>\n            <span *ngFor=\"let resPlaceAux of places; \n            let i=index\">\n                <input type=\"checkbox\" name=\"fPlaces\" (click)=\"addRemovePlaces(resPlaceAux)\">{{resPlaceAux.description}}\n            </span>\n        </div>\n        <br><br>\n\n        <label class=\"col-xs-12\">How do you rate the webpage?</label>\n        <div class=\"col-xs-12\">\n            <span *ngFor=\"let resRateAux of\n                  rates; let i = index\">\n                <input type=\"radio\" name=\"fRate{{i}}\" [value]=\"resRateAux\" [(ngModel)]=\"objFeedback.rate\">\n                {{resRateAux.description}}\n            </span>\n        </div>\n        <br><br>\n\n        <label class=\"col-xs-12\">Would you recommend us?</label>\n        <div class=\"col-xs-12\">\n            <select class=\"form-control\" [(ngModel)]=\"objFeedback.recommendation\" name=\"fRecommendation\">\n                <option *ngFor=\"let resRecommendationAux\n                 of recommendations; let i=index\" [ngValue]=\"resRecommendationAux\">\n                    {{resRecommendationAux.description}}</option>\n            </select>\n        </div>\n        <br><br>\n\n        <label class=\"col-xs-12\">Comments *</label>\n        <div class=\"col-xs-12\">\n            <textarea #fComments=\"ngModel\" [(ngModel)]=\"objFeedback.comments\" name=\"fComments\" rows=\"4\" cols=\"50\"\n                maxlength=\"100\" class=\"form-control\" placeholder=\"max. 100 characters\" required></textarea>\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!fComments.pristine && \n      fComments.errors?.required\">\n            <span class=\"text-danger\">Please, enter a comment</span>\n        </div>\n        <br><br>\n\n        <!-- Button submit is disabled until all the form fields are valid -->\n        <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!feedbackForm.valid\" (click)=\"alertEvent($event)\">Submit</button>\n        &nbsp;\n        <!--Button reset for resetting all the data introduced -->\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetEvent($event)\">Reset</button>\n    </div>\n</form>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/page-not-found/page-not-found.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/page-not-found/page-not-found.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>This page has not been found...</p>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This is the main page of the results -->\n<form #resultsForm=\"ngForm\" action=\"\" (submit)=\"results()\">\n    <div class=\"jumbotron\">\n        <div class=\"col-xs-12 text-center\">\n            <h2>Results</h2>\n        </div>\n    </div>\n    <br>\n\n    <div class=\"jumbotron\">\n        <div class=\"col-xs-12 text-center\">\n            <h3>Calculate final price</h3>\n        </div>\n        <br>\n\n        <!-- Place -->\n        <label class=\"col-xs-12\">Where do you prefer to collect the results? *</label>\n        <div class=\"col-xs-12\">\n            <select class=\"form-control\" name=\"rPlace\" [(ngModel)]=\"objResults.resultsPlace\"\n                (ngModelChange)=\"calculateTotalPrice()\" required>\n                <option *ngFor=\"let resPlaceAux\n                 of resultsPlaces; let i=index\" [ngValue]=\"resPlaceAux\">\n                    {{resPlaceAux.description}}</option>\n            </select>\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!rPlace?.pristine && \n        rPlace?.errors?.required\">\n            <span class=\"text-danger\">Please, choose a place</span>\n        </div>\n        <br><br>\n\n        <!-- Date -->\n        <label class=\"col-xs-12\">When do you prefer to collect the results? *</label>\n        <div class=\"col-xs-12\">\n            <span *ngFor=\"let resDateAux of\n                  resultsDates; let i = index\">\n                <input type=\"radio\" name=\"rDate{{i}}\" [value]=\"resDateAux\" [(ngModel)]=\"objResults.resultsDate\"\n                    (ngModelChange)=\"calculateTotalPrice()\" required>\n                {{resDateAux.description}}\n            </span>\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!rDate?.pristine && \n        rDate?.errors?.required\">\n            <span class=\"text-danger\">Please, choose a date</span>\n        </div>\n        <br><br>\n\n        <!-- Copies -->\n        <label class=\"col-xs-12\">Will you need extra copies of the results? *</label>\n        <div class=\"col-xs-12\">\n            <select class=\"form-control\" name=\"rCopies\" [(ngModel)]=\"objResults.resultsCopies\"\n                (ngModelChange)=\"calculateTotalPrice()\" required>\n                <option *ngFor=\"let resCopiesAux\n                 of resultsCopies; let i=index\" [ngValue]=\"resCopiesAux\">\n                    {{resCopiesAux.description}}</option>\n            </select>\n        </div>\n        <!-- Error div when error required -->\n        <div class=\"col-xs-12\" *ngIf=\"!rCopies?.pristine && \n        rCopies?.errors?.required\">\n            <span class=\"text-danger\">Please, choose an option</span>\n        </div>\n        <br><br>\n\n        <!-- No. of copies -->\n        <label class=\"col-xs-12\">No. of copies</label>\n        <div class=\"col-xs-12\">\n            <input #resultsNumCopies=\"ngModel\" [(ngModel)]=\"objResults.resultsNumCopies\" name=\"rNumCopies\" type=\"number\" class=\"form-control\" min=\"1\" max=\"5\" />\n        </div>\n        <br><br>\n\n        <!-- Total price -->\n        <label class=\"col-xs-12\">Total price:</label>\n        <div class=\"col-xs-12\">\n            <b>{{objResults.totalPrice|currency:'EUR':true}}</b>\n        </div>\n        <br><br>\n\n        <!-- Button submit is disabled until all the form fields are valid -->\n        <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!resultsForm.valid\">Submit</button>\n    </div>\n</form>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/resultsmanagement/resultsmanagement.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/resultsmanagement/resultsmanagement.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This is the main page of the results management -->\n<div class=\"container well\">\n    <!-- This div will contain filters and pagination params -->\n    <form #resultsFilterForm=\"ngForm\" novalidate>\n        <table class=\"table table-striped\">\n            <tr>\n                <td>Filter by price</td>\n                <td>\n                    <input type=\"range\" min=\"0\" max=\"121\" [(ngModel)]=\"priceFilter\" name=\"priceF\"\n                        (ngModelChange)=\"filter()\">\n                    <b>{{priceFilter|currency:'EUR'}}</b>\n                </td>\n            </tr>\n            <tr>\n                <td>\n                    Current Page\n                </td>\n                <td>\n                    {{currentPage}}\n                </td>\n            </tr>\n            <tr>\n                <td>Results per page</td>\n                <td>\n                    <input type=\"number\" [(ngModel)]=\"itemsPerPage\" name=\"itemsPerPage\">\n                </td>\n            </tr>\n            <tr>\n                <td>Results found</td>\n                <td>{{resultsFiltered.length}}</td>\n            </tr>\n\n        </table>\n    </form>\n</div>\n\n<div class=\"container well\">\n    <!-- This div will be shown in case there are records to display -->\n    <form #resultsManagementForm='ngForm' novalidate>\n        <table class=\"table table-striped\">\n            <thead>\n                <tr>\n                    <th>Place</th>\n                    <th>Date</th>\n                    <th>Copies?</th>\n                    <th>No. of copies</th>\n                    <th>Price</th>\n                    <th>Delete result</th>\n                    <th>Go to result</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let rv of resultsFiltered | paginate: {\n                    itemsPerPage:itemsPerPage,\n                    currentPage:currentPage}\">\n                    <td>{{rv.resultsPlace.description}}</td>\n                    <td>{{rv.resultsDate.description}}</td>\n                    <td>{{rv.resultsCopies.description}}</td>\n                    <td>{{rv.resultsNumCopies}}</td>\n                    <td>{{rv.totalPrice | currency:'EUR'}}</td>\n                    <td>\n                        <button class=\"btn btn-danger\" (click)=\"removeResult(rv)\">X</button>\n                    </td>\n                    <td>\n                        <button class=\"btn btn-primary\" (click)=\"onClick(rv)\">Go to</button>\n                    </td>\n                </tr>\n            </tbody>\n\n        </table>\n        <pagination-controls (pageChange)=\"currentPage=$event\" maxSize=\"5\" directionLinks=\"true\" autoHide=\"true\">\n        </pagination-controls>\n    </form>\n\n</div>\n<!-- This prints the details of the result selected -->\n<app-results *ngIf=\"resultSelected\" [objResults]=\"resultSelected\">\n</app-results>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/sequence-entry/sequence-entry.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sequence-entry/sequence-entry.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This is the main page of the sequence-entry -->\n<form #sequenceEntryForm=\"ngForm\" action=\"\" (submit)=\"sequenceEntry()\">\n  <div class=\"jumbotron\">\n    <div class=\"col-xs-12 text-center\">\n      <h2>Sequence entry</h2>\n    </div>\n  </div>\n  <br>\n\n  <div class=\"jumbotron\">\n    <div class=\"col-xs-12 text-center\">\n      <h3>Sequence data</h3>\n    </div>\n    <br>\n\n    <!-- Sequence type -->\n    <label class=\"col-xs-12\">Sequence type *</label>\n    <div class=\"col-xs-12\">\n      <select class=\"form-control\" [(ngModel)]=\"objSequence.sequenceType\" name=\"sequenceType\" required>\n        <option *ngFor=\"let seqTypeAux\n         of sequenceTypes; let i=index\" [ngValue]=\"seqTypeAux\">\n          {{seqTypeAux.description}}</option>\n      </select>\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!sequenceType?.pristine && \n              sequenceType?.errors?.required\">\n      <span class=\"text-danger\">Please, choose a type</span>\n    </div>\n    <br><br>\n\n    <!-- Sequence -->\n    <label class=\"col-xs-12\">Sequence *</label>\n\n    <!-- DNA -->\n    <div class=\"col-xs-12\" *ngIf=\"objSequence.sequenceType.id == 0\">\n      <div class=\"col-xs-12\">\n        <input #sequence=\"ngModel\" [(ngModel)]=\"objSequence.sequence\" name=\"DNA\" type=\"text\" class=\"form-control\"\n          placeholder=\"min. 10 nucleotids\" pattern=\"^[ATCGatcg]{10,}$\" required />\n      </div>\n      <!-- Error div when error required -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.pristine && \n    sequence?.errors?.required\">\n        <span class=\"text-danger\">Please, enter a sequence</span>\n      </div>\n      <!-- Error div when error DNA -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.errors?.required && sequence?.errors?.pattern\">\n        <span class=\"text-danger\">Please, enter a valid DNA sequence (min. 10 nucleotids)</span>\n      </div>\n    </div>\n\n    <!-- RNA -->\n    <div class=\"col-xs-12\" *ngIf=\"objSequence.sequenceType.id == 1\">\n      <div class=\"col-xs-12\">\n        <input #sequence=\"ngModel\" [(ngModel)]=\"objSequence.sequence\" name=\"RNA\" type=\"text\" class=\"form-control\"\n          placeholder=\"min. 10 nucleotids\" pattern=\"^[AUCGaucg]{10,}$\" required />\n      </div>\n      <!-- Error div when error required -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.pristine && \n    sequence?.errors?.required\">\n        <span class=\"text-danger\">Please, enter a sequence</span>\n      </div>\n      <!-- Error div when error RNA -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.errors?.required && sequence?.errors?.pattern\">\n        <span class=\"text-danger\">Please, enter a valid RNA sequence (min. 10 nucleotids)</span>\n      </div>\n    </div>\n\n    <!-- Protein -->\n    <div class=\"col-xs-12\" *ngIf=\"objSequence.sequenceType.id == 2\">\n      <div class=\"col-xs-12\">\n        <input #sequence=\"ngModel\" [(ngModel)]=\"objSequence.sequence\" name=\"protein\" type=\"text\" class=\"form-control\"\n          placeholder=\"No. aminoacids multiple of 3\" pattern=\"^[ACDEFGHIKLMNPQRSTVWYacdefghiklmnpqrstvwy]+$\" required\n          proteinLength />\n      </div>\n      <!-- Error div when error required -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.pristine && \n    sequence?.errors?.required\">\n        <span class=\"text-danger\">Please, enter a sequence</span>\n      </div>\n      <!-- Error div when error length -->\n      <div class=\"col-xs-12\" *ngIf=\"!sequence?.errors?.required && sequence?.errors?.isNotCorrect\">\n        <span class=\"text-danger\">Please, enter a correct number of aminoacids (multiple of 3)</span>\n      </div>\n      <!-- Error div when error protein -->\n      <div *ngIf=\"!sequence?.errors?.isNotCorrect && sequence?.errors?.pattern\">\n        <span class=\"text-danger\">Please, enter a valid protein sequence</span>\n      </div>\n    </div>\n    <br><br>\n\n    <!-- Sequence file -->\n    <label class=\"col-xs-12\">Sequence file</label>\n    <div class=\"col-xs-12\">\n      <input #seqFile=\"ngModel\" [(ngModel)]=\"objSequence.file\" name=\"seqFile\" type=\"file\" class=\"form-control\" />\n    </div>\n    <br><br>\n\n    <!-- Button submit is disabled until all the form fields are valid -->\n    <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!sequenceEntryForm.valid\" (click)=\"alertEvent($event)\">Submit</button>\n    \n  </div>\n</form>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-entry/user-entry.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user-entry/user-entry.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- This is the main page of the user-entry -->\n<form #userEntryForm=\"ngForm\" action=\"\" onreset=\"reset()\" (submit)=\"userEntry()\">\n  <div class=\"jumbotron\">\n    <div class=\"col-xs-12 text-center\">\n      <h2>User registration</h2>\n    </div>\n  </div>\n  <br>\n\n  <div class=\"jumbotron\">\n    <div class=\"col-xs-12 text-center\">\n      <h3>Personal data</h3>\n    </div>\n    <br>\n\n    <!-- Name -->\n    <label class=\"col-xs-12\">Name *</label>\n    <div class=\"col-xs-12\">\n      <input #userName=\"ngModel\" [(ngModel)]=\"objUser.name\" name=\"userName\" type=\"text\" class=\"form-control\" required (focus)=\"myFunction()\"  />\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!userName.pristine && \n            userName.errors?.required\">\n      <span class=\"text-danger\">Please, enter a name</span>\n    </div>\n    <br>\n\n    <!-- Surname -->\n    <label class=\"col-xs-12\">Surname *</label>\n    <div class=\"col-xs-12\">\n      <input #userSurname=\"ngModel\" [(ngModel)]=\"objUser.surname\" name=\"userSurname\" type=\"text\" class=\"form-control\"\n        required />\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!userSurname.pristine && \n            userSurname.errors?.required\">\n      <span class=\"text-danger\">Please, enter a surname</span>\n    </div>\n    <br>\n\n    <!-- DNI -->\n    <label class=\"col-xs-12\">DNI *</label>\n    <div class=\"col-xs-12\">\n      <input #userDNI=\"ngModel\" [(ngModel)]=\"objUser.DNI\" name=\"userDNI\" type=\"text\" class=\"form-control\"\n        placeholder=\"12345678Z\" pattern=\"^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$\" required />\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!userDNI?.pristine && \n            userDNI?.errors?.required\">\n      <span class=\"text-danger\">Please, enter a DNI</span>\n    </div>\n    <!-- Error div when error DNI format -->\n    <div class=\"col-xs-12\" *ngIf=\"!userDNI?.errors?.required &&\n      userDNI?.errors?.pattern\">\n      <span class=\"text-danger\">Please, enter a DNI with a valid format</span>\n    </div>\n    <!-- Error div when error DNI letter-->\n    <div class=\"col-xs-12\"\n      *ngIf=\"!userDNI?.pristine && !userDNI?.errors?.required && !userDNI?.errors?.pattern && !objUser.validateLetterDNI()\">\n      <span class=\"text-danger\">Please, enter a valid DNI</span>\n    </div>\n\n    <br>\n\n    <!-- Email -->\n    <label class=\"col-xs-12\">Email *</label>\n    <div class=\"col-xs-12\">\n      <input #userEmail=\"ngModel\" [(ngModel)]=\"objUser.email\" name=\"userEmail\" type=\"email\" class=\"form-control\"\n        placeholder=\"hola@hola.com\" pattern=\"^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\" required />\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!userEmail.pristine && \n        userEmail.errors?.required\">\n      <span class=\"text-danger\">Please, enter an email</span>\n    </div>\n    <!-- Error div when error email -->\n    <div class=\"col-xs-12\" *ngIf=\"!userEmail.errors?.required &&\n         userEmail.errors?.pattern\">\n      <span class=\"text-danger\">Please, enter an email with a valid format</span>\n    </div>\n    <br>\n\n    <!-- Password -->\n    <label class=\"col-xs-12\">Password *</label>\n    <div class=\"col-xs-12\">\n      <input #userPassword=\"ngModel\" [(ngModel)]=\"objUser.password\" name=\"userPassword\" type=\"password\"\n        class=\"form-control\" placeholder=\"min. 8 characters\" required passwordMinLength />\n    </div>\n    <!-- Error div when error required -->\n    <div class=\"col-xs-12\" *ngIf=\"!userPassword.pristine && \n    userPassword.errors?.required\">\n      <span class=\"text-danger\">Please, enter a password</span>\n    </div>\n    <!-- Error div when error minlength -->\n    <div class=\"col-xs-12\" *ngIf=\"!userPassword.errors?.required && userPassword.errors?.isNotCorrect\">\n      <span class=\"text-danger\">Please, enter a password of minimum 8 characters</span>\n    </div>\n    <br>\n\n    <!-- Phone -->\n    <label class=\"col-xs-12\">Phone</label>\n    <div class=\"col-xs-12\">\n      <input #userPhone=\"ngModel\" [(ngModel)]=\"objUser.phone\" name=\"userPhone\" type=\"tel\" class=\"form-control\" />\n    </div>\n    <br>\n\n    <!-- Birthdate -->\n    <label class=\"col-xs-12\">Birthdate</label>\n    <div class=\"col-xs-12\">\n      <input #userBirthdate=\"ngModel\" [(ngModel)]=\"objUser.birthdate\" name=\"userBirthdate\" type=\"date\"\n        class=\"form-control\" />\n    </div>\n    <br>\n\n    <!-- Zipcode -->\n    <label class=\"col-xs-12\">Zipcode</label>\n    <div class=\"col-xs-12\">\n      <input #userZipcode=\"ngModel\" [(ngModel)]=\"objUser.zipcode\" name=\"userZipcode\" type=\"text\" class=\"form-control\" minLength=\"5\" maxLength=\"5\" />\n    </div>\n    <br>\n\n    <!-- City -->\n    <label class=\"col-xs-12\">City</label>\n    <div class=\"col-xs-12\">\n      <input #userCity=\"ngModel\" [(ngModel)]=\"objUser.city\" name=\"userCity\" type=\"text\"\n        class=\"form-control\" />\n    </div>\n    <br>\n\n    <!-- Country -->\n    <label class=\"col-xs-12\">Country</label>\n    <div class=\"col-xs-12\">\n      <input #userCountry=\"ngModel\" [(ngModel)]=\"objUser.country\" name=\"userCountry\" type=\"text\"\n        class=\"form-control\" />\n    </div>\n    <br><br>\n\n    <!-- Button submit is disabled until all the form fields are valid -->\n    <button type=\"submit\" class=\"btn btn-primary\" [disabled]=\"!userEntryForm.valid\" (click)=\"alertEvent($event)\">Submit</button>\n    &nbsp;\n    <!--Button reset for resetting all the data introduced -->\n    <button type=\"button\" class=\"btn btn-primary\" (click)=\"resetEvent($event)\">Reset</button>\n  </div>\n</form>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'AngularPt1';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/locales/es */ "./node_modules/@angular/common/locales/es.js");
/* harmony import */ var _angular_common_locales_es__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _user_entry_user_entry_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user-entry/user-entry.component */ "./src/app/user-entry/user-entry.component.ts");
/* harmony import */ var _sequence_entry_sequence_entry_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sequence-entry/sequence-entry.component */ "./src/app/sequence-entry/sequence-entry.component.ts");
/* harmony import */ var _feedback_feedback_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./feedback/feedback.component */ "./src/app/feedback/feedback.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _directives_password_min_length_directive__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./directives/password-min-length.directive */ "./src/app/directives/password-min-length.directive.ts");
/* harmony import */ var _directives_protein_length_directive__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./directives/protein-length.directive */ "./src/app/directives/protein-length.directive.ts");
/* harmony import */ var _results_results_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./results/results.component */ "./src/app/results/results.component.ts");
/* harmony import */ var _resultsmanagement_resultsmanagement_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./resultsmanagement/resultsmanagement.component */ "./src/app/resultsmanagement/resultsmanagement.component.ts");









Object(_angular_common__WEBPACK_IMPORTED_MODULE_5__["registerLocaleData"])(_angular_common_locales_es__WEBPACK_IMPORTED_MODULE_6___default.a);









const appRoutes = [
    { path: 'user-entry', component: _user_entry_user_entry_component__WEBPACK_IMPORTED_MODULE_10__["UserEntryComponent"] },
    { path: 'sequence-entry', component: _sequence_entry_sequence_entry_component__WEBPACK_IMPORTED_MODULE_11__["SequenceEntryComponent"] },
    { path: 'price-calculate', component: _results_results_component__WEBPACK_IMPORTED_MODULE_16__["ResultsComponent"] },
    { path: 'feedback', component: _feedback_feedback_component__WEBPACK_IMPORTED_MODULE_12__["FeedbackComponent"] },
    { path: 'resultsmanagement', component: _resultsmanagement_resultsmanagement_component__WEBPACK_IMPORTED_MODULE_17__["ResultsmanagementComponent"] },
    { path: '', redirectTo: '/user-entry', pathMatch: 'full' },
    { path: '**', component: _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_13__["PageNotFoundComponent"] }
];
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
            _user_entry_user_entry_component__WEBPACK_IMPORTED_MODULE_10__["UserEntryComponent"],
            _sequence_entry_sequence_entry_component__WEBPACK_IMPORTED_MODULE_11__["SequenceEntryComponent"],
            _feedback_feedback_component__WEBPACK_IMPORTED_MODULE_12__["FeedbackComponent"],
            _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_13__["PageNotFoundComponent"],
            _directives_password_min_length_directive__WEBPACK_IMPORTED_MODULE_14__["PasswordMinLengthDirective"],
            _directives_protein_length_directive__WEBPACK_IMPORTED_MODULE_15__["ProteinLengthDirective"],
            _results_results_component__WEBPACK_IMPORTED_MODULE_16__["ResultsComponent"],
            _resultsmanagement_resultsmanagement_component__WEBPACK_IMPORTED_MODULE_17__["ResultsmanagementComponent"],
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes, { enableTracing: false } //True for debugging purposes only
            ),
            ngx_pagination__WEBPACK_IMPORTED_MODULE_8__["NgxPaginationModule"]
        ],
        providers: [
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_7__["CookieService"], {
                provide: _angular_core__WEBPACK_IMPORTED_MODULE_4__["LOCALE_ID"],
                useValue: 'es'
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/directives/password-min-length.directive.ts":
/*!*************************************************************!*\
  !*** ./src/app/directives/password-min-length.directive.ts ***!
  \*************************************************************/
/*! exports provided: PasswordMinLengthDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordMinLengthDirective", function() { return PasswordMinLengthDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
var PasswordMinLengthDirective_1;



let PasswordMinLengthDirective = PasswordMinLengthDirective_1 = class PasswordMinLengthDirective {
    constructor() { }
    /**
       * @description
       * Method that performs synchronous validation against the provided control.
       *
       * @param control The control to validate against.
       *
       * @returns A map of validation errors if validation fails,
       * otherwise null.
    */
    validate(formFieldToValidate) {
        let validInput = false;
        if (formFieldToValidate && formFieldToValidate.value
            && formFieldToValidate.value.length >= 8) {
            validInput = true;
        }
        return validInput ? null : { 'isNotCorrect': true };
    }
};
PasswordMinLengthDirective = PasswordMinLengthDirective_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[passwordMinLength]',
        providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
                useExisting: PasswordMinLengthDirective_1,
                multi: true }]
    })
], PasswordMinLengthDirective);



/***/ }),

/***/ "./src/app/directives/protein-length.directive.ts":
/*!********************************************************!*\
  !*** ./src/app/directives/protein-length.directive.ts ***!
  \********************************************************/
/*! exports provided: ProteinLengthDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProteinLengthDirective", function() { return ProteinLengthDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
var ProteinLengthDirective_1;



let ProteinLengthDirective = ProteinLengthDirective_1 = class ProteinLengthDirective {
    constructor() { }
    /**
       * @description
       * Method that performs synchronous validation against the provided control.
       *
       * @param control The control to validate against.
       *
       * @returns A map of validation errors if validation fails,
       * otherwise null.
    */
    validate(formFieldToValidate) {
        let validInput = false;
        if (formFieldToValidate && formFieldToValidate.value
            && formFieldToValidate.value.length % 3 == 0) {
            validInput = true;
        }
        return validInput ? null : { 'isNotCorrect': true };
    }
};
ProteinLengthDirective = ProteinLengthDirective_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[proteinLength]',
        providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
                useExisting: ProteinLengthDirective_1,
                multi: true }]
    })
], ProteinLengthDirective);



/***/ }),

/***/ "./src/app/feedback/feedback.component.css":
/*!*************************************************!*\
  !*** ./src/app/feedback/feedback.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlZWRiYWNrL2ZlZWRiYWNrLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/feedback/feedback.component.ts":
/*!************************************************!*\
  !*** ./src/app/feedback/feedback.component.ts ***!
  \************************************************/
/*! exports provided: FeedbackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackComponent", function() { return FeedbackComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Feedback__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Feedback */ "./src/app/model/Feedback.ts");
/* harmony import */ var _services_feedback_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/feedback.service */ "./src/app/services/feedback.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.js");

//This is the controller of the feedback.component.html




let FeedbackComponent = class FeedbackComponent {
    /**
     *Creates an instance of FeedbackComponent.
     * @param {CookieService} cookieService
     * @param {FeedbackService} feedbackService
     * @memberof FeedbackComponent
     */
    constructor(cookieService, feedbackService) {
        this.cookieService = cookieService;
        this.feedbackService = feedbackService;
        this.places = [];
        this.rates = [];
        this.recommendations = [];
    }
    //This function starts when the page is loaded
    ngOnInit() {
        this.places = this.feedbackService.createPlaces();
        this.rates = this.feedbackService.createRates();
        this.recommendations = this.feedbackService.createRecommendations();
        this.initializeForm();
        this.getCookie();
    }
    //This function begins after the component view is initailized
    ngAfterViewInit() {
        this.getCookieCheckBox();
    }
    //This function prints the cookies in the form and the object Feedback in console
    feedback() {
        this.cookieService.set("objFeedback", JSON.stringify(this.objFeedback));
        console.log(this.objFeedback);
    }
    //This function initializes the variables when the form is loaded
    initializeForm() {
        this.feedbackForm.reset();
        this.feedbackForm.form.markAsPristine();
        this.feedbackForm.form.markAsUntouched();
        this.objFeedback = new _model_Feedback__WEBPACK_IMPORTED_MODULE_2__["Feedback"]();
        this.objFeedback.places = [];
        this.objFeedback.rate = this.rates[0]; //the first rate of the array will appear marked as default
        this.objFeedback.recommendation = this.recommendations[0]; //the first answer of the array will appear selected as default
    }
    //This fuction gets the values of the varibales and inserts them in the cookie object
    getCookie() {
        if (this.cookieService.check("objFeedback")) {
            this.cookieObj = JSON.parse(this.cookieService.get("objFeedback"));
            /**
           * Copy the values of all of the enumerable own properties from one or more source objects to a
           * target object. Returns the target object.
           * @param target The target object to copy to.
           * @param source The source object from which to copy properties.
           */
            Object.assign(this.objFeedback, this.cookieObj);
            if (this.cookieObj && this.cookieObj._rate) {
                this.objFeedback.rate =
                    this.rates[this.cookieObj._rate._id];
            }
            if (this.cookieObj && this.cookieObj._recommendation) {
                this.objFeedback.recommendation =
                    this.recommendations[this.cookieObj._recommendation._id];
            }
            this.objFeedback.places = [];
            for (let placesAux of this.cookieObj._places) {
                this.objFeedback.places.push(this.places[placesAux._id]);
            }
        }
    }
    //This function is the same as previos but only for checkboxes
    getCookieCheckBox() {
        if (this.cookieObj) {
            for (let placesAux of this.cookieObj._places) {
                this.divCheckBox.nativeElement.
                    children[placesAux._id].children[0].checked = true;
            }
        }
    }
    //if the array of places is empty, it fills it. Otherwise, it empties it
    addRemovePlaces(placesAux) {
        //indexOf() method returns the first index at which a given 
        //element can be found in the array, or -1 if it is not present.
        let myIndex = this.objFeedback.places.indexOf(placesAux);
        if (myIndex == -1) {
            this.objFeedback.places.push(placesAux);
        }
        else {
            this.objFeedback.places.splice(myIndex, 1);
        }
    }
    alertEvent(event) {
        alert("The form have been send");
        console.log(event);
    }
    resetEvent(event) {
        alert("Form reseted");
        this.feedbackForm.reset();
    }
};
FeedbackComponent.ctorParameters = () => [
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] },
    { type: _services_feedback_service__WEBPACK_IMPORTED_MODULE_3__["FeedbackService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('feedbackForm', null)
], FeedbackComponent.prototype, "feedbackForm", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('divCheckBox', null)
], FeedbackComponent.prototype, "divCheckBox", void 0);
FeedbackComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-feedback',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./feedback.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feedback/feedback.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./feedback.component.css */ "./src/app/feedback/feedback.component.css")).default]
    })
], FeedbackComponent);



/***/ }),

/***/ "./src/app/model/Feedback.ts":
/*!***********************************!*\
  !*** ./src/app/model/Feedback.ts ***!
  \***********************************/
/*! exports provided: Feedback */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Feedback", function() { return Feedback; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Feedback {
    /**
     *Creates an instance of Feedback.
     * @param {string} name
     * @param {string} surname
     * @param {string} email
     * @param {Places[]} places
     * @param {Rate} rate
     * @param {Recommendation} recommendation
     * @param {string} comments
     * @memberof Feedback
     */
    constructor(name, surname, email, places, rate, recommendation, comments) {
        this._name = name;
        this._surname = surname;
        this._email = email;
        this._places = places;
        this._rate = rate;
        this._recommendation = recommendation;
        this._comments = comments;
    }
    /**
     * Getter name
     * @return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter surname
     * @return {string}
     */
    get surname() {
        return this._surname;
    }
    /**
     * Getter email
     * @return {string}
     */
    get email() {
        return this._email;
    }
    /**
     * Getter places
     * @return {Places[]}
     */
    get places() {
        return this._places;
    }
    /**
     * Getter rate
     * @return {Rate}
     */
    get rate() {
        return this._rate;
    }
    /**
     * Getter recommendation
     * @return {Recommendation}
     */
    get recommendation() {
        return this._recommendation;
    }
    /**
     * Getter comments
     * @return {string}
     */
    get comments() {
        return this._comments;
    }
    /**
     * Setter name
     * @param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter surname
     * @param {string} value
     */
    set surname(value) {
        this._surname = value;
    }
    /**
     * Setter email
     * @param {string} value
     */
    set email(value) {
        this._email = value;
    }
    /**
     * Setter places
     * @param {Places[]} value
     */
    set places(value) {
        this._places = value;
    }
    /**
     * Setter rate
     * @param {Rate} value
     */
    set rate(value) {
        this._rate = value;
    }
    /**
     * Setter recommendation
     * @param {Recommendation} value
     */
    set recommendation(value) {
        this._recommendation = value;
    }
    /**
     * Setter comments
     * @param {string} value
     */
    set comments(value) {
        this._comments = value;
    }
}


/***/ }),

/***/ "./src/app/model/Places.ts":
/*!*********************************!*\
  !*** ./src/app/model/Places.ts ***!
  \*********************************/
/*! exports provided: Places */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Places", function() { return Places; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Places {
    /**
     *Creates an instance of Place.
     * @param {number} id
     * @param {string} description
     * @memberof Places
     */
    constructor(id, description) {
        this._id = id;
        this._description = description;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
}


/***/ }),

/***/ "./src/app/model/Rate.ts":
/*!*******************************!*\
  !*** ./src/app/model/Rate.ts ***!
  \*******************************/
/*! exports provided: Rate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rate", function() { return Rate; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Rate {
    /**
     *Creates an instance of Rate.
     * @param {number} id
     * @param {string} description
     * @memberof Rate
     */
    constructor(id, description) {
        this._id = id;
        this._description = description;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
}


/***/ }),

/***/ "./src/app/model/Recommendation.ts":
/*!*****************************************!*\
  !*** ./src/app/model/Recommendation.ts ***!
  \*****************************************/
/*! exports provided: Recommendation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Recommendation", function() { return Recommendation; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Recommendation {
    /**
     *Creates an instance of Recommendation.
     * @param {number} id
     * @param {string} description
     * @memberof Recommendation
     */
    constructor(id, description) {
        this._id = id;
        this._description = description;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
}


/***/ }),

/***/ "./src/app/model/Results.ts":
/*!**********************************!*\
  !*** ./src/app/model/Results.ts ***!
  \**********************************/
/*! exports provided: Results */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Results", function() { return Results; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Results {
    /**
     *Creates an instance of Results.
     * @param {ResultsPlace} resultsPlace
     * @param {ResultsDate} resultsDate
     * @param {ResultsCopies} resultsCopies
     * @param {number} resultsNumCopies
     * @param {number} totalPrice
     * @memberof Results
     */
    constructor(resultsPlace, resultsDate, resultsCopies, resultsNumCopies, totalPrice) {
        this._resultsPlace = resultsPlace;
        this._resultsDate = resultsDate;
        this._resultsCopies = resultsCopies;
        this._resultsNumCopies = resultsNumCopies;
        this._totalPrice = totalPrice;
    }
    /**
     * Getter resultsPlace
     * @return {ResultsPlace}
     */
    get resultsPlace() {
        return this._resultsPlace;
    }
    /**
     * Getter resultsDate
     * @return {ResultsDate}
     */
    get resultsDate() {
        return this._resultsDate;
    }
    /**
     * Getter resultsCopies
     * @return {ResultsCopies}
     */
    get resultsCopies() {
        return this._resultsCopies;
    }
    /**
     * Getter resultsNumCopies
     * @return {number}
     */
    get resultsNumCopies() {
        return this._resultsNumCopies;
    }
    /**
     * Getter totalPrice
     * @return {number}
     */
    get totalPrice() {
        return this._totalPrice;
    }
    /**
     * Setter resultsPlace
     * @param {ResultsPlace} value
     */
    set resultsPlace(value) {
        this._resultsPlace = value;
    }
    /**
     * Setter resultsDate
     * @param {ResultsDate} value
     */
    set resultsDate(value) {
        this._resultsDate = value;
    }
    /**
     * Setter resultsCopies
     * @param {ResultsCopies} value
     */
    set resultsCopies(value) {
        this._resultsCopies = value;
    }
    /**
     * Setter resultsNumCopies
     * @param {number} value
     */
    set resultsNumCopies(value) {
        this._resultsNumCopies = value;
    }
    /**
     * Setter totalPrice
     * @param {number} value
     */
    set totalPrice(value) {
        this._totalPrice = value;
    }
}


/***/ }),

/***/ "./src/app/model/ResultsCopies.ts":
/*!****************************************!*\
  !*** ./src/app/model/ResultsCopies.ts ***!
  \****************************************/
/*! exports provided: ResultsCopies */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsCopies", function() { return ResultsCopies; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class ResultsCopies {
    /**
     *Creates an instance of ResultPlace.
     * @param {number} id
     * @param {string} description
     * @param {number} price
     * @memberof ResultsCopies
     */
    constructor(id, description, price) {
        this._id = id;
        this._description = description;
        this._price = price;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Getter price
     * @return {number}
     */
    get price() {
        return this._price;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
    /**
     * Setter price
     * @param {number} value
     */
    set price(value) {
        this._price = value;
    }
}


/***/ }),

/***/ "./src/app/model/ResultsDate.ts":
/*!**************************************!*\
  !*** ./src/app/model/ResultsDate.ts ***!
  \**************************************/
/*! exports provided: ResultsDate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsDate", function() { return ResultsDate; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class ResultsDate {
    /**
     *Creates an instance of ResultPlace.
     * @param {number} id
     * @param {string} description
     * @param {number} price
     * @memberof ResultsDate
     */
    constructor(id, description, price) {
        this._id = id;
        this._description = description;
        this._price = price;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Getter price
     * @return {number}
     */
    get price() {
        return this._price;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
    /**
     * Setter price
     * @param {number} value
     */
    set price(value) {
        this._price = value;
    }
}


/***/ }),

/***/ "./src/app/model/ResultsPlace.ts":
/*!***************************************!*\
  !*** ./src/app/model/ResultsPlace.ts ***!
  \***************************************/
/*! exports provided: ResultsPlace */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsPlace", function() { return ResultsPlace; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class ResultsPlace {
    /**
     *Creates an instance of ResultPlace.
     * @param {number} id
     * @param {string} description
     * @param {number} price
     * @memberof ResultPlace
     */
    constructor(id, description, price) {
        this._id = id;
        this._description = description;
        this._price = price;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Getter price
     * @return {number}
     */
    get price() {
        return this._price;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
    /**
     * Setter price
     * @param {number} value
     */
    set price(value) {
        this._price = value;
    }
}


/***/ }),

/***/ "./src/app/model/Sequence.ts":
/*!***********************************!*\
  !*** ./src/app/model/Sequence.ts ***!
  \***********************************/
/*! exports provided: Sequence */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sequence", function() { return Sequence; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class Sequence {
    /**
     *Creates an instance of Sequence.
     * @param {SequenceType} sequenceType
     * @param {string} sequence
     * @param {File} file
     * @memberof Sequence
     */
    constructor(sequenceType, sequence, file) {
        this._sequenceType = sequenceType;
        this._sequence = sequence;
        this._file = file;
    }
    /**
     * Getter sequenceType
     * @return {SequenceType}
     */
    get sequenceType() {
        return this._sequenceType;
    }
    /**
     * Getter sequence
     * @return {string}
     */
    get sequence() {
        return this._sequence;
    }
    /**
     * Getter file
     * @return {File}
     */
    get file() {
        return this._file;
    }
    /**
     * Setter sequenceType
     * @param {SequenceType} value
     */
    set sequenceType(value) {
        this._sequenceType = value;
    }
    /**
     * Setter sequence
     * @param {string} value
     */
    set sequence(value) {
        this._sequence = value;
    }
    /**
     * Setter file
     * @param {File} value
     */
    set file(value) {
        this._file = value;
    }
}


/***/ }),

/***/ "./src/app/model/SequenceType.ts":
/*!***************************************!*\
  !*** ./src/app/model/SequenceType.ts ***!
  \***************************************/
/*! exports provided: SequenceType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SequenceType", function() { return SequenceType; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class SequenceType {
    /**
     *Creates an instance of SequenceType.
     * @param {number} [id]
     * @param {string} [description]
     * @memberof SequenceType
     */
    constructor(id, description) {
        this._id = id;
        this._description = description;
    }
    /**
     * Getter id
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     * Getter description
     * @return {string}
     */
    get description() {
        return this._description;
    }
    /**
     * Setter id
     * @param {number} value
     */
    set id(value) {
        this._id = value;
    }
    /**
     * Setter description
     * @param {string} value
     */
    set description(value) {
        this._description = value;
    }
}


/***/ }),

/***/ "./src/app/model/User.ts":
/*!*******************************!*\
  !*** ./src/app/model/User.ts ***!
  \*******************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class User {
    /**
     *Creates an instance of User.
     * @param {string} name
     * @param {string} surname
     * @param {string} DNI
     * @param {string} email
     * @param {string} password
     * @param {string} phone
     * @param {string} birthdate
     * @param {string} zipcode
     * @param {string} city
     * @param {string} country
     * @memberof User
     */
    constructor(name, surname, DNI, email, password, phone, birthdate, zipcode, city, country) {
        this._name = name;
        this._surname = surname;
        this._DNI = DNI;
        this._email = email;
        this._password = password;
        this._phone = phone;
        this._birthdate = birthdate;
        this._zipcode = zipcode;
        this._city = city;
        this._country = country;
    }
    /**
     * Getter name
     * @return {string}
     */
    get name() {
        return this._name;
    }
    /**
     * Getter surname
     * @return {string}
     */
    get surname() {
        return this._surname;
    }
    /**
     * Getter DNI
     * @return {string}
     */
    get DNI() {
        return this._DNI;
    }
    /**
     * Getter email
     * @return {string}
     */
    get email() {
        return this._email;
    }
    /**
     * Getter password
     * @return {string}
     */
    get password() {
        return this._password;
    }
    /**
     * Getter phone
     * @return {string}
     */
    get phone() {
        return this._phone;
    }
    /**
     * Getter birthdate
     * @return {string}
     */
    get birthdate() {
        return this._birthdate;
    }
    /**
     * Getter zipcode
     * @return {string}
     */
    get zipcode() {
        return this._zipcode;
    }
    /**
     * Getter city
     * @return {string}
     */
    get city() {
        return this._city;
    }
    /**
     * Getter country
     * @return {string}
     */
    get country() {
        return this._country;
    }
    /**
     * Setter name
     * @param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     * Setter surname
     * @param {string} value
     */
    set surname(value) {
        this._surname = value;
    }
    /**
     * Setter DNI
     * @param {string} value
     */
    set DNI(value) {
        this._DNI = value;
    }
    /**
     * Setter email
     * @param {string} value
     */
    set email(value) {
        this._email = value;
    }
    /**
     * Setter password
     * @param {string} value
     */
    set password(value) {
        this._password = value;
    }
    /**
     * Setter phone
     * @param {string} value
     */
    set phone(value) {
        this._phone = value;
    }
    /**
     * Setter birthdate
     * @param {string} value
     */
    set birthdate(value) {
        this._birthdate = value;
    }
    /**
     * Setter zipcode
     * @param {string} value
     */
    set zipcode(value) {
        this._zipcode = value;
    }
    /**
     * Setter city
     * @param {string} value
     */
    set city(value) {
        this._city = value;
    }
    /**
     * Setter country
     * @param {string} value
     */
    set country(value) {
        this._country = value;
    }
    /**
     * It validates the letter of the DNI
     * @returns {boolean}
     * @memberof User
     */
    validateLetterDNI() {
        let result = true;
        let numsDNIintro = parseInt(this._DNI.substr(0, 8));
        let letterDNIintro = this._DNI.substr(8).toUpperCase();
        let rest = numsDNIintro % 23;
        let letters = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
        let letter = letters[rest];
        return letter == letterDNIintro;
    }
}


/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.css":
/*!*************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.ts":
/*!************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.ts ***!
  \************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PageNotFoundComponent = class PageNotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
};
PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-page-not-found',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./page-not-found.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/page-not-found/page-not-found.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/page-not-found/page-not-found.component.css")).default]
    })
], PageNotFoundComponent);



/***/ }),

/***/ "./src/app/results/results.component.css":
/*!***********************************************!*\
  !*** ./src/app/results/results.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc3VsdHMvcmVzdWx0cy5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/results/results.component.ts":
/*!**********************************************!*\
  !*** ./src/app/results/results.component.ts ***!
  \**********************************************/
/*! exports provided: ResultsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsComponent", function() { return ResultsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Results__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Results */ "./src/app/model/Results.ts");
/* harmony import */ var _model_ResultsDate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/ResultsDate */ "./src/app/model/ResultsDate.ts");
/* harmony import */ var _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/ResultsCopies */ "./src/app/model/ResultsCopies.ts");
/* harmony import */ var _services_results_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/results.service */ "./src/app/services/results.service.ts");

//This is the controller of the results.component.html





//import { CookieService } from 'ngx-cookie-service';
let ResultsComponent = class ResultsComponent {
    /**
     *Creates an instance of ResultsComponent.
     * @param {CookieService} cookieService
     * @param {ResultsService} resultsService
     * @memberof ResultsComponent
     */
    constructor(//private cookieService: CookieService,
    resultsService) {
        this.resultsService = resultsService;
        this.resultsPlaces = [];
        this.resultsDates = [];
        this.resultsCopies = [];
    }
    //This function starts when the page is loaded
    ngOnInit() {
        this.resultsPlaces = this.resultsService.createPlaces();
        this.resultsDates = this.resultsService.createDates();
        this.resultsCopies = this.resultsService.createCopies();
    }
    ngOnChanges(changes) {
        this.initializeForm();
        //this.getCookie();
        setTimeout(() => {
            this.calculateTotalPrice();
        });
    }
    //This function prints the object Results in console
    results() {
        //this.cookieService.set("objResults", JSON.stringify(this.objResults));
        console.log(this.objResults);
    }
    //This function initializes the variables when the form is loaded
    initializeForm() {
        this.resultsForm.reset();
        this.resultsForm.form.markAsPristine();
        this.resultsForm.form.markAsUntouched();
        //If this.objResults is null is because no input param has been received. Otherwise, we should be careful not to smash the parameter
        if (!this.objResults) {
            this.objResults = new _model_Results__WEBPACK_IMPORTED_MODULE_2__["Results"]();
            this.objResults.resultsPlace = this.resultsPlaces[0]; //the first place of the array will appear selected as default
            this.objResults.resultsDate = this.resultsDates[0]; //the first date of the array will appear marked as default
            this.objResults.resultsCopies = this.resultsCopies[0]; //the first answer of the array will appear selected as default
        }
        else {
            if (this.resultsPlaces.length == 0)
                this.resultsPlaces = this.resultsService.createPlaces();
            this.objResults.resultsPlace = this.resultsPlaces[this.objResults.resultsPlace.id];
            this.objResults.resultsDate = new _model_ResultsDate__WEBPACK_IMPORTED_MODULE_3__["ResultsDate"](this.objResults.resultsDate.id, this.objResults.resultsDate.description, this.objResults.resultsDate.price);
            this.objResults.resultsCopies = new _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_4__["ResultsCopies"](this.objResults.resultsCopies.id, this.objResults.resultsCopies.description, this.objResults.resultsCopies.price);
        }
    }
    /*getCookie() {
      if (this.cookieService.check("objResults")) {
        this.cookieObj = JSON.parse(this.cookieService.get("objResults"));
        Object.assign(this.objResults, this.cookieObj);
  
        if (this.cookieObj.resultsPlace) {
          this.objResults.resultsPlace =
            this.resultsPlaces[this.cookieObj.resultsPlace.id];
        }
  
        if (this.cookieObj.resultsDate) {
          this.objResults.resultsDate =
            this.resultsDates[this.cookieObj.resultsDate.id];
        }
  
        if (this.cookieObj.resultsCopies) {
          this.objResults.resultsCopies =
            this.resultsCopies[this.cookieObj.resultsCopies.id];
        }
      }
    }*/
    //This function calculates the total price
    calculateTotalPrice() {
        this.objResults.totalPrice = 97;
        this.objResults.totalPrice += this.objResults.resultsPlace.price + this.objResults.resultsDate.price + this.objResults.resultsCopies.price;
    }
};
ResultsComponent.ctorParameters = () => [
    { type: _services_results_service__WEBPACK_IMPORTED_MODULE_5__["ResultsService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ResultsComponent.prototype, "objResults", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('resultsForm', null)
], ResultsComponent.prototype, "resultsForm", void 0);
ResultsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-results',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./results.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/results/results.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./results.component.css */ "./src/app/results/results.component.css")).default]
    })
], ResultsComponent);



/***/ }),

/***/ "./src/app/resultsmanagement/resultsmanagement.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/resultsmanagement/resultsmanagement.component.css ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc3VsdHNtYW5hZ2VtZW50L3Jlc3VsdHNtYW5hZ2VtZW50LmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/resultsmanagement/resultsmanagement.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/resultsmanagement/resultsmanagement.component.ts ***!
  \******************************************************************/
/*! exports provided: ResultsmanagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsmanagementComponent", function() { return ResultsmanagementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Results__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Results */ "./src/app/model/Results.ts");
/* harmony import */ var _services_results_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/results.service */ "./src/app/services/results.service.ts");
/* harmony import */ var _model_ResultsPlace__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/ResultsPlace */ "./src/app/model/ResultsPlace.ts");
/* harmony import */ var _model_ResultsDate__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/ResultsDate */ "./src/app/model/ResultsDate.ts");
/* harmony import */ var _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../model/ResultsCopies */ "./src/app/model/ResultsCopies.ts");

//This is the controller of the resultsManagement.html






let ResultsmanagementComponent = class ResultsmanagementComponent {
    /**
     *Creates an instance of ResultsmanagementComponent.
     * @param {ResultsService} resultsService
     * @memberof ResultsmanagementComponent
     */
    constructor(resultsService) {
        this.resultsService = resultsService;
        //Filter properties
        this.priceFilter = 0;
        //Pagination properties
        this.currentPage = 1;
        this.itemsPerPage = 10;
        this.results = [];
        this.resultsFiltered = [];
    }
    //This function starts when the page is loaded
    ngOnInit() {
        this.results = this.resultsService.generateResultsRandom();
        this.resultsFiltered = this.results;
        this.resultSelected = new _model_Results__WEBPACK_IMPORTED_MODULE_2__["Results"](new _model_ResultsPlace__WEBPACK_IMPORTED_MODULE_4__["ResultsPlace"](0, "", 0), new _model_ResultsDate__WEBPACK_IMPORTED_MODULE_5__["ResultsDate"](0, "", 0), new _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_6__["ResultsCopies"](0, "", 0), 0, 0);
        this.priceFilter = 121;
        this.itemsPerPage = 10;
        this.currentPage = 1;
    }
    //This function filters the results according to the params selected
    filter() {
        this.resultsFiltered = this.results.filter(result => {
            let priceValid = false;
            priceValid = (result.totalPrice <= this.priceFilter);
            return priceValid;
        });
    }
    //This function shows the result selected
    onClick(rv) {
        Object.assign(this.resultSelected, rv);
        //this.resultSelected = rv;
        console.log(rv);
    }
    //This function removes the result selected
    removeResult(rv) {
        this.results.splice(this.results.indexOf(rv), 1);
        this.resultsFiltered.splice(this.resultsFiltered.indexOf(rv), 1);
    }
};
ResultsmanagementComponent.ctorParameters = () => [
    { type: _services_results_service__WEBPACK_IMPORTED_MODULE_3__["ResultsService"] }
];
ResultsmanagementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-resultsmanagement',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./resultsmanagement.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/resultsmanagement/resultsmanagement.component.html")).default,
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./resultsmanagement.component.css */ "./src/app/resultsmanagement/resultsmanagement.component.css")).default]
    })
], ResultsmanagementComponent);



/***/ }),

/***/ "./src/app/sequence-entry/sequence-entry.component.css":
/*!*************************************************************!*\
  !*** ./src/app/sequence-entry/sequence-entry.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcXVlbmNlLWVudHJ5L3NlcXVlbmNlLWVudHJ5LmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/sequence-entry/sequence-entry.component.ts":
/*!************************************************************!*\
  !*** ./src/app/sequence-entry/sequence-entry.component.ts ***!
  \************************************************************/
/*! exports provided: SequenceEntryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SequenceEntryComponent", function() { return SequenceEntryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Sequence__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Sequence */ "./src/app/model/Sequence.ts");
/* harmony import */ var _services_sequence_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/sequence.service */ "./src/app/services/sequence.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.js");

//This is the controller of the sequence-entry.component.html




let SequenceEntryComponent = class SequenceEntryComponent {
    /**
     *Creates an instance of SequenceEntryComponent.
     * @param {CookieService} cookieService
     * @param {SequenceService} sequenceService
     * @memberof SequenceEntryComponent
     */
    constructor(cookieService, sequenceService) {
        this.cookieService = cookieService;
        this.sequenceService = sequenceService;
        this.sequenceTypes = [];
    }
    //This function starts when the page is loaded
    ngOnInit() {
        this.sequenceTypes = this.sequenceService.createSequenceTypes();
        this.initializeForm();
        this.getCookie();
    }
    //This function prints the cookies in the form and the object Sequence in console
    sequenceEntry() {
        this.cookieService.set("objSequence", JSON.stringify(this.objSequence));
        console.log(this.objSequence);
    }
    //This function initializes the variables when the form is loaded
    initializeForm() {
        this.sequenceEntryForm.reset();
        this.sequenceEntryForm.form.markAsPristine();
        this.sequenceEntryForm.form.markAsUntouched();
        this.objSequence = new _model_Sequence__WEBPACK_IMPORTED_MODULE_2__["Sequence"]();
        this.objSequence.sequenceType = this.sequenceTypes[0]; //the first sequenceType of the array will appear selected as default
    }
    //This fuction gets the values of the varibales and inserts them in the cookie object
    getCookie() {
        if (this.cookieService.check("objSequence")) {
            this.cookieObj = JSON.parse(this.cookieService.get("objSequence"));
            Object.assign(this.objSequence, this.cookieObj);
        }
        if (this.cookieObj && this.cookieObj._sequenceType) {
            this.objSequence.sequenceType =
                this.sequenceTypes[this.cookieObj._sequenceType._id];
        }
    }
    alertEvent(event) {
        alert("The form have been send");
        console.log(event);
    }
};
SequenceEntryComponent.ctorParameters = () => [
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_4__["CookieService"] },
    { type: _services_sequence_service__WEBPACK_IMPORTED_MODULE_3__["SequenceService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('sequenceEntryForm', null)
], SequenceEntryComponent.prototype, "sequenceEntryForm", void 0);
SequenceEntryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sequence-entry',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sequence-entry.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/sequence-entry/sequence-entry.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sequence-entry.component.css */ "./src/app/sequence-entry/sequence-entry.component.css")).default]
    })
], SequenceEntryComponent);



/***/ }),

/***/ "./src/app/services/feedback.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/feedback.service.ts ***!
  \**********************************************/
/*! exports provided: FeedbackService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackService", function() { return FeedbackService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Places__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Places */ "./src/app/model/Places.ts");
/* harmony import */ var _model_Rate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/Rate */ "./src/app/model/Rate.ts");
/* harmony import */ var _model_Recommendation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/Recommendation */ "./src/app/model/Recommendation.ts");





let FeedbackService = class FeedbackService {
    constructor() { }
    //it creates the array of the possible places
    createPlaces() {
        let places = [];
        //A temporary array of places is builded due to the lack of database
        let placesAux = ["Friend / family recommendation", "Internet", "TV", "Radio", "Billboard", "Other"];
        for (let i = 0; i < placesAux.length; i++) {
            places.push(new _model_Places__WEBPACK_IMPORTED_MODULE_2__["Places"](i, placesAux[i]));
        }
        return places;
    }
    //it creates the array of the possible rates
    createRates() {
        let rates = [];
        //A temporary array of rates is builded due to the lack of database
        let ratesAux = ["Excellent", "Very good", "Good", "Regular", "Bad", "Very bad"];
        for (let i = 0; i < ratesAux.length; i++) {
            rates.push(new _model_Rate__WEBPACK_IMPORTED_MODULE_3__["Rate"](i, ratesAux[i]));
        }
        return rates;
    }
    //it creates the array of the possible answers
    createRecommendations() {
        let recommendations = [];
        //A temporary array of recommendations is builded due to the lack of database
        let recommendationsAux = ["Yes", "No"];
        for (let i = 0; i < recommendationsAux.length; i++) {
            recommendations.push(new _model_Recommendation__WEBPACK_IMPORTED_MODULE_4__["Recommendation"](i, recommendationsAux[i]));
        }
        return recommendations;
    }
};
FeedbackService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FeedbackService);



/***/ }),

/***/ "./src/app/services/results.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/results.service.ts ***!
  \*********************************************/
/*! exports provided: ResultsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsService", function() { return ResultsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_Results__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Results */ "./src/app/model/Results.ts");
/* harmony import */ var _model_ResultsPlace__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/ResultsPlace */ "./src/app/model/ResultsPlace.ts");
/* harmony import */ var _model_ResultsDate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/ResultsDate */ "./src/app/model/ResultsDate.ts");
/* harmony import */ var _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/ResultsCopies */ "./src/app/model/ResultsCopies.ts");






let ResultsService = class ResultsService {
    constructor() { }
    //it creates the array of the possible places
    createPlaces() {
        let resultsPlaces = [];
        //A temporary array of places is builded due to the lack of database
        let placesAux = ["Laboratory", "Medical center", "Home delivery", "Email"];
        for (let i = 0; i < placesAux.length; i++) {
            resultsPlaces.push(new _model_ResultsPlace__WEBPACK_IMPORTED_MODULE_3__["ResultsPlace"](i, placesAux[i], i * 3 + 1));
        }
        return resultsPlaces;
    }
    //it creates the array of the possible dates
    createDates() {
        let resultsDates = [];
        //A temporary array of dates is builded due to the lack of database
        let datesAux = ["When they are ready", "Within a month", "Within one week", "Next day"];
        for (let i = 0; i < datesAux.length; i++) {
            resultsDates.push(new _model_ResultsDate__WEBPACK_IMPORTED_MODULE_4__["ResultsDate"](i, datesAux[i], i * 3 + 1));
        }
        return resultsDates;
    }
    //it creates the array of the possible answers
    createCopies() {
        let resultsCopies = [];
        //A temporary array of answers is builded due to the lack of database
        let copiesAux = ["No", "Yes"];
        for (let i = 0; i < copiesAux.length; i++) {
            resultsCopies.push(new _model_ResultsCopies__WEBPACK_IMPORTED_MODULE_5__["ResultsCopies"](i, copiesAux[i], i * 3 + 1));
        }
        return resultsCopies;
    }
    //it generates random results
    generateResultsRandom() {
        let results = [];
        let randomPlace;
        let randomDate;
        let randomCopies;
        let randomNumCopies;
        let randomPrice;
        let result;
        for (let i = 0; i < 200; i++) {
            randomPlace = Math.floor(Math.random() * 3);
            randomDate = Math.floor(Math.random() * 4);
            randomCopies = Math.floor(Math.random() * 2);
            randomNumCopies = Math.floor(Math.random() * 6);
            randomPrice = Math.floor(Math.random() * 121);
            result = new _model_Results__WEBPACK_IMPORTED_MODULE_2__["Results"](this.createPlaces()[randomPlace], this.createDates()[randomDate], this.createCopies()[randomCopies], randomNumCopies, randomPrice);
            results.push(result);
        }
        return results;
    }
};
ResultsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ResultsService);



/***/ }),

/***/ "./src/app/services/sequence.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/sequence.service.ts ***!
  \**********************************************/
/*! exports provided: SequenceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SequenceService", function() { return SequenceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_SequenceType__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/SequenceType */ "./src/app/model/SequenceType.ts");



let SequenceService = class SequenceService {
    constructor() { }
    //it creates the array with the possible sequence types
    createSequenceTypes() {
        let sequenceTypes = [];
        //A temporary array of sequenceTypes is builded due to the lack of database
        let sequenceTypesAux = ["DNA", "RNA", "Protein"];
        for (let i = 0; i < sequenceTypesAux.length; i++) {
            sequenceTypes.push(new _model_SequenceType__WEBPACK_IMPORTED_MODULE_2__["SequenceType"](i, sequenceTypesAux[i]));
        }
        return sequenceTypes;
    }
};
SequenceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], SequenceService);



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UserService = class UserService {
    constructor() { }
};
UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserService);



/***/ }),

/***/ "./src/app/user-entry/user-entry.component.css":
/*!*****************************************************!*\
  !*** ./src/app/user-entry/user-entry.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItZW50cnkvdXNlci1lbnRyeS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/user-entry/user-entry.component.ts":
/*!****************************************************!*\
  !*** ./src/app/user-entry/user-entry.component.ts ***!
  \****************************************************/
/*! exports provided: UserEntryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserEntryComponent", function() { return UserEntryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _model_User__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/User */ "./src/app/model/User.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.js");

//This is the controller of the user-entry.component.html





let UserEntryComponent = class UserEntryComponent {
    /**
     *Creates an instance of UserEntryComponent.
     * @param {DatePipe} datePipe
     * @param {CookieService} cookieService
     * @param {UserService} userService
     * @memberof UserEntryComponent
     */
    constructor(datePipe, cookieService, userService) {
        this.datePipe = datePipe;
        this.cookieService = cookieService;
        this.userService = userService;
    }
    //This function starts when the page is loaded
    ngOnInit() {
        this.initializeForm();
        this.getCookie();
    }
    //This function prints the cookies in the form and the object User in console
    userEntry() {
        this.cookieService.set("objUser", JSON.stringify(this.objUser));
        console.log(this.objUser); //it prints in console the object User
    }
    //This function initializes the variables when the form is loaded
    initializeForm() {
        this.userEntryForm.reset();
        this.userEntryForm.form.markAsPristine();
        this.userEntryForm.form.markAsUntouched();
        this.objUser = new _model_User__WEBPACK_IMPORTED_MODULE_3__["User"]();
        this.objUser.birthdate = this.datePipe.transform(new Date(), 'yyyy-MM-dd'); //this is to initialize the birthdate to todays's date
    }
    //This fuction gets the values of the varibales and inserts them in the cookie object
    getCookie() {
        if (this.cookieService.check("objUser")) {
            this.cookieObj = JSON.parse(this.cookieService.get("objUser"));
            Object.assign(this.objUser, this.cookieObj);
        }
    }
    alertEvent(event) {
        alert("The form have been send");
        console.log(event);
    }
    resetEvent(event) {
        alert("Form reseted");
        this.userEntryForm.reset();
    }
};
UserEntryComponent.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"] },
    { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('userEntryForm', null)
], UserEntryComponent.prototype, "userEntryForm", void 0);
UserEntryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-user-entry',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./user-entry.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user-entry/user-entry.component.html")).default,
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./user-entry.component.css */ "./src/app/user-entry/user-entry.component.css")).default]
    })
], UserEntryComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/tarda/Laura/angular/m6_angular_pt2/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map