import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { genomeManagementComponent } from './genome-management.component';

describe('genomeManagementComponent', () => {
  let component: genomeManagementComponent;
  let fixture: ComponentFixture<genomeManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [genomeManagementComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(genomeManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
