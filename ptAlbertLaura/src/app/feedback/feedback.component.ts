//This is the controller of the feedback.component.html
import { Component, OnInit, ViewChild } from '@angular/core';

import { Feedback } from '../model/Feedback';
import { Places } from '../model/Places';
import { Rate } from '../model/Rate';
import { Recommendation } from '../model/Recommendation';

import { FeedbackService } from '../services/feedback.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  //Properties
  objFeedback: Feedback;
  places: Places[] = [];
  rates: Rate[] = [];
  recommendations: Recommendation[] = [];

  cookieObj: any;

  @ViewChild('feedbackForm', null)
  feedbackForm: HTMLFormElement;

  @ViewChild('divCheckBox', null)
  divCheckBox: HTMLFormElement;

  /**
   *Creates an instance of FeedbackComponent.
   * @param {CookieService} cookieService
   * @param {FeedbackService} feedbackService
   * @memberof FeedbackComponent
   */
  constructor(private cookieService: CookieService,
    private feedbackService: FeedbackService) { }

  //This function starts when the page is loaded
  ngOnInit() {
    this.places = this.feedbackService.createPlaces();
    this.rates = this.feedbackService.createRates();
    this.recommendations = this.feedbackService.createRecommendations();
    this.initializeForm();
    this.getCookie();
  }

  //This function begins after the component view is initailized
  ngAfterViewInit() {
    this.getCookieCheckBox();
  }

  //This function prints the cookies in the form and the object Feedback in console
  feedback(): void {
    this.cookieService.set("objFeedback", JSON.stringify(this.objFeedback));
    console.log(this.objFeedback);
  }

  //This function initializes the variables when the form is loaded
  initializeForm() {
    this.feedbackForm.reset();
    this.feedbackForm.form.markAsPristine();
    this.feedbackForm.form.markAsUntouched();

    this.objFeedback = new Feedback();
    this.objFeedback.places = [];
    this.objFeedback.rate = this.rates[0]; //the first rate of the array will appear marked as default
    this.objFeedback.recommendation = this.recommendations[0]; //the first answer of the array will appear selected as default
  }

  //This fuction gets the values of the varibales and inserts them in the cookie object
  getCookie() {
    if (this.cookieService.check("objFeedback")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objFeedback"));
      /**
     * Copy the values of all of the enumerable own properties from one or more source objects to a
     * target object. Returns the target object.
     * @param target The target object to copy to.
     * @param source The source object from which to copy properties.
     */
      Object.assign(this.objFeedback, this.cookieObj);

      if (this.cookieObj && this.cookieObj._rate) {
        this.objFeedback.rate =
          this.rates[this.cookieObj._rate._id];
      }

      if (this.cookieObj && this.cookieObj._recommendation) {
        this.objFeedback.recommendation =
          this.recommendations[this.cookieObj._recommendation._id];
      }

      this.objFeedback.places = [];
      for (let placesAux of this.cookieObj._places) {
        this.objFeedback.places.push(
          this.places[placesAux._id]);
      }
    }
  }

  //This function is the same as previos but only for checkboxes
  getCookieCheckBox(){
    if(this.cookieObj){
      for(let placesAux of this.cookieObj._places){
        this.divCheckBox.nativeElement.
        children[placesAux._id].children[0].checked=true;
      }
    }
  }

  //if the array of places is empty, it fills it. Otherwise, it empties it
  addRemovePlaces(placesAux: Places): void {
    //indexOf() method returns the first index at which a given 
    //element can be found in the array, or -1 if it is not present.
    let myIndex: number = this.objFeedback.places.indexOf(placesAux);

    if (myIndex == -1) {
      this.objFeedback.places.push(placesAux);
    } else {
      this.objFeedback.places.splice(myIndex, 1);
    }
  }
}
