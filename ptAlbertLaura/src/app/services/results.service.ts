import { Injectable } from '@angular/core';
import { Results } from '../model/Results';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';


@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor() { }

  //it creates the array of the possible places
  createPlaces(): ResultsPlace[] {
    let resultsPlaces: ResultsPlace[] = [];
    //A temporary array of places is builded due to the lack of database
    let placesAux: string[] = ["Laboratory", "Medical center", "Home delivery", "Email"];

    for (let i: number = 0; i < placesAux.length; i++) {
      resultsPlaces.push(new ResultsPlace
        (i, placesAux[i], i * 3 + 1));
    }
    
    return resultsPlaces;
  }

  //it creates the array of the possible dates
  createDates(): ResultsDate[] {
    let resultsDates: ResultsDate[] = [];
    //A temporary array of dates is builded due to the lack of database
    let datesAux: string[] =
      ["When they are ready", "Within a month", "Within one week", "Next day"];

    for (let i: number = 0; i < datesAux.length; i++) {
      resultsDates.push(new ResultsDate
        (i, datesAux[i], i * 3 + 1));
    }

    return resultsDates;
  }

  //it creates the array of the possible answers
  createCopies(): ResultsCopies[] {
    let resultsCopies: ResultsCopies[] = [];
    //A temporary array of answers is builded due to the lack of database
    let copiesAux: string[] = ["No", "Yes"];

    for (let i: number = 0; i < copiesAux.length; i++) {
      resultsCopies.push(new ResultsCopies
        (i, copiesAux[i], i * 3 + 1));
    }

    return resultsCopies;
  }

  //it generates random results
  generateResultsRandom(): Results[] {
    let results: Results[] = [];

    let randomPlace: number;
    let randomDate: number;
    let randomCopies: number;
    let randomNumCopies: number;
    let randomPrice: number;

    let result: Results;

    for (let i = 0; i < 200; i++) {
      randomPlace = Math.floor(Math.random() * 3);
      randomDate = Math.floor(Math.random() * 4);
      randomCopies = Math.floor(Math.random() * 2);
      randomNumCopies = Math.floor(Math.random() * 6);
      randomPrice = Math.floor(Math.random() * 121);

      result = new Results(i, this.createPlaces()[randomPlace],
        this.createDates()[randomDate],
        this.createCopies()[randomCopies],
        randomNumCopies,
        randomPrice);
      
      
     
      results.push(result)

    }
    
    return results;
  }
}
