import { Injectable } from '@angular/core';

import { Sequence } from '../model/Sequence';
import { SequenceType } from '../model/SequenceType';

@Injectable({
  providedIn: 'root'
})
export class SequenceService {

  constructor() { }

  //it creates the array with the possible sequence types
  createSequenceTypes(): SequenceType[] {
    let sequenceTypes: SequenceType[] = [];
    //A temporary array of sequenceTypes is builded due to the lack of database
    let sequenceTypesAux: string[] = ["DNA", "RNA", "Protein"];

    for (let i: number = 0; i < sequenceTypesAux.length; i++) {
      sequenceTypes.push(new SequenceType
        (i, sequenceTypesAux[i]));
    }
    
    return sequenceTypes;
  }
}
