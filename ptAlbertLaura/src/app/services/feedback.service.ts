import { Injectable } from '@angular/core';

import { Feedback } from '../model/Feedback';
import { Places } from '../model/Places';
import { Rate } from '../model/Rate';
import { Recommendation } from '../model/Recommendation';


@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor() { }

  //it creates the array of the possible places
  createPlaces(): Places[] {
    let places: Places[] = [];
    //A temporary array of places is builded due to the lack of database
    let placesAux: string[] = ["Friend / family recommendation", "Internet", "TV", "Radio", "Billboard", "Other"];

    for (let i: number = 0; i < placesAux.length; i++) {
      places.push(new Places(i, placesAux[i]));
    }

    return places;
  }

  //it creates the array of the possible rates
  createRates(): Rate[] {
    let rates: Rate[] = [];
    //A temporary array of rates is builded due to the lack of database
    let ratesAux: string[] =
      ["Excellent", "Very good", "Good", "Regular", "Bad", "Very bad"];

    for (let i: number = 0; i < ratesAux.length; i++) {
      rates.push(new Rate
        (i, ratesAux[i]));
    }

    return rates;
  }

  //it creates the array of the possible answers
  createRecommendations(): Recommendation[] {
    let recommendations: Recommendation[] = [];
    //A temporary array of recommendations is builded due to the lack of database
    let recommendationsAux: string[] = ["Yes", "No"];

    for (let i: number = 0; i < recommendationsAux.length; i++) {
      recommendations.push(new Recommendation
        (i, recommendationsAux[i]));
    }

    return recommendations;
  }
}
