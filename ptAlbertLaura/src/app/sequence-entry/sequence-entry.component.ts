//This is the controller of the sequence-entry.component.html
import { Component, OnInit, ViewChild } from '@angular/core';

import { Sequence } from '../model/Sequence';
import { SequenceType } from '../model/SequenceType';

import { SequenceService } from '../services/sequence.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-sequence-entry',
  templateUrl: './sequence-entry.component.html',
  styleUrls: ['./sequence-entry.component.css']
})
export class SequenceEntryComponent implements OnInit {

  //Properties
  objSequence: Sequence;
  sequenceTypes: SequenceType[] = [];

  cookieObj: any;

  @ViewChild('sequenceEntryForm', null)
  sequenceEntryForm: HTMLFormElement;

  /**
   *Creates an instance of SequenceEntryComponent.
   * @param {CookieService} cookieService
   * @param {SequenceService} sequenceService
   * @memberof SequenceEntryComponent
   */
  constructor(
    private cookieService: CookieService,
    private sequenceService: SequenceService) { }

  //This function starts when the page is loaded
  ngOnInit() {
    this.sequenceTypes = this.sequenceService.createSequenceTypes();
    this.initializeForm();
    this.getCookie();
  }

  //This function prints the cookies in the form and the object Sequence in console
  sequenceEntry(): void {
    this.cookieService.set("objSequence", JSON.stringify(this.objSequence));
    console.log(this.objSequence);
  }

  //This function initializes the variables when the form is loaded
  initializeForm() {
    this.sequenceEntryForm.reset();
    this.sequenceEntryForm.form.markAsPristine();
    this.sequenceEntryForm.form.markAsUntouched();

    this.objSequence = new Sequence();
    this.objSequence.sequenceType = this.sequenceTypes[0]; //the first sequenceType of the array will appear selected as default
  }

  //This fuction gets the values of the varibales and inserts them in the cookie object
  getCookie() {
    if (this.cookieService.check("objSequence")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objSequence"));
      Object.assign(this.objSequence, this.cookieObj);
    }
    
    if (this.cookieObj && this.cookieObj._sequenceType) {
      this.objSequence.sequenceType =
        this.sequenceTypes[this.cookieObj._sequenceType._id];
    }
  }
}
