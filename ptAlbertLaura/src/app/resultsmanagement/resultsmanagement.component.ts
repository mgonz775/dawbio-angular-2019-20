//This is the controller of the resultsManagement.html
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Results } from '../model/Results';
import { ResultsService } from '../services/results.service';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';

@Component({
  selector: 'app-resultsmanagement',
  templateUrl: './resultsmanagement.component.html',
  styleUrls: ['./resultsmanagement.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsmanagementComponent implements OnInit {

  //Filter properties
  priceFilter: number = 0;
  
  //Pagination properties
  currentPage: number = 1;
  itemsPerPage: number = 10;

  results: Results[] = [];
  resultsFiltered: Results[] = [];
  resultSelected: Results;

  /**
   *Creates an instance of ResultsmanagementComponent.
   * @param {ResultsService} resultsService
   * @memberof ResultsmanagementComponent
   */
  constructor(private resultsService: ResultsService) {
  }

  //This function starts when the page is loaded
  ngOnInit() {
    this.results = this.resultsService.generateResultsRandom();
    this.resultsFiltered = this.results;
    console.log(this.resultsFiltered);
    
    this.resultSelected = new Results(-1, new ResultsPlace(0, "", 0), new ResultsDate(0, "",0),new ResultsCopies(0,"",0), 0, 0);

    this.priceFilter = 121;
    this.itemsPerPage = 10;
    this.currentPage = 1;
  }

  //This function filters the results according to the params selected
  filter() {
    this.resultsFiltered = this.results.filter(result => {
      let priceValid: boolean = false;
      priceValid = (result.totalPrice <= this.priceFilter);
      
      return priceValid;
    })
  }

  //This function shows the result selected
  onClick(rv: Results) {
    //Object.assign(this.resultSelected, rv);
    console.log(rv);
    console.log(this.resultsFiltered);
    
    debugger;
    this.resultSelected = this.resultsFiltered[this.resultsFiltered.map(e=>e.id).indexOf(rv.id)];
    
    console.log(this.resultsFiltered[this.resultsFiltered.map(e=>e.id).indexOf(rv.id)]);
     
    // console.log(this.resultsFiltered[this.resultsFiltered.map(e=>e.id).indexOf(rv.id)]);
    
    // this.resultSelected = new Results(rv.id, new ResultsPlace(rv.resultsPlace.id, rv.resultsPlace.description, rv.resultsPlace.price), 
    //   new ResultsDate(rv.resultsDate.id, rv.resultsDate.description, rv.resultsDate.price), 
    //   new ResultsCopies(rv.resultsCopies.id, rv.resultsCopies.description, rv.resultsCopies.price), rv.resultsNumCopies, rv.totalPrice);
    console.log(this.resultSelected);
    
    // this.resultSelected.resultsPlace = new ResultsPlace(rv.resultsPlace.id, rv.resultsPlace.description, rv.resultsPlace.price);
    // this.resultSelected.resultsDate = new ResultsDate(rv.resultsDate.id, rv.resultsDate.description, rv.resultsDate.price);
    // this.resultSelected.resultsCopies = new ResultsCopies(rv.resultsCopies.id, rv.resultsCopies.description, rv.resultsCopies.price);

    //this.resultSelected = rv;
  }
  
  //This function removes the result selected
  removeResult(rv: Results) {
    
    this.results.splice(this.results.indexOf(rv), 1);
    this.resultsFiltered.splice(this.resultsFiltered.indexOf(rv), 1);
  }
}
