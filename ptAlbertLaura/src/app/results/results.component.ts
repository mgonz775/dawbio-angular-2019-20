//This is the controller of the results.component.html
import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { Results } from '../model/Results';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';

import { ResultsService } from '../services/results.service';
//import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  //Properties
  @Input() objResults: Results;
  resultsPlaces: ResultsPlace[] = [];
  resultsDates: ResultsDate[] = [];
  resultsCopies: ResultsCopies[] = [];

  //cookieObj: any;

  @ViewChild('resultsForm', null)
  resultsForm: HTMLFormElement;

  /**
   *Creates an instance of ResultsComponent.
   * @param {CookieService} cookieService
   * @param {ResultsService} resultsService
   * @param {ChangeDetectorRef} cdref
   * @memberof ResultsComponent
   */
  constructor(//private cookieService: CookieService,
    private resultsService: ResultsService) { }

  //This function starts when the page is loaded
  ngOnInit() {
    this.resultsPlaces = this.resultsService.createPlaces();
    this.resultsDates = this.resultsService.createDates();
    this.resultsCopies = this.resultsService.createCopies();
    //this.getCookie();
  }

  ngOnChanges(changes: any){
    this.initializeForm();
    setTimeout(() => {
      this.calculateTotalPrice();
    })
  }

  /*ngAfterViewChecked() {
    
    this.calculateTotalPrice();
    this.cdref.detectChanges();

  }*/

  //This function prints the object Results in console
  results(): void {
    //this.cookieService.set("objResults", JSON.stringify(this.objResults));
    console.log(this.objResults);
  }

  //This function initializes the variables when the form is loaded
  initializeForm() {
    this.resultsForm.reset();
    this.resultsForm.form.markAsPristine();
    this.resultsForm.form.markAsUntouched();

    //If this.objResults is null is because no input param has been received. Otherwise, we should be careful not to smash the parameter
    if (!this.objResults) {
      this.objResults = new Results();  
      this.objResults.resultsPlace = this.resultsPlaces[0]; //the first place of the array will appear selected as default
      this.objResults.resultsDate = this.resultsDates[0]; //the first date of the array will appear marked as default
      this.objResults.resultsCopies = this.resultsCopies[0]; //the first answer of the array will appear selected as default
    }else{
      if(this.resultsPlaces.length==0) this.resultsPlaces = this.resultsService.createPlaces();     
      this.objResults.resultsPlace = this.resultsPlaces[this.objResults.resultsPlace.id];
    }
  }
  /*getCookie() {
    if (this.cookieService.check("objResults")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objResults"));
      Object.assign(this.objResults, this.cookieObj);

      if (this.cookieObj._resultsPlace) {
        this.objResults.resultsPlace =
          this.resultsPlaces[this.cookieObj._resultsPlace.id];
      }

      if (this.cookieObj._resultsDate) {
        this.objResults.resultsDate =
          this.resultsDates[this.cookieObj._resultsDate.id];
      }

      if (this.cookieObj._resultsCopies) {
        this.objResults.resultsCopies =
          this.resultsCopies[this.cookieObj.resultsCopies.id];
      }
    }
  }*/

  //This function calculates the total price
  calculateTotalPrice(): void {
    this.objResults.totalPrice = 97;
    console.log(this.objResults);
    
    this.objResults.totalPrice += this.objResults.resultsPlace.price + this.objResults.resultsDate.price + this.objResults.resultsCopies.price;
  }
}
