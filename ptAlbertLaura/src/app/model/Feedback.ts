import { Places } from './Places';
import { Rate } from './Rate';
import { Recommendation } from './Recommendation';

export class Feedback {

    //Properties
    private _name: string;
    private _surname: string;
    private _email: string;
    private _places: Places[];
    private _rate: Rate;
    private _recommendation: Recommendation;
    private _comments: string;

	/**
     *Creates an instance of Feedback.
     * @param {string} name
     * @param {string} surname
     * @param {string} email
     * @param {Places[]} places
     * @param {Rate} rate
     * @param {Recommendation} recommendation
     * @param {string} comments
     * @memberof Feedback
     */
    constructor(name?: string, surname?: string, email?: string, places?: Places[], rate?: Rate, recommendation?: Recommendation, comments?: string) {
		this._name = name;
		this._surname = surname;
		this._email = email;
		this._places = places;
		this._rate = rate;
		this._recommendation = recommendation;
		this._comments = comments;
    }

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter surname
     * @return {string}
     */
	public get surname(): string {
		return this._surname;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter places
     * @return {Places[]}
     */
	public get places(): Places[] {
		return this._places;
	}

    /**
     * Getter rate
     * @return {Rate}
     */
	public get rate(): Rate {
		return this._rate;
	}

    /**
     * Getter recommendation
     * @return {Recommendation}
     */
	public get recommendation(): Recommendation {
		return this._recommendation;
	}

    /**
     * Getter comments
     * @return {string}
     */
	public get comments(): string {
		return this._comments;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {string} value
     */
	public set surname(value: string) {
		this._surname = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter places
     * @param {Places[]} value
     */
	public set places(value: Places[]) {
		this._places = value;
	}

    /**
     * Setter rate
     * @param {Rate} value
     */
	public set rate(value: Rate) {
		this._rate = value;
	}

    /**
     * Setter recommendation
     * @param {Recommendation} value
     */
	public set recommendation(value: Recommendation) {
		this._recommendation = value;
	}

    /**
     * Setter comments
     * @param {string} value
     */
	public set comments(value: string) {
		this._comments = value;
	}
}
