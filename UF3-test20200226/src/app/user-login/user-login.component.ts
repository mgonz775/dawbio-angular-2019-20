import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { ReservationService } from '../services/reservation.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  validUserData: boolean = true;
  userConnected: User;
  users: User[];

  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    this.userConnected = new User();
  }

  connection(){
    //subscribe receives 3 optional callback functions
    //subscribe(data, error, end)
    //data: function parameters
    //error: error returned by the function
    //end: actions to bootstrap after the execution of the method
    this.reservationService.userConnection
      (this.userConnected).subscribe(outPutData => 
        console.log(outPutData));
  }

}
