import { Component, OnInit } from '@angular/core';
import { Protein } from '../model/protein/Protein';
import { ComponentOfProtein } from '../model/protein/componentOfProtein';
import { TypeOfProtein } from '../model/protein/typeOfProtein';
import { FunctionModel } from '../model/protein/functionModel';

@Component({
  selector: 'app-protein',
  templateUrl: './protein.component.html',
  styleUrls: ['./protein.component.css']
})
export class ProteinComponent implements OnInit {

  //properties
  objProtein: Protein;
  proteinType: string;
  componentOfProteins: ComponentOfProtein[]=[];
  typeOfProteins: TypeOfProtein[]=[];
  functionOfProteins: FunctionModel[]=[];

  //constructor
  constructor() { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * instance the objProtein and set his properties to default values
   */
  ngOnInit() {
    this.createComponentOfProtein();
    this.createTypeOfProtein();
    this.createFunctionOfProtein();
    this.objProtein = new Protein();
    this.objProtein.$componentOfProtein = this.componentOfProteins[0];
    this.objProtein.$typeOfProtein = this.typeOfProteins[0];
    this.objProtein.$functionOfProtein = [];
  }

  /**
   * @name createComponentOfProtein
   * @description function that register the component of a protein in an array and push it to componentOfProteins (a property),
   */
  createComponentOfProtein(): void {
    let componentProteinAux: string[]=["Simple (aminoacids)", "Conjugated", "Derived"];

    for(let i:number=0; i<componentProteinAux.length;i++){
      this.componentOfProteins.push(new ComponentOfProtein
          (i,componentProteinAux[i]));
    }
  }

  /**
   * @name createTypeOfProtein
   * @description function that register the type of protein (animal or vegetable) and push it to typeOfProteins property.
   */
  createTypeOfProtein() {
    let typeProteinAux: string[]=["Animal",
    "Vegetable"];

    for(let i:number=0; i<typeProteinAux.length;i++){
      this.typeOfProteins.push(new TypeOfProtein
          (i,typeProteinAux[i]));
    }
  }

  /**
   * @name createFunctionOfProtein
   * @description function that register all functions of proteins in an array and push it to functionOfProteins array (a property)
   */
  createFunctionOfProtein(): void{
    let functionProtAux: string[]=["Hormonal",
    "Enzymatic","Structural", "Defensive", "Storage", "Conveyor",
    "Receiver", "Motor"];

    for(let i:number=0; i<functionProtAux.length;i++){
      this.functionOfProteins.push(new FunctionModel
          (i,functionProtAux[i]));
    }
  }

  /**
   * @name addRemoveFunctionProtein
   * @description function to control the check of checkbox; when we check some component or not. 
   */
  addRemoveFunctionProtein(FuncProt: FunctionModel): void{
    //indexOf() method returns the first index at which a given 
    //element can be found in the array, or -1 if it is not present.

    let myIndex: number = this.objProtein.$functionOfProtein.indexOf(FuncProt);

    if(myIndex==-1){
      this.objProtein.$functionOfProtein.push(FuncProt);
    }else{
      this.objProtein.$functionOfProtein.splice(myIndex,1);
    }
  }

  /**
   * @name protein
   * @description Method that shows the objChain and his properties in console
   */
  protein(): void {
    console.log(this.objProtein);
  }

}
