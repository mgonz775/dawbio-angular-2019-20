import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[chainMinLength]',
  providers: [{provide: NG_VALIDATORS, useExisting: ChainDirectiveDirective, multi: true}]
})

export class ChainDirectiveDirective {

  constructor() { }

  /**
   * @param {AbstractControl} formFieldValidate
   * @returns {{[key:string]: any}}
   * @memberof ChainDirectiveDirective
   */
  //The minimum of characters is 2. Length must be greater than 1.
  validate(formFieldValidate: AbstractControl): {[key:string]: any} {
    let validInput: boolean = false;
    if (formFieldValidate && formFieldValidate.value && formFieldValidate.value.length > 1) {
      validInput = true;
    }
    return validInput? null: {'isNotCorrect':true};

  }

}
