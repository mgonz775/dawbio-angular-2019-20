import { ComponentOfProtein } from './componentOfProtein';
import { TypeOfProtein } from './typeOfProtein';
import { FunctionModel } from './functionModel';

export class Protein {
    private protein_name: String;
    private componentOfProtein: ComponentOfProtein;
    private typeOfProtein: TypeOfProtein;
    private functionOfProtein: FunctionModel[];

	/**
     *Creates an instance of Protein.
     * @param {String} [$protein_name]
     * @param {ComponentOfProtein} [$componentOfProtein]
     * @param {TypeOfProtein} [$typeOfProtein]
     * @param {FunctionModel[]} [$functionOfProtein]
     * @memberof Protein
     */
    constructor($protein_name?: String, $componentOfProtein?: ComponentOfProtein, $typeOfProtein?: TypeOfProtein, $functionOfProtein?: FunctionModel[]) {
		this.protein_name = $protein_name;
		this.componentOfProtein = $componentOfProtein;
		this.typeOfProtein = $typeOfProtein;
		this.functionOfProtein = $functionOfProtein;
	}

    /**
     * Getter $protein_name
     * @return {String}
     */
	public get $protein_name(): String {
		return this.protein_name;
	}

    /**
     * Getter $componentOfProtein
     * @return {ComponentOfProtein}
     */
	public get $componentOfProtein(): ComponentOfProtein {
		return this.componentOfProtein;
	}

    /**
     * Getter $typeOfProtein
     * @return {TypeOfProtein}
     */
	public get $typeOfProtein(): TypeOfProtein {
		return this.typeOfProtein;
	}

    /**
     * Getter $functionOfProtein
     * @return {FunctionOfProtein[]}
     */
	public get $functionOfProtein(): FunctionModel[] {
		return this.functionOfProtein;
	}

    /**
     * Setter $protein_name
     * @param {String} value
     */
	public set $protein_name(value: String) {
		this.protein_name = value;
	}

    /**
     * Setter $componentOfProtein
     * @param {ComponentOfProtein} value
     */
	public set $componentOfProtein(value: ComponentOfProtein) {
		this.componentOfProtein = value;
	}

    /**
     * Setter $typeOfProtein
     * @param {TypeOfProtein} value
     */
	public set $typeOfProtein(value: TypeOfProtein) {
		this.typeOfProtein = value;
	}

    /**
     * Setter $functionOfProtein
     * @param {FunctionOfProtein[]} value
     */
	public set $functionOfProtein(value: FunctionModel[]) {
		this.functionOfProtein = value;
	}


}