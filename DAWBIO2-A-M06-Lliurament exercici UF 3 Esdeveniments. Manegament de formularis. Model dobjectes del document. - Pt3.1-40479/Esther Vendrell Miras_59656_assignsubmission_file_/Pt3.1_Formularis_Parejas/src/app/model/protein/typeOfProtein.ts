export class TypeOfProtein {
    private id: number;
    private type: string;

	/**
     *Creates an instance of TypeOfProtein.
     * @param {number} $id
     * @param {string} $type
     * @memberof TypeOfProtein
     */
    constructor($id: number, $type: string) {
		this.id = $id;
		this.type = $type;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $type
     * @return {string}
     */
	public get $type(): string {
		return this.type;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $type
     * @param {string} value
     */
	public set $type(value: string) {
		this.type = value;
	}

}