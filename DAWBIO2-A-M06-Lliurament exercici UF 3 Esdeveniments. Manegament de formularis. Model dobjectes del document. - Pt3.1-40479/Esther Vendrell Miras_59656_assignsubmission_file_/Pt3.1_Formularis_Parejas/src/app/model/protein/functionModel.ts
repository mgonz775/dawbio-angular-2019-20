export class FunctionModel {
    private id: number;
    private function: string;

	/**
     *Creates an instance of FunctionModel.
     * @param {number} $id
     * @param {string} $function
     * @memberof FunctionModel
     */
    constructor($id: number, $function: string) {
		this.id = $id;
		this.function = $function;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $function
     * @return {string}
     */
	public get $function(): string {
		return this.function;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $function
     * @param {string} value
     */
	public set $function(value: string) {
		this.function = value;
	}


}