export class SubTrend {

    private _id: number;
    private _trend: string;

	/**
     *Creates an instance of SubTrend.
     * @param {number} id
     * @param {string} trend
     * @memberof SubTrend
     */
    constructor(id: number, trend: string) {
		this._id = id;
		this._trend = trend;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter trend
     * @return {string}
     */
	public get trend(): string {
		return this._trend;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter trend
     * @param {string} value
     */
	public set trend(value: string) {
		this._trend = value;
	}

}
