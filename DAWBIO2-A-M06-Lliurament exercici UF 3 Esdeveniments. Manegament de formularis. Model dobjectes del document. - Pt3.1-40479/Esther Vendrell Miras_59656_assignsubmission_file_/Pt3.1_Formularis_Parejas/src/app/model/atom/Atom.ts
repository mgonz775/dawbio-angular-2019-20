import { Occurrence } from './Occurrence';
import { Orbital } from './Orbital';
import { State } from './State';
import { Trend } from './Trend';
import { SubTrend } from './SubTrend';

export class Atom {
    private _state: State;
    private _occurrence: Occurrence;
    private _orbital: Orbital[];
    private _trend: Trend;
    private _subtrend: SubTrend;



	/**
     *Creates an instance of Atom.
     * @param {State} state
     * @param {Occurrence} occurrence
     * @param {Orbital[]} orbital
     * @param {Trend} trend
     * @param {SubTrend} subtrend
     * @memberof Atom
     */
    constructor(state?: State, occurrence?: Occurrence, orbital?: Orbital[], trend?: Trend, subtrend?: SubTrend) {
		this._state = state;
		this._occurrence = occurrence;
		this._orbital = orbital;
		this._trend = trend;
		this._subtrend = subtrend;
	}

    /**
     * Getter state
     * @return {State}
     */
	public get state(): State {
		return this._state;
	}

    /**
     * Getter occurrence
     * @return {Occurrence}
     */
	public get occurrence(): Occurrence {
		return this._occurrence;
	}

    /**
     * Getter orbital
     * @return {Orbital[]}
     */
	public get orbital(): Orbital[] {
		return this._orbital;
	}

    /**
     * Getter trend
     * @return {Trend}
     */
	public get trend(): Trend {
		return this._trend;
	}

    /**
     * Getter subtrend
     * @return {SubTrend}
     */
	public get subtrend(): SubTrend {
		return this._subtrend;
	}

    /**
     * Setter state
     * @param {State} value
     */
	public set state(value: State) {
		this._state = value;
	}

    /**
     * Setter occurrence
     * @param {Occurrence} value
     */
	public set occurrence(value: Occurrence) {
		this._occurrence = value;
	}

    /**
     * Setter orbital
     * @param {Orbital[]} value
     */
	public set orbital(value: Orbital[]) {
		this._orbital = value;
	}

    /**
     * Setter trend
     * @param {Trend} value
     */
	public set trend(value: Trend) {
		this._trend = value;
	}

    /**
     * Setter subtrend
     * @param {SubTrend} value
     */
	public set subtrend(value: SubTrend) {
		this._subtrend = value;
	}

}