import { Essentiality } from './Essentiality';
import { ChainPropertie } from './ChainPropertie';

export class Aminoacid {

    private _essenciality: Essentiality;
    private _chainPropertie: ChainPropertie;

	/**
     *Creates an instance of Aminoacid.
     * @param {Essentiality} [essenciality]
     * @param {ChainPropertie} [chainPropertie]
     * @memberof Aminoacid
     */
    constructor(essenciality?: Essentiality, chainPropertie?: ChainPropertie) {
		this._essenciality = essenciality;
		this._chainPropertie = chainPropertie;
	}

    /**
     * Getter essenciality
     * @return {Essentiality}
     */
	public get essenciality(): Essentiality {
		return this._essenciality;
	}

    /**
     * Getter chainPropertie
     * @return {ChainPropertie}
     */
	public get chainPropertie(): ChainPropertie {
		return this._chainPropertie;
	}

    /**
     * Setter essenciality
     * @param {Essentiality} value
     */
	public set essenciality(value: Essentiality) {
		this._essenciality = value;
	}

    /**
     * Setter chainPropertie
     * @param {ChainPropertie} value
     */
	public set chainPropertie(value: ChainPropertie) {
		this._chainPropertie = value;
	}

	
    
}