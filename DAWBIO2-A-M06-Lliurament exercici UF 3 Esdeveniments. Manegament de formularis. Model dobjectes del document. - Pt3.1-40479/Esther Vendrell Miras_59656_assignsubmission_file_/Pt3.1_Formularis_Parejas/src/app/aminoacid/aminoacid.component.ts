//imports
import { Component, OnInit } from '@angular/core';
import { Aminoacid } from '../model/aminoacid/Aminoacid';
import { Essentiality} from '../model/aminoacid/Essentiality';
import { ChainPropertie} from '../model/aminoacid/ChainPropertie';
import {FullInfoAmino} from '../model/aminoacid/FullInfoAmino';

//Component, selector tag...
@Component({
  selector: 'app-aminoacid',
  templateUrl: './aminoacid.component.html',
  styleUrls: ['./aminoacid.component.css']
})
export class AminoacidComponent implements OnInit {
  
  //properties
  objAminoacid: Aminoacid;
  arr_essentiality: Essentiality[]=[];
  arr_chainProperties: ChainPropertie[]=[];
  arr_aa: FullInfoAmino[]=[];
  show_arr_aa: FullInfoAmino[]=[];

  //Constructor
  constructor() { }

  /**
   *
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * and instance the objAtom and set his properties to default values
   */
  ngOnInit() {
    this.createEssentiality();
    this.createChainPropertie();
    this.createTableFullInfoAmino();
    
    this.objAminoacid = new Aminoacid();
    this.objAminoacid.essenciality = this.arr_essentiality[0];
    this.objAminoacid.chainPropertie = this.arr_chainProperties[0];
    this.filterAmino();
  }

  /**
   *
   * @name aminoacid
   * @description Method that shows the objAminoacid and his properties in Console
   * And shows in console the show_arr_aa that contains the aminoacids that hace the selected properties
   */
  aminoacid(): void {
    console.log(this.objAminoacid);
    console.log(this.show_arr_aa);
  }

  /**
   *
   * @name createEssentiality
   * @description Method that fills the arr_essentiality
   */
  createEssentiality(): void{
    let essentiality: string[]=["essential","non essential","conditional"];
    for(let i:number=0; i<essentiality.length;i++){
      this.arr_essentiality.push(new Essentiality(i, essentiality[i]));
    }
  }

  /**
   *
   * @name createChainPropertie
   * @description Method that fills the arr_chainProperties
   */
  createChainPropertie(): void{
    let properties: string[]=["polar","non polar","acidic polar", "basic polar", "Aromatic"];
    for(let i:number=0; i<properties.length;i++){
      this.arr_chainProperties.push(new ChainPropertie(i, properties[i]));
    }
  }

  /**
   *
   * @name createTableFullInfoAmino
   * @description Method that fills the arr_aa
   */
  createTableFullInfoAmino(): void{
    this.arr_aa.push(new FullInfoAmino("Alanine","Ala","A",new Essentiality(1,"non essential"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Arginine","Arg","R",new Essentiality(2,"conditional"),new ChainPropertie(3,"basic polar")));
    this.arr_aa.push(new FullInfoAmino("Asparagine","Asn","N",new Essentiality(1,"non essential"),new ChainPropertie(0,"polar")));
    this.arr_aa.push(new FullInfoAmino("Aspartic acid","Asp","D",new Essentiality(1,"non essential"),new ChainPropertie(2,"acidic polar")));
    this.arr_aa.push(new FullInfoAmino("Cysteine","Cys","C",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Glutamic acid","Glu","E",new Essentiality(1,"non essential"),new ChainPropertie(2,"acidic polar")));
    this.arr_aa.push(new FullInfoAmino("Glutamine","Gln","Q",new Essentiality(2,"conditional"),new ChainPropertie(0,"polar")));
    this.arr_aa.push(new FullInfoAmino("Glycine","Gly","G",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Histidine","His","H",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    this.arr_aa.push(new FullInfoAmino("Isoleucine","Ile","I",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Leucine","Leu","L",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Lysine","Lys","K",new Essentiality(0,"essential"),new ChainPropertie(3,"basic polar")));
    this.arr_aa.push(new FullInfoAmino("Methionine","Met","M",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Phenylalanine","Phe","F",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    this.arr_aa.push(new FullInfoAmino("Proline","Pro","P",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    this.arr_aa.push(new FullInfoAmino("Serine","Ser","S",new Essentiality(2,"conditional"),new ChainPropertie(0,"polar")));
    this.arr_aa.push(new FullInfoAmino("Threonine","Thr","T",new Essentiality(0,"essential"),new ChainPropertie(0,"polar")));
    this.arr_aa.push(new FullInfoAmino("Tryptophan","Trp","W",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    this.arr_aa.push(new FullInfoAmino("Tyrosine","Tyr","Y",new Essentiality(2,"conditional"),new ChainPropertie(4,"Aromatic")));
    this.arr_aa.push(new FullInfoAmino("Valine","Val","V",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
  }

  /**
   *
   * @name filterAmino
   * @description Method that filters the aminoacid with the selected properties (essenciality and chainProperty)
   */
  filterAmino(): void{
    this.show_arr_aa =  this.arr_aa.filter(aa => aa.essenciality.id === this.objAminoacid.essenciality.id);
    this.show_arr_aa =  this.show_arr_aa.filter(aa => aa.chainPropertie.id === this.objAminoacid.chainPropertie.id);

  }
}
