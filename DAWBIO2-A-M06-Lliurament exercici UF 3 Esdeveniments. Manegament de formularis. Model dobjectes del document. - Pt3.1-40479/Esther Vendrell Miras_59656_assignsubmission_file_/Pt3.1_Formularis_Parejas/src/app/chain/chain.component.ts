import { Component, OnInit } from '@angular/core';
import { Chain } from '../model/chain/Chain';
import { StructureOfChain } from '../model/chain/structureOfChain';

@Component({
  selector: 'app-chain',
  templateUrl: './chain.component.html',
  styleUrls: ['./chain.component.css']
})
export class ChainComponent implements OnInit {
  
  //properties
  objChain: Chain;
  chainType: string;
  typeOfChains: StructureOfChain[]=[];
  
  //constructor
  constructor() { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * instance the objChain and set his properties to default values
   */
  ngOnInit() {
    this.createTypeOfChain();
    this.objChain = new Chain();
    this.objChain.$typeOfChain = this.typeOfChains[0];
  }

  /**
   * @name createTypeOfChain
   * @description function that register all types of chains in an array and push it to typeOfChains array (a property)
   */
  createTypeOfChain() {
    let typeChainAux: string[]=["Primary", "Secondary", "Tertiary", "Quaternary"];

    for(let i:number=0; i<typeChainAux.length;i++){
      this.typeOfChains.push(new StructureOfChain
          (i,typeChainAux[i]));
    }
  }

  /**
   * @name calculateSequenceLength
   * @description function that calculate the length of chain_sequence and save it in chain_sequence_length variable. 
   */
  calculateSequenceLength(){
    this.objChain.$chain_sequence_length = this.objChain.$chain_sequence.length;
  }

  /**
   * @name chain
   * @description Method that shows the objChain and his properties in console
   */
  chain(): void {
    console.log(this.objChain);
  }

}
