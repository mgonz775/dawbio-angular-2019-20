import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

//Title of our project
export class AppComponent {
  title = 'DAVIDESTHER-m06';
}