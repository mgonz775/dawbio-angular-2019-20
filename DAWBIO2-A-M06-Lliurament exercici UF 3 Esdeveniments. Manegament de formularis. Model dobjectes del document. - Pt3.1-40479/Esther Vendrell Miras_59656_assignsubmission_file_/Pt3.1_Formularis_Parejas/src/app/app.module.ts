import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';   

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Import of our components
import { ProteinComponent } from './protein/protein.component';
import { ChainComponent } from './chain/chain.component';
import { AminoacidComponent } from './aminoacid/aminoacid.component';
import { AtomComponent } from './atom/atom.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ChainDirectiveDirective } from './directives/chain-directive.directive';

//*
const appRoutes: Routes = [
  { path: 'protein', component: ProteinComponent },       // localhost/protein
  { path: 'chain', component: ChainComponent },           // localhost/chain
  { path: 'aminoacid', component: AminoacidComponent },   // localhost/aminoacid
  { path: 'atom', component: AtomComponent },             // localhost/atom
  { path: 'navbar', component: NavbarComponent },         // localhost/navbar (the home)
  { path: '', redirectTo: '/navbar', pathMatch: 'full' }, // Initially. 
  { path: '**', component: PageNotFoundComponent }        // When the route is not found
]

@NgModule({
  declarations: [
    AppComponent,
    ProteinComponent,
    ChainComponent,
    AminoacidComponent,
    AtomComponent,
    PageNotFoundComponent,
    NavbarComponent,
    ChainDirectiveDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: false} //True for debugging purpouses only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
