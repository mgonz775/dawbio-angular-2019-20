//imports
import { Component, OnInit } from '@angular/core';
import { Atom } from '../model/atom/Atom';
import { Occurrence } from '../model/atom/Occurrence';
import { Orbital} from '../model/atom/Orbital';
import { State} from '../model/atom/State';
import {Trend} from '../model/atom/Trend';
import {SubTrend} from '../model/atom/SubTrend';

//Component, selector tag...
@Component({
  selector: 'app-atom',
  templateUrl: './atom.component.html',
  styleUrls: ['./atom.component.css']
})


export class AtomComponent implements OnInit {
  
  //properties
  objAtom: Atom;
  arr_occurrence: Occurrence[]=[];
  arr_sfdp: Orbital[]=[];
  arr_state: State[]=[];
  arr_trend: Trend[]=[];
  arr_subtrend_metal: SubTrend[]=[];
  arr_subtrend_nonmetal: SubTrend[]=[];

  //Constructor
  constructor() { }

  /**
   *
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * and instance the objAtom and set his properties to default values
   */
  ngOnInit() {
    this.createOccurrence();
    this.createOrbital();
    this.createState();
    this.createTrend();
    this.createSubTrendMetal();
    this.createSubTrendNonMetal();

    this.objAtom = new Atom();
    this.objAtom.trend = this.arr_trend[0];
    this.objAtom.subtrend = this.arr_subtrend_metal[0];
    this.objAtom.state = this.arr_state[0];
    this.objAtom.occurrence = this.arr_occurrence[0];
    this.objAtom.orbital = [];
  }

  /**
   *
   * @name atom
   * @description Method that shows the objAtom and his properties in Console
   */
  atom(): void {
    console.log(this.objAtom);
  }

  /**
   *
   * @name createOccurrence
   * @description Method that fills the arr_occurrence
   */
  createOccurrence(): void{
    let occurrence: string[]=["Primordial","From decay","Synthetic"];
    for(let i:number=0; i<occurrence.length;i++){
      this.arr_occurrence.push(new Occurrence(i, occurrence[i]));
    }
  }

  /**
   *
   * @name createOrbital
   * @description Method that fills the arr_sfdp
   */
  createOrbital(): void{
    let sfdp: string[]=["s","f","d","p"];
    for(let i:number=0; i<sfdp.length;i++){
      this.arr_sfdp.push(new Orbital(i, sfdp[i]));
    }
  }

  /**
   *
   * @name createState
   * @description Method that fills the arr_state
   */
  createState(): void{
    let state: string[]=["Gas","Solid","Liquid","Unknown"];
    for(let i:number=0; i<state.length;i++){
      this.arr_state.push(new State(i, state[i]));
    }
  }

  /**
   *
   * @name createTrend
   * @description Method that fills the arr_trend
   */
  createTrend(): void{
    let trend: string[]=["Metal","Metalloid","Nonmetal","Unknown chemical properties"];
    for(let i:number=0; i<trend.length;i++){
      this.arr_trend.push(new Trend(i, trend[i]));
    }
  }

  /**
   *
   * @name createSubTrendMetal
   * @description Method that fills the arr_subtrend_metal
   */
  createSubTrendMetal(): void{
    let subtrend: string[]=["Alkali metal","Alkali earth metal","Lanthonide","Actinide","Transition metal","Post-transitional metal"];
    for(let i:number=0; i<subtrend.length;i++){
      this.arr_subtrend_metal.push(new SubTrend(i, subtrend[i]));
    }
  }

  /**
   *
   * @name createSubTrendNonMetal
   * @description Method that fills the arr_subtrend_nonmetal
   */
  createSubTrendNonMetal(): void{
    let subtrend: string[]=["Reactive nonmetal", "Noble gas"];
    for(let i:number=0; i<subtrend.length;i++){
      this.arr_subtrend_nonmetal.push(new SubTrend(i, subtrend[i]));
    }
  }

  /**
   *
   * @name changeSubtrend
   * @description Method that fills the objAtom.subtrend with the corresponding subtrend 
   * depending on the selected trend in the template
   */
  changeSubtrend(): void{
    switch(this.objAtom.trend.id) { 
      case 0: { 
        this.objAtom.subtrend = this.arr_subtrend_metal[0]; 
         break; 
      } 
      case 2: { 
        this.objAtom.subtrend = this.arr_subtrend_nonmetal[0]; 
         break; 
      } 
      default: { 
        this.objAtom.subtrend = null; 
         break; 
      } 
   } 
  }

  /**
   *
   * @name addRemoveOrbital
   * @description Method that add and remove the orbital on objAtom.orbital
   * depending on the checkbox in the template
   */
  addRemoveOrbital(sfdp: Orbital): void{
    //(array).splice(index, num_elem)
    //(array).idexOf(obj) return -1 if not found, return the index if founds
    let myIndex: number = this.objAtom.orbital.indexOf(sfdp);
    if (myIndex == -1){
      this.objAtom.orbital.push(sfdp);
    }else{
      this.objAtom.orbital.splice(myIndex, 1);
    }
  }
}
