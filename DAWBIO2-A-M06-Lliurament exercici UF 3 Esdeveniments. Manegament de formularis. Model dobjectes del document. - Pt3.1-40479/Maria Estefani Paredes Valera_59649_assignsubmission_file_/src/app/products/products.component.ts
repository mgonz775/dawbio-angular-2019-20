import { Component, OnInit } from '@angular/core';
import { Products } from '../model/products';
import { Fermentation } from '../model/fermentation';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  //properties
  objProducts: Products;
  arraypProducts: Fermentation[] = [];
  
  constructor() { }

  ngOnInit() {
    this.objProducts = new Products();
    this.objProducts.fermentation = new Fermentation();
    this.objProducts.fermentation=this.arraypProducts[1];
    this.createFermentationSelect();
  }

  yeastEntryProducts(): void {
    console.log(this.objProducts);
  }

  createFermentationSelect(): void {
    
      let productTypeAux: string[]=["Alcoholic Fermentation","Lactic Acid Fermentation","Propionic Acid Fermentation","Butyric Acid — Butanol Fermentation"];
  
      for(let i:number=0; i<productTypeAux.length;i++){
        this.arraypProducts.push(new Fermentation
            (i,productTypeAux[i]));
      }
    
  }


}
