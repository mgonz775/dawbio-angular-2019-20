import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[inputMinLength]',//es el nombre de el atributo que voy a poner a mi componente
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: InputValidationDirective,
    multi: true
  }]

}) 
export class InputValidationDirective  implements Validator {

  constructor() { }

  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {// trnga un minimo de 6 caracteres
    let validInput: boolean = false;
    if (formFieldToValidate && formFieldToValidate.value && formFieldToValidate.value.length > 6) {//formFieldToValidate !=null
      validInput = true;
    }
    return validInput? null : {'isNotCorrect':true};//{'isNotCorrect':true} al caltogo de errores le añado este error 
  }
}
