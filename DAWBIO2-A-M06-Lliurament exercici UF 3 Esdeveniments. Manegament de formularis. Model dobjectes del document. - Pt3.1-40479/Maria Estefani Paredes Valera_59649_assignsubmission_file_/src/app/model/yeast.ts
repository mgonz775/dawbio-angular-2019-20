import { Taxonomy } from './taxonomy';

/**
 *
 *
 * @export
 * @class Yeast
 */
export class Yeast{
    private _id: number; //lenght min de 6 chars , combinate letters and numbers
    private _taxonomy: Taxonomy;
    private _classificationDate: string;//no required


	constructor(id?: number, taxonomy?: Taxonomy, classificationDate?: string) {
		this._id = id;
		this._taxonomy = taxonomy;
		this._classificationDate = classificationDate;
    }
    

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter taxonomy
     * @return {Taxonomy}
     */
	public get taxonomy(): Taxonomy {
		return this._taxonomy;
	}

    /**
     * Getter classificationDate
     * @return {string}
     */
	public get classificationDate(): string {
		return this._classificationDate;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter taxonomy
     * @param {Taxonomy} value
     */
	public set taxonomy(value: Taxonomy) {
		this._taxonomy = value;
	}

    /**
     * Setter classificationDate
     * @param {string} value
     */
	public set classificationDate(value: string) {
		this._classificationDate = value;
	}


}

