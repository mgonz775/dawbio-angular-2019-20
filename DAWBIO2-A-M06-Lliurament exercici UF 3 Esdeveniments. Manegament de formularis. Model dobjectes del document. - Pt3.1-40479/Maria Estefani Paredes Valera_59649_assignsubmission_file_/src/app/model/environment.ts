export class Environment{
    private _geography: string;
    private _weather: string;

	constructor(geography?: string, weather?: string) {
		this._geography = geography;
		this._weather = weather;
	}

    /**
     * Getter geography
     * @return {string}
     */
	public get geography(): string {
		return this._geography;
	}

    /**
     * Getter weather
     * @return {string}
     */
	public get weather(): string {
		return this._weather;
	}

    /**
     * Setter geography
     * @param {string} value
     */
	public set geography(value: string) {
		this._geography = value;
	}

    /**
     * Setter weather
     * @param {string} value
     */
	public set weather(value: string) {
		this._weather = value;
	}
    
}