import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

registerLocaleData(localeES);

import { AppComponent } from './app.component';
import { UserEntryComponent } from './user-entry/user-entry.component';
import { SequenceEntryComponent } from './sequence-entry/sequence-entry.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PasswordMinLengthDirective } from './directives/password-min-length.directive';
import { ProteinLengthDirective } from './directives/protein-length.directive';
import { ResultsComponent } from './results/results.component';

const appRoutes: Routes = [
  { path: 'user-entry', component: UserEntryComponent},
  { path: 'sequence-entry', component: SequenceEntryComponent},
  { path: 'price-calculate', component: ResultsComponent},
  { path: 'feedback', component: FeedbackComponent},
  { path: '', redirectTo: '/user-entry', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
]


@NgModule({
  declarations: [
    AppComponent,
    UserEntryComponent,
    SequenceEntryComponent,
    FeedbackComponent,
    PageNotFoundComponent,
    PasswordMinLengthDirective,
    ProteinLengthDirective,
    ResultsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes, { enableTracing: false }  //True for debugging purposes only
    )
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'es'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
