export class User {

    //Properties
    private _name: string;
    private _surname: string;
    private _DNI: string;
    private _email: string;
    private _password: string;
    private _phone: string;
    private _birthdate: string;
    private _zipcode: string;
    private _location: string;

	/**
     *Creates an instance of User.
     * @param {string} name
     * @param {string} surname
     * @param {string} DNI
     * @param {string} email
     * @param {string} password
     * @param {string} phone
     * @param {string} birthdate
     * @param {string} zipcode
     * @param {string} location
     * @memberof User
     */
    constructor(name?: string, surname?: string, DNI?: string, email?: string, password?: string, phone?: string, birthdate?: string, zipcode?: string, location?: string) {
		this._name = name;
		this._surname = surname;
		this._DNI = DNI;
		this._email = email;
		this._password = password;
		this._phone = phone;
		this._birthdate = birthdate;
		this._zipcode = zipcode;
		this._location = location;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter surname
     * @return {string}
     */
	public get surname(): string {
		return this._surname;
	}

    /**
     * Getter DNI
     * @return {string}
     */
	public get DNI(): string {
		return this._DNI;
	}

    /**
     * Getter email
     * @return {string}
     */
	public get email(): string {
		return this._email;
	}

    /**
     * Getter password
     * @return {string}
     */
	public get password(): string {
		return this._password;
	}

    /**
     * Getter phone
     * @return {string}
     */
	public get phone(): string {
		return this._phone;
	}

    /**
     * Getter birthdate
     * @return {string}
     */
	public get birthdate(): string {
		return this._birthdate;
	}

    /**
     * Getter zipcode
     * @return {string}
     */
	public get zipcode(): string {
		return this._zipcode;
	}

    /**
     * Getter location
     * @return {string}
     */
	public get location(): string {
		return this._location;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter surname
     * @param {string} value
     */
	public set surname(value: string) {
		this._surname = value;
	}

    /**
     * Setter DNI
     * @param {string} value
     */
	public set DNI(value: string) {
		this._DNI = value;
	}

    /**
     * Setter email
     * @param {string} value
     */
	public set email(value: string) {
		this._email = value;
	}

    /**
     * Setter password
     * @param {string} value
     */
	public set password(value: string) {
		this._password = value;
	}

    /**
     * Setter phone
     * @param {string} value
     */
	public set phone(value: string) {
		this._phone = value;
	}

    /**
     * Setter birthdate
     * @param {string} value
     */
	public set birthdate(value: string) {
		this._birthdate = value;
	}

    /**
     * Setter zipcode
     * @param {string} value
     */
	public set zipcode(value: string) {
		this._zipcode = value;
	}

    /**
     * Setter location
     * @param {string} value
     */
	public set location(value: string) {
		this._location = value;
	}
	
    /**
     * It validates the letter of the DNI
     * @returns {boolean}
     * @memberof User
     */
    validateLetterDNI(): boolean {
        let result: boolean = true;
        let numsDNIintro: number = parseInt(this._DNI.substr(0, 8));
        let letterDNIintro: string = this._DNI.substr(8).toUpperCase( );
        let rest: number = numsDNIintro % 23;
        let letters: string[] = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
        let letter: string = letters[rest];
        return letter == letterDNIintro;
    }
}
