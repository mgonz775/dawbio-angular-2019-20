export class Recommendation {

    //Properties
    private _id: number;
    private _description: string;

	/**
     *Creates an instance of Recommendation.
     * @param {number} id
     * @param {string} description
     * @memberof Recommendation
     */
    constructor(id: number, description: string) {
		this._id = id;
		this._description = description;
    }

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter description
     * @return {string}
     */
	public get description(): string {
		return this._description;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter description
     * @param {string} value
     */
	public set description(value: string) {
		this._description = value;
	}
}    