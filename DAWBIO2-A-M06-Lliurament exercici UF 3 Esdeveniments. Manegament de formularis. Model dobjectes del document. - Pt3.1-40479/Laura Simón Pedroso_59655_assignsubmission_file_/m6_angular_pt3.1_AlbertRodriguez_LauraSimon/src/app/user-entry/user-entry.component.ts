import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import { User } from '../model/User';

@Component({
  selector: 'app-user-entry',
  templateUrl: './user-entry.component.html',
  styleUrls: ['./user-entry.component.css'],
  providers:[DatePipe]
})
export class UserEntryComponent implements OnInit {

  //Properties
  objUser: User;

  constructor(private datePipe : DatePipe) { }

  ngOnInit() {
    this.objUser = new User();
    this.objUser.birthdate = this.datePipe.transform(new Date(), 'yyyy-MM-dd'); //this is to initialize the birthdate to todays's date
  }

  //it prints in console the object User
  userEntry(): void{
    console.log(this.objUser);   
  }
}
