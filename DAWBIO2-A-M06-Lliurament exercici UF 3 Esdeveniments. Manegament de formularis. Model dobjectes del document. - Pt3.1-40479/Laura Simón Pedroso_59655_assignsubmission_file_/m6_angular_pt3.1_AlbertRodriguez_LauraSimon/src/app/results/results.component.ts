import { Component, OnInit } from '@angular/core';
import { Results } from '../model/Results';
import { ResultsPlace } from '../model/ResultsPlace';
import { ResultsDate } from '../model/ResultsDate';
import { ResultsCopies } from '../model/ResultsCopies';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  //Properties
  objResults: Results;
  resultsPlaces: ResultsPlace[] = [];
  resultsDates: ResultsDate[] = [];
  resultsCopies: ResultsCopies[] = [];

  constructor() { }

  ngOnInit() {
    this.createPlaces();
    this.createDates();
    this.createCopies();
    this.objResults = new Results();
    this.objResults.resultsPlace = this.resultsPlaces[0]; //the first place of the array will appear selected as default
    this.objResults.resultsDate = this.resultsDates[0]; //the first date of the array will appear marked as default
    this.objResults.resultsCopies = this.resultsCopies[0]; //the first answer of the array will appear selected as default
    this.objResults.totalPrice = 100;
    this.calculateTotalPrice();
  }

  //it creates the array of the possible places
  createPlaces(): void {
    let placesAux: string[] = ["Laboratory", "Medical center", "Home delivery", "Email"];

    for (let i: number = 0; i < placesAux.length; i++) {
      this.resultsPlaces.push(new ResultsPlace
        (i, placesAux[i], i * 3 + 1));  
    }
  }

  //it creates the array of the possible dates
  createDates(): void {
    let datesAux: string[] =
      ["When they are ready", "Within a month", "Within one week", "Next day"];

    for (let i: number = 0; i < datesAux.length; i++) {
      this.resultsDates.push(new ResultsDate
        (i, datesAux[i], i * 3 + 1));
    }
  }

  //it creates the array of the possible answers
  createCopies(): void {
    let copiesAux: string[] = ["No", "Yes"];

    for (let i: number = 0; i < copiesAux.length; i++) {
      this.resultsCopies.push(new ResultsCopies
        (i, copiesAux[i], i * 3 + 1));
    }
  }

  //it calculates the total price
  calculateTotalPrice(): void {
    this.objResults.totalPrice += this.objResults.resultsPlace.price + this.objResults.resultsDate.price + this.objResults.resultsCopies.price;
  }

  //it prints in console the object Results
  results(): void {
    console.log(this.objResults);
  }
}
