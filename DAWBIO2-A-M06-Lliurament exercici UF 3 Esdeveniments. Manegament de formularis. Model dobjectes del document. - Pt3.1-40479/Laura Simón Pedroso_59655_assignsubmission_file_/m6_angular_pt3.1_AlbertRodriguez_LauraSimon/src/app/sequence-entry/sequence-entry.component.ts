import { Component, OnInit } from '@angular/core';
import { Sequence } from '../model/Sequence';
import { SequenceType } from '../model/SequenceType';

@Component({
  selector: 'app-sequence-entry',
  templateUrl: './sequence-entry.component.html',
  styleUrls: ['./sequence-entry.component.css']
})
export class SequenceEntryComponent implements OnInit {

  //Properties
  objSequence: Sequence;
  sequenceTypes: SequenceType[] = [];

  constructor() { }

  ngOnInit() {
    this.createSequenceTypes();
    this.objSequence = new Sequence();
    this.objSequence.sequenceType = this.sequenceTypes[0]; //the first sequenceType of the array will appear selected as default
  }

  //it creates the array with the possible sequence types
  createSequenceTypes(): void{
    let sequenceTypesAux: string[]=["DNA", "RNA", "Protein"];

    for(let i:number=0; i<sequenceTypesAux.length;i++){
      this.sequenceTypes.push(new SequenceType
          (i,sequenceTypesAux[i]));
    }
  }

  //it prints in console the object Sequence
  sequenceEntry(): void{
    console.log(this.objSequence);   
  }
}
