import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SequenceEntryComponent } from './sequence-entry.component';

describe('SequenceEntryComponent', () => {
  let component: SequenceEntryComponent;
  let fixture: ComponentFixture<SequenceEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SequenceEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SequenceEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
