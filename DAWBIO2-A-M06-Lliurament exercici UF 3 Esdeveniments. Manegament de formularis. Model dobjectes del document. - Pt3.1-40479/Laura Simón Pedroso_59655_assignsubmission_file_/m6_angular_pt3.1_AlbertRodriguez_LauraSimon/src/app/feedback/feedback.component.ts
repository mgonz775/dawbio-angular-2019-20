import { Component, OnInit } from '@angular/core';
import { Feedback } from '../model/Feedback';
import { Places } from '../model/Places';
import { Rate } from '../model/Rate';
import { Recommendation } from '../model/Recommendation';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  //Properties
  objFeedback: Feedback;
  places: Places[] = [];
  rates: Rate[] = [];
  recommendations: Recommendation[] = [];

  constructor() { }

  ngOnInit() {
    this.createPlaces();
    this.createRates();
    this.createRecommendations();
    this.objFeedback = new Feedback();
    this.objFeedback.places = [];
    this.objFeedback.rate = this.rates[0]; //the first rate of the array will appear marked as default
    this.objFeedback.recommendation = this.recommendations[0]; //the first answer of the array will appear selected as default
  }

  //it creates the array of the possible places
  createPlaces(): void {
    let placesAux: string[] = ["Friend / family recommendation", "Internet", "TV", "Radio", "Billboard", "Other"];

    for (let i: number = 0; i < placesAux.length; i++) {
      this.places.push(new Places
        (i, placesAux[i]));
    }
  }

  //if the array of places is empty, it fills it. Otherwise, it empties it
  addRemovePlaces(placesAux: Places): void {
    //indexOf() method returns the first index at which a given 
    //element can be found in the array, or -1 if it is not present.
    let myIndex: number = this.objFeedback.places.indexOf(placesAux);

    if (myIndex == -1) {
      this.objFeedback.places.push(placesAux);
    } else {
      this.objFeedback.places.splice(myIndex, 1);
    }
  }

  //it creates the array of the possible rates
  createRates(): void {
    let ratesAux: string[] =
      ["Excellent", "Very good",
        "Good", "Regular", "Bad", "Very bad"];

    for (let i: number = 0; i < ratesAux.length; i++) {
      this.rates.push(new Rate
        (i, ratesAux[i]));
    }
  }

  //it creates the array of the possible answers
  createRecommendations(): void {
    let recommendationsAux: string[] = ["Yes", "No"];

    for (let i: number = 0; i < recommendationsAux.length; i++) {
      this.recommendations.push(new Recommendation
        (i, recommendationsAux[i]));
    }
  }

  //it prints in console the object Feedback
  feedback(): void {
    console.log(this.objFeedback);
  }
}
