import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component'
import { ClientsComponent } from './components/clients/clients.component'
import { StaffRegistrationComponent } from './components/staff/staff-registration.component';
import { ClientRegistrationComponent } from './components/client-registration/client-registration.component';
import { MaterialsComponent } from './components/materials/materials.component';


const routes: Routes = [
  {
    path: "home",
    component: HomeComponent,
  },
  {
    path: "clients",
    component: ClientsComponent,
  },
  {
    path: "register/staff",
    component: StaffRegistrationComponent,
  },
  {
    path: "register/client",
    component: ClientRegistrationComponent,
  },
  {
    path: "materials",
    component: MaterialsComponent,
  },
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], /*, { enableTracing: true } */
  exports: [RouterModule]
})
export class AppRoutingModule { }
