/*
     @name= ValidNameDirective
     @author= Jimmy Tonato, Ankush Rana
     @version= 1.0
     @description= A directive class to valid a name.
     @date = 09-02-2020
     @params= event: boolean
     @return = none
   */

import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[isValidName]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidNameDirective,
    multi: true
  }]
})

export class ValidNameDirective implements Validator {

  constructor() { }

  //This method receives the control to validate: formFieldValidate
  validate(formFieldValidate: AbstractControl): { [key: string]: any } {
    let validInput: boolean = false;

    if (formFieldValidate.value) {
      //Pattern for Spanish or English names, at least three letters
      var re = new RegExp("^[a-zñáéíóúü]{3,}$", "i");
      validInput = formFieldValidate.value.match(re);
    }

    return validInput ? null : { 'inCorrectName': true };
  }
  //returns an object with the errors of the validation
  //or null in case the validation was correct

}