/*
     @name= ValidNameDirective
     @author= Jimmy Tonato, Ankush Rana
     @version= 1.0
     @description= A directive class to valid sequence depending on the time.
     @date = 09-02-2020
     @params= event: boolean
     @return = none
   */

import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validators, FormGroup, ValidationErrors } from '@angular/forms';

import {SequenceTypesBase} from '../components/client-registration/client-registration.component'


@Directive({
  selector: '[validSequence]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidSequenceDirective, multi: true }
  ]
})

export class ValidSequenceDirective implements Validators {
  //Getting type of sequence
  @Input('validSequence') type: string;
  constructor() {

  }

  //This method receives the control to validate: formFieldValidate
  validate(formFieldValidate: FormGroup): ValidationErrors {
    if (formFieldValidate.value && this.type) {
      
      //It gets base of type from SequenceTypesBase from component client, and creates a pattern
      var pattern = new RegExp(`[^${SequenceTypesBase[this.type.toLowerCase()]}]`,"i")
      
      //If it give -1 so it's good sequence.
      return formFieldValidate.value.search(pattern) == - 1 ? null :{ 'sequenceInCorrect': true } ;
    }


  }



}
