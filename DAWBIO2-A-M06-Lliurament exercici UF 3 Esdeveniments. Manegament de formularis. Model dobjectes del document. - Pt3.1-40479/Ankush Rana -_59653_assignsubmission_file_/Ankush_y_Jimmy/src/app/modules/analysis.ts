/*  
   @name= class Address
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Anlysis person information
   @date = 09-02-2020
   @return = none
 */
export class Analysis {
    private type:string;
    private tested:boolean;
    private sequence: string;


/*  
   @name= constructor()
   @params= $type: string,$tested:boolean, $sequence: string
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= The parameters are saved in their respective variables
   @date = 09-02-2020
   @return = none
 */
	constructor($type: string=null,$tested:boolean=null, $sequence: string=null) {
        this.type = $type;
        this.tested= $tested;
		this.sequence = $sequence;
	}

    /**
     * Getter $type
     * @return {string}
     */
	public get $type(): string {
		return this.type;
	}

    /**
     * Getter $sequence
     * @return {string}
     */
	public get $sequence(): string {
		return this.sequence;
    }
    
     /**
     * Getter $tested
     * @return {boolean}
     */
	public get $tested(): boolean {
		return this.tested;
	}

    /**
     * Setter $type
     * @param {string} value
     */
	public set $type(value: string) {
		this.type = value;
	}

    /**
     * Setter $sequence
     * @param {string} value
     */
	public set $sequence(value: string) {
		this.sequence = value;
    }
    
     /**
     * Setter $tested
     * @param {boolean} value
     */
	public set $tested(value: boolean) {
		this.tested= value;
	}




}
