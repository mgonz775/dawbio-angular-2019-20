import {Address } from './address';

/*  
   @name= class Person
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Person information
   @date = 09-02-2020
   @return = none
 */
export class Person {
    private first_name: string;
    private last_name: string;
    private dni: string;
    private age: number;
    private phone: string;
    private address: Address;
    
 /*  
   @name= constructor()
   @params= $first_name: string, $last_name: string,$dni: string, 
            $age: number, $phone: string, $address: Address
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= The parameters are saved in their respective variables
   @date = 09-02-2020
   @return = none
 */
    constructor($first_name: string=null, $last_name: string=null, 
        $dni: string=null, $age: number=null, $phone: string=null, $address: Address=null) {

		this.first_name = $first_name;
		this.last_name = $last_name;
		this.dni = $dni;
		this.age = $age;
		this.phone = $phone;
		this.address = $address;
	}
    


    /**
     * Getter $first_name
     * @return {string}
     */
	public get $first_name(): string {
		return this.first_name;
	}

    /**
     * Getter $last_name
     * @return {string}
     */
	public get $last_name(): string {
		return this.last_name;
	}

    /**
     * Getter $dni
     * @return {string}
     */
	public get $dni(): string {
		return this.dni;
	}

    /**
     * Getter $age
     * @return {number}
     */
	public get $age(): number {
		return this.age;
	}

    /**
     * Getter $phone
     * @return {string}
     */
	public get $phone(): string {
		return this.phone;
	}

    /**
     * Getter $address
     * @return {Address}
     */
	public get $address(): Address {
		return this.address;
	}


    /**
     * Setter $first_name
     * @param {string} value
     */
	public set $first_name(value: string) {
		this.first_name = value;
	}

    /**
     * Setter $last_name
     * @param {string} value
     */
	public set $last_name(value: string) {
		this.last_name = value;
	}

    /**
     * Setter $dni
     * @param {string} value
     */
	public set $dni(value: string) {
		this.dni = value;
	}

    /**
     * Setter $age
     * @param {number} value
     */
	public set $age(value: number) {
		this.age = value;
	}

    /**
     * Setter $phone
     * @param {string} value
     */
	public set $phone(value: string) {
		this.phone = value;
	}

    /**
     * Setter $address
     * @param {Address} value
     */
	public set $address(value: Address) {
		this.address = value;
	}

}
