
/*  
   @name= class Tool
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Tool information
   @date = 09-02-2020
   @return = none
 */
export class Tool {
    private id: number;
    private name:string;
    private availability:boolean;
    private price:number;
    
/*  
   @name= constructor()
   @params=$id: number, $name: string, $availability: boolean, $price: number
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description=The parameters are saved in their respective variables
   @date = 09-02-2020
   @return = none
 */
	constructor($id: number=null, $name: string=null, $availability: boolean=null, $price: number=null) {
		this.id = $id;
		this.name = $name;
		this.availability = $availability;
		this.price = $price;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $name
     * @return {string}
     */
	public get $name(): string {
		return this.name;
	}

    /**
     * Getter $availability
     * @return {boolean}
     */
	public get $availability(): boolean {
		return this.availability;
	}

    /**
     * Getter $price
     * @return {number}
     */
	public get $price(): number {
		return this.price;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $name
     * @param {string} value
     */
	public set $name(value: string) {
		this.name = value;
	}

    /**
     * Setter $availability
     * @param {boolean} value
     */
	public set $availability(value: boolean) {
		this.availability = value;
	}

    /**
     * Setter $price
     * @param {number} value
     */
	public set $price(value: number) {
		this.price = value;
	}
    
}
