/*  
   @name= class Address
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Address person information
   @date = 09-02-2020
   @return = none
 */
export class Address {
    private street_name: string;
    private number: number;
    private other: string;
    private city:string;
    private postal_code: string;

/*  
   @name= constructor()
   @params= $street_name: string, $number: number, $other: string, $city: string, $postal_code: string
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= The parameters are saved in their respective variables
   @date = 09-02-2020
   @return = none
 */
	constructor($street_name: string=null, $number: number=null, $other: string=null, $city: string=null, $postal_code: string=null) {
		this.street_name = $street_name;
		this.number = $number;
		this.other = $other;
		this.city = $city;
		this.postal_code = $postal_code;
	}

    /**
     * Getter $street_name
     * @return {string}
     */
	public get $street_name(): string {
		return this.street_name;
	}

    /**
     * Getter $number
     * @return {number}
     */
	public get $number(): number {
		return this.number;
	}

    /**
     * Getter $other
     * @return {string}
     */
	public get $other(): string {
		return this.other;
	}

    /**
     * Getter $city
     * @return {string}
     */
	public get $city(): string {
		return this.city;
	}

    /**
     * Getter $postal_code
     * @return {string}
     */
	public get $postal_code(): string {
		return this.postal_code;
	}

    /**
     * Setter $street_name
     * @param {string} value
     */
	public set $street_name(value: string) {
		this.street_name = value;
	}

    /**
     * Setter $number
     * @param {number} value
     */
	public set $number(value: number) {
		this.number = value;
	}

    /**
     * Setter $other
     * @param {string} value
     */
	public set $other(value: string) {
		this.other = value;
	}

    /**
     * Setter $city
     * @param {string} value
     */
	public set $city(value: string) {
		this.city = value;
	}

    /**
     * Setter $postal_code
     * @param {string} value
     */
	public set $postal_code(value: string) {
		this.postal_code = value;
	}
    
}
