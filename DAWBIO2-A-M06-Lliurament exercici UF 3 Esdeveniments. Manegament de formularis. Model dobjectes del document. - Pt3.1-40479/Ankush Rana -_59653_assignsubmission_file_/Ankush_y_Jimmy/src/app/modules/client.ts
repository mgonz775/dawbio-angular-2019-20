/*  
   @name= class Client
   @extends= Person
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Client class , extends person class
   @date = 09-02-2020
   @return = none
 */

import { MedicalData } from './medical-data'
import { Person } from './person';
import { Address } from './address';

export class Client extends Person {
    private id: number;
    private comments: string;
    private medical_data: MedicalData;

/*  
   @name= constructor()
   @params= $id: number, $first_name: string, $last_name: string , $dni: string ,
            $age: number, $phone: string, $address: Address, $comments: string , 
            $medical_data: MedicalData
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description=The parameters are saved in their respective variables and we 
                also call super to save the variables of Persona, MedicalData, Address
   @date = 09-02-2020
   @return = none
 */
    constructor($id: number = null, $first_name: string = null, $last_name: string = null, $dni: string = null,
        $age: number = null, $phone: string = null, $address: Address = null, $comments: string = null, $medical_data: MedicalData = null) {
        super($first_name, $last_name, $dni, $age, $phone, $address);

        this.id = $id;
        this.comments = $comments;
        this.medical_data = $medical_data;
    }


    /**
     * Getter $id
     * @return {number}
     */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $comments
     * @return {string}
     */
    public get $comments(): string {
        return this.comments;
    }

    /**
     * Getter $medical_data
     * @return {MedicalData}
     */
    public get $medical_data(): MedicalData {
        return this.medical_data;
    }

    /**
     * Setter $id
     * @param {number} value
     */
    public set $id(value: number) {
        this.id = value;
    }

    /**
     * Setter $comments
     * @param {string} value
     */
    public set $comments(value: string) {
        this.comments = value;
    }

    /**
     * Setter $medical_data
     * @param {MedicalData} value
     */
    public set $medical_data(value: MedicalData) {
        this.medical_data = value;
    }


}
