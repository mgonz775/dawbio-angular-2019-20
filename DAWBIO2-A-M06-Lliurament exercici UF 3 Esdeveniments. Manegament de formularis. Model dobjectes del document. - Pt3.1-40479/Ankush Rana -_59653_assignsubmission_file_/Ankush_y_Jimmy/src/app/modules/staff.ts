import { Person } from "./person";
import { Address } from "./address";


/*  
   @name= class Staff
   @extends= Person
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Staff person information
   @date = 09-02-2020
   @return = none
 */
export class Staff extends Person {
    private id: number;
    private role: string;
    private manager_id: number;
    private hire_date: Date;
/*  
   @name= constructor()
   @params= $id: number, $first_name: string, $last_name: string,
        $dni: string, $age: number, $phone: string, $address: Address,
        $role: string, $manager_id: number, $hire_date: Date
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description=The parameters are saved in their respective variables and we 
                also call super to save the variables of Person, Address
   @date = 09-02-2020
   @return = none
 */
    constructor($id: number = null, $first_name: string = null, $last_name: string = null,
        $dni: string = null, $age: number = null, $phone: string = null, $address: Address = null,
        $role: string = null, $manager_id: number = null, $hire_date: Date = null) {
        super($first_name, $last_name, $dni, $age, $phone, $address);

        this.id = $id;
        this.role = $role;
        this.manager_id = $manager_id;
        this.hire_date = $hire_date;
    }

    /**
   * Getter $id
   * @return {number}
   */
    public get $id(): number {
        return this.id;
    }

    /**
     * Getter $role
     * @return {string}
     */
    public get $role(): string {
        return this.role;
    }

    /**
     * Getter $manager_id
     * @return {number}
     */
    public get $manager_id(): number {
        return this.manager_id;
    }

    /**
     * Getter $hire_date
     * @return {Date}
     */
    public get $hire_date(): Date {
        return this.hire_date;
    }


    /**
  * Setter $id
  * @param {number} value
  */
    public set $id(value: number) {
        this.id = value;
    }
    /**
     * Setter $role
     * @param {string} value
     */
    public set $role(value: string) {
        this.role = value;
    }

    /**
     * Setter $manager_id
     * @param {number} value
     */
    public set $manager_id(value: number) {
        this.manager_id = value;
    }

    /**
     * Setter $hire_date
     * @param {string} value
     */
    public set $hire_date(value: Date) {
        this.hire_date = value;
    }


}
