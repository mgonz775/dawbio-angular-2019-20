import { Analysis } from './analysis';

/*  
   @name= class MedicalData
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= MedicalData person information
   @date = 09-02-2020
   @return = none
 */
export class MedicalData {
    private analysis: Analysis;
    private diseases: string[];

/*  
   @name= constructor()
   @params= $analysis: Analysis, $diseases: string[]
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= The parameters are saved in their respective variables
   @date = 09-02-2020
   @return = none
 */
    constructor($analysis: Analysis = null, $diseases: string[] = null) {
        this.analysis = $analysis;
        this.diseases = $diseases;
    }

    /**
     * Getter $analysis
     * @return {Analysis}
     */
    public get $analysis(): Analysis {
        return this.analysis;
    }

    /**
     * Getter $diseases
     * @return {string}
     */
    public get $diseases(): string[] {
        return this.diseases;
    }

    /**
     * Setter $analysis
     * @param {Analysis} value
     */
    public set $analysis(value: Analysis) {
        this.analysis = value;
    }

    /**
     * Setter $diseases
     * @param {string} value
     */
    public set $diseases(value: string[]) {
        this.diseases = value;
    }


}
