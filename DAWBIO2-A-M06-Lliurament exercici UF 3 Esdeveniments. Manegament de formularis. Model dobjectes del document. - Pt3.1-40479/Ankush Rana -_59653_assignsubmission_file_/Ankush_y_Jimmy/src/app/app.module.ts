import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common'
import localeEs from '@angular/common/locales/es'

registerLocaleData(localeEs);

import {  FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {
  MatTabsModule, MatInputModule, MatToolbarModule, MatButtonModule, MatStepperModule, MatGridListModule, MatFormFieldModule,
  MatSelectModule, MatCheckboxModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule, MatDialogModule,MatSlideToggleModule,
  MatTableModule,MatIconModule,MatChipsModule,MatSnackBarModule
} from '@angular/material';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientRegistrationComponent } from './components/client-registration/client-registration.component';
import { StaffRegistrationComponent} from './components/staff/staff-registration.component';
import { MaterialsComponent, DialogAddTool } from './components/materials/materials.component';
import { PersonComponent } from './components/person/person.component';
import { ValidNameDirective } from './directives/valid-name.directive';
import { ValidSequenceDirective } from './directives/valid-sequence.directive';


@NgModule({
  declarations: [
    MaterialsComponent,
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ClientsComponent,
    ClientRegistrationComponent,
    StaffRegistrationComponent,
    PersonComponent,
    DialogAddTool,
    ValidNameDirective,
    ValidSequenceDirective,

    
  ],
  entryComponents: [DialogAddTool],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatInputModule,
    BrowserAnimationsModule,

    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatStepperModule,
    MatGridListModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTableModule,
    MatIconModule,
    MatChipsModule,
    MatSnackBarModule

  ],
  providers: [
    {
      provide: LOCALE_ID, useValue: 'es'
    },
  ],
    
  bootstrap: [AppComponent]
})
export class AppModule { }
