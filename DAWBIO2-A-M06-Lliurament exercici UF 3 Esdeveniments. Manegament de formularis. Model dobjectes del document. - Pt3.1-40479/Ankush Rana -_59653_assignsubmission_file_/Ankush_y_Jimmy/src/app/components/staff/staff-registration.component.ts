/*  
  @name= staff-registration.component
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description=  Component (class) of Staff to register  staff data. 
  @date = 09-02-2020
*/
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Staff } from '../../modules/staff';
import { Address } from '../../modules/address';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-staff',
  templateUrl: './staff-registration.component.html',
  styleUrls: ['./staff-registration.component.css'],
  // DatePipe Formats a date value according to locale rules
  providers: [DatePipe]
})

export class StaffRegistrationComponent implements OnInit {

  title = "registration of staff member";

  //We use it on the date
  today: string;

  //We use it as a verifier to make way for the following form
  gotPerson: boolean = false;

  //staff_members_list initialized empty.
  staff_members_list: Staff[] = [];

  //we initialize a staff
  staff_member: Staff;

  // Roles list
  roles: string[] = ["manager", "doctor", "student", "worker", "other"];


  constructor(private datePipe: DatePipe, private _snackBar: MatSnackBar) {}


  ngOnInit() {
    //Initialize staff member
    this.initializedStaff();
    this.getStaffMembers();


  }

  /*  
     @name= initializedStaff()
     @author= Jimmy Tonato, Ankush Rana
     @version= 1.0
     @description= Method to initialize staff_member variable.
     @date = 09-02-2020
     @params= event: boolean
     @return = none
   */
  initializedStaff(){
    this.staff_member = new Staff();
    // this.datePipe.transform(new Date(),'yyyy-MM-dd');
    // With Angular material no need to transform
    this.staff_member.$hire_date = new Date();
    this.staff_member.$address = new Address()
  }

 /*  
     @name= getStaffMember()
     @author= Jimmy Tonato, Ankush Rana
     @version= 1.0
     @description= The Boolean is saved to make way for the following form
     @date = 09-02-2020
     @params= event: boolean
     @return = none
   */
  getStaffMember(event: boolean) {
    this.gotPerson = event;
  }

  /*  
     @name= getStaffMembers()
     @author= Jimmy Tonato, Ankush Rana
     @version= 1.0
     @description= This method is for create temporal staff list.
     @date = 09-02-2020
     @params= none
     @return = none
   */
  getStaffMembers() {

    //Temporal staff for staff_members_list
    const p1: Staff =
      new Staff(1, "Ankush", "Rana", "4578545D", 25, "+34632455789",
        new Address("Calle Alfonso", 10, null, "Barcelona", "08950"), "manager", 4, new Date());
    const p2: Staff =
      new Staff(2, "Steven", "Wong", "4578457M", 25, "+34343454543",
        new Address("Calle Alfonso", 10, null, "Barcelona", "08940"), "doctor", 4, new Date());
    const p3: Staff =
      new Staff(3, "Mario", "Alonso", "4343434L", 25, "+34895678956",
        new Address("Calle Alfonso", 10, null, "Barcelona", "08940"), "manager", 4, new Date());
    const p4: Staff =
      new Staff(4, "Jimmy", "Tonnato", "4578543R", 25, "+34546455444",
        new Address("Calle Alfonso", 10, null, "Barcelona", "08940"), "doctor", 4, new Date());


    this.staff_members_list.push(p1, p2, p3, p4);


  }
  /*  
   @name= managers()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= function that serves to filter the manager
   @date = 09-02-2020
   @params= none
   @return = none
 */
  managers = () => this.staff_members_list.filter(member => member.$role.toLowerCase() == "manager")
  

  /*  
   @name= onSubmit()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= We show the client inserted by the console by clicking 
   @date = 09-02-2020
   @params= none
   @return = none
 */
  onSubmit() {
    // Staff Member have analysis but not type, it will put to null the sequence. 
    //Because it means user doesn't wanted to enter the sequence.

    console.log(this.staff_member);
    this._snackBar.open("Client has been added successfully.\nPlease check the console.", "Ok", {
      duration: 2000,
    });

    this.staff_members_list.push(this.staff_member);

    // It got person variable will be false, and user go back to the form.
    this.gotPerson = false;
    //Initialize staff member
    this.initializedStaff();
  }

}
