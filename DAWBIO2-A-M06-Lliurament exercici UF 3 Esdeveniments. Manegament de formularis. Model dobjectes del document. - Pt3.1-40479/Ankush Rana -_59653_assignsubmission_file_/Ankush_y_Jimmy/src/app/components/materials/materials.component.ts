import { Component, OnInit, Inject } from '@angular/core';
import { Tool } from 'src/app/modules/tool';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.css']
})
export class MaterialsComponent implements OnInit {
  title = "Registration of staff member";

  tool: Tool;
  materialList: Tool[] = []

  constructor(private _matDialog: MatDialog, private _snackBar: MatSnackBar) { }

  // The Method work when the component  is initialized.
  ngOnInit() {
    this.tool = new Tool();
    this.getMaterial();
  }


  /*  
   @name= addMaterial()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This method work when user click on button to add material.
   @date = 09-02-2020
   @return = void
 */
  addMaterial() {

    //Open dialog box of DialogAddTool.html
    const dialogRef = this._matDialog.open(DialogAddTool, {
      width: '400px',
      height: '400px'
    });

    //When user close the dialog
    dialogRef.afterClosed().subscribe((result: Tool) => {
      if (result) {
        result.$id = this.getNextId();
        this.materialList.push(result);
        this.materialList.sort((a, b) => a.$id - b.$id);
        console.log(result);
        this._snackBar.open("Materia has been added successfully.\nPlease check the console.", "Ok", {
          duration: 2000,
        });
      }
    });
  }

  /*  
   @name= getNextId()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This function check the list of material and get list of id, after get get maximum number add 1 and return.
   @date = 09-02-2020
   @params= none
   @return = none
 */
  getNextId() {
    // Getting ids of all materials
    //Map function compare is serve to get ids.
    let arr = this.materialList.map((obj: Tool) => {
      return obj.$id;
    });

    let max = 0;
    if (arr.length > 0) {
      max = Math.max(...arr);
    }
    return max + 1;
  }


  getMaterial() {
    const m1 = new Tool(1, "Pipeta", true, 25);
    const m2 = new Tool(2, "Tubo de ensayo", false, 50);
    const m3 = new Tool(3, "Microscopio", true, 205);
    const m4 = new Tool(4, "Portaobjetos", true, 50);

    this.materialList.push(m1, m2, m3, m4);
  }
}



@Component({
  selector: 'dialog-add-tool',
  templateUrl: 'addToolForm.html',
})
export class DialogAddTool {
  data: Tool = new Tool();
  constructor(
    public dialogRef: MatDialogRef<DialogAddTool>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }


}