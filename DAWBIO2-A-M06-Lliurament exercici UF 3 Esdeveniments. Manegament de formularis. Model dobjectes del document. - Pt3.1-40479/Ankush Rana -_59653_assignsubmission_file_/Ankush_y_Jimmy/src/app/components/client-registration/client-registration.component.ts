/*  
  @name= ClientRegistrationComponent
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description=  Component (class) of client to register  client data. 
  @date = 09-02-2020
*/

import { Component, OnInit } from '@angular/core';

import { MatChipInputEvent, MatSnackBar, MatDialog } from '@angular/material';

// Event of comma and enter. It serves to detect if user enter coma or break line.
import { COMMA, ENTER } from '@angular/cdk/keycodes';

import { Client } from '../../modules/client';
import { Address } from '../../modules/address';
import { Analysis } from '../../modules/analysis';
import { MedicalData } from '../../modules/medical-data';

export const SequenceTypesBase: object = {
  protein: "ARNDCEQGHIULKMFPSTWYVO",
  adn: "ATCG",
  rna: "AUCG"
}

@Component({
  selector: 'app-client',
  templateUrl: './client-registration.component.html',
  styleUrls: ['./client-registration.component.css']
})


export class ClientRegistrationComponent implements OnInit {
  // This variable contains some third parties event, as list. 
  //It serves to detect if person push enter or insert a coma into the input. 

  separatorKeysCodes: number[] = [ENTER, COMMA];
  // Titles to show dynamically. (Title, subtile, titleInformation).
  title = "Registration of Client";
  subtitle = "Medical Information";
  titleInformation = "Information about diseases";

  //Variable serves to control if person is completed by user.
  gotPerson: boolean = false;
  client: Client
  clients_list: Client[] = [];

  //types
  typesSequence: string[] = ['adn', 'rna', 'protein'];

  //checkbox default
  checked: boolean = false;
  //disabled it's used in select and textarea

  //Is used this variable for the verification the textarea sequence
  SequenceTypesBase = SequenceTypesBase;


  // Mat Dialog to open dialogues whit other components.
  //Mat snackBar, to open a snack bar with message , bottom of the window.

  constructor(private _snackBar: MatSnackBar, private _matDialog: MatDialog) {

   }

  //It's initialized when the application is started 
  ngOnInit() {

    this.incializeClient();
    
  }

incializeClient(){

  //Creating new client and other variables necessary of client.
  this.client = new Client();
  this.client.$address = new Address();
  this.client.$medical_data = new MedicalData();
  this.client.$medical_data.$diseases = [];
  this.client.$medical_data.$analysis = new Analysis();
}


  /*  
    @name= getClient()
    @author= Jimmy Tonato, Ankush Rana
    @version= 1.0
    @description= This function is to change variable of gotPerson (boolean).
    @date = 09-02-2020
    @params= none
    @return = none
  */
  getClient(event: boolean) {
    this.gotPerson = event;
  }


  /*  
  @name= getPatentsList()
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description= This function serves to create temporal clients.
  @date = 09-02-2020
*/
  getPatentsList() {

    //Client temporals
    const client1: Client =
      new Client(1, "Jimmy", "Tonato", "172265", 13, "1231",
        new Address("princep", 2, "jordi", "barcelona", "08014"), "Dolores de cabeza y se tomo una pastilla",
        new MedicalData(new Analysis("adn", false, "gatgatgatgatagta"), ["dolor de cabeza"]));

    //Client temporals
    const client2: Client =
      new Client(2, "daniel", "tonato", "172263", 13, "1231",
        new Address("princep", 2, "jordi", "barcelona", "08014"), "Dolores de cabeza y se tomo una pastilla",
        new MedicalData(
          new Analysis("adn", false, "gatgatgatgatagta"), ["dolor de cabeza", "dolor de estomago"]));

    //Put to the list of clients list.
    this.clients_list.push(client1, client2)
  }

  /*  
   @name= trim()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This method receives a string and return it after trim.
   @date = 09-02-2020
   @params= string
   @return = string
 */
  trim(value) {
    //If the value is valid.
    if(value){
      return value.trim();
    }
    return value;
  }



  /*  
   @name= getNextId()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This function check the list of clients and get list of id, after get get maximum number add 1 and return.
   @date = 09-02-2020
   @params= none
   @return = none
 */
  getNextId() {
    // Getting ids of all clients
    //Map function compare is serve to get ids.
    let arr = this.clients_list.map((obj: Client) => {
      return obj.$id;
    });

    let max = 0;
    // If the list is no empty, max will be max number of id.
    if (arr.length > 0) {
      max = Math.max(...arr);
    }

    // Send max number + 1. 
    // If it was 0 , then send 1.
    return max + 1;
  }



  /*  
   @name= onSubmit()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= Function to submit clients.
   @date = 09-02-2020
   @params= none
   @return = none
 */
  onSubmit() {
    // Client have analysis but not type, it will put to null the sequence. 
    //Because it means user doesn't wanted to enter the sequence.
    if (!this.client.$medical_data.$analysis.$type && this.client.$medical_data.$analysis.$sequence) {
      this.client.$medical_data.$analysis.$sequence = null;
    }
    // Getting a new id for client and save into client's id attribute.
    this.client.$id = this.getNextId();

    //Show client from console log.
    console.log(this.client);

    
    //It opens a snack bar bottom of the window, with a message.
    this._snackBar.open("Client has been added successfully.\nPlease check the console.", "Ok", {
      //Duration of snack bar time 2 second
      duration: 2000,
    });

    // It got person variable will be false, and user go back to the form.
    this.gotPerson = false;
    this._matDialog.closeAll();
    
    this.incializeClient();
  }


  /*  
  @name= add()
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description= The method add the disease to the diseases list. 
  @date = 09-02-2020
  @params= disease
  @return = none
  */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Adding diseases, if value is none it will check empty string, with this i prevent 
    if ((value || '').trim()) {
      //Push value to diseases.
      this.client.$medical_data.$diseases.push(value.trim());
    }

    // Reset the input value. input will be empty
    if (input) {
      input.value = '';
    }
  }

  /*  
  @name= remove()
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description= The method removes the disease form the diseases list. 
  @date = 09-02-2020
  @params= disease
  @return = none
  */
  remove(disease: string): void {
    //Getting the index of disease in the list
    const index = this.client.$medical_data.$diseases.indexOf(disease);

    if (index >= 0) {
      //If any index found, it will remove value of this index, from the list.
      this.client.$medical_data.$diseases.splice(index, 1);
    }
  }


}
