import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  //Path for router link, if i need to add any other path, I just have to modify the list.
  links = [
    {
      path: "home",
      label: "home",
      icon: "home",
    },
    {
      path: "register/staff",
      label: "register staff member",
      icon: "person_add",
    },
    {
      path: "register/client",
      label: "register client",
      icon: "person_add",

    },
    {
      path: "clients",
      label: "clients",
      icon: "account_box",
    },
    {
      path: "materials",
      label: "materials",
      icon: "all_inbox",

    },

  ]
  constructor() { }

  ngOnInit() {
  }

}
