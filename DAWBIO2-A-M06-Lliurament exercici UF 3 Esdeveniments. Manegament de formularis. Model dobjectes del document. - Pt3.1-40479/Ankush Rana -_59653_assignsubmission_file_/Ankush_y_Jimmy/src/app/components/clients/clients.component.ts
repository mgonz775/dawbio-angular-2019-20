
/*  
 @name= ClientsComponent
 @author= Jimmy Tonato, Ankush Rana
 @version= 1.0
 @description= Component (class), this component is for show clients to user and , also can add.
 @date = 09-02-2020
 @params= none
 @return = none
 */
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ClientRegistrationComponent } from '../client-registration/client-registration.component';
import { MatDialog } from '@angular/material/dialog';
import { Client } from '../../modules/client';
import { Address } from '../../modules/address';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MedicalData } from '../../modules/medical-data';
import { Analysis } from '../../modules/analysis';


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css'],
  // Animation for animations table data, it's necessary to expand and minimize table rows.
  animations: [
    trigger('detailExpand', [
      //Min hight and width are 0, when collapsed
      state('collapsed', style({ height: '0', minHeight: '0' })),
      //Hight maybe whatever, depending on the data.
      state('expanded', style({ height: '*' })),
      //Time for expand and collapse rows, and animation theme (cubic-bezier)
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})

export class ClientsComponent implements OnInit {

  title = "Clients"


  //Clients_list initialized empty.
  clients_list: Client[] = [];

  //Clients_list initialized empty, it show clients to user, when he search.
  searchClients: Client[] = [];

  //Columns to show in table , when rows are collapsed.
  columnsToDisplay = ['dni', 'first_name', 'last_name'];

  //Table will get data depending on the class (module) Client
  //Client of null, if its not Client than it will be null and show nothing.
  expandedElement: Client | null;

  constructor(private _matDialog: MatDialog) { }

  ngOnInit() {
    //Getting data list, it means clients.
    this.getDataList();
    //SearchClients list will be same as clients list, because it will show all user until client search.
    this.searchClients = this.clients_list;

  }

  /*  
   @name= getDataList()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This method is for create temporal clients list.
   @date = 09-02-2020
   @params= none
   @return = none
 */
  getDataList() {
    //Temporal clients for clients_list
    const client1: Client =
      new Client(1, "Ione", "Correa", "1722654M", 13, "12343431",
        new Address("moli", 2, "jordi", "barcelona", "08014"), "Dolores de cabeza y se tomo una pastilla",
        new MedicalData(new Analysis("adn", false, "gatgatgatgatagta"), []));

    const client2: Client =
      new Client(1, "daniel", "tonato", "1722633R", 20, "134343231",
        new Address("rivera", 2, "jordi", "barcelona", "08014"), "Dolores de cabeza y se tomo una pastilla",
        new MedicalData(
          new Analysis("adn", true, "gatgatgatgatagta"), ["dolor de cabeza", "fiebre",]));
    const client3: Client =
      new Client(1, "Manuel", "Rivera", "4333263D", 30, "124423431",
        new Address("constancia", 2, "jordi", "barcelona", "08014"), "Dolores de cabeza y se tomo una pastilla",
        new MedicalData(
          new Analysis("rna", false, "agcuagcugucagucau"), ["fiebre", "resfriado"]));
    const client4: Client =
      new Client(1, "Michele", "Wong", "172263", 25, "43435544",
        new Address("princep", 2, "jordi", "barcelona", "08014"), null,
        new MedicalData(
          new Analysis(null, null, null), []));


    //Adding clients to the components
    this.clients_list.push(client1, client2, client3, client4);

  }

  /*  
   @name= getNextId()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This function check the list of clients and get list of id, after get get maximum number add 1 and return.
   @date = 09-02-2020
   @params= none
   @return = none
 */
  getNextId() {
    // Getting ids of all clients
    //Map function compare is serve to get ids.
    let arr = this.clients_list.map((obj: Client) => {
      return obj.$id;
    });

    // If the list is no empty, max will be max number of id.
    let max = 0;
    if (arr.length > 0) {
      max = Math.max(...arr);
    }
    return max + 1;
  }

  /*  
   @name= addMaterial()
   @author= Jimmy Tonato, Ankush Rana
   @version= 1.0
   @description= This method work when user click on button to add clients.
   @date = 09-02-2020
   @return = void
 */
  addClient() {
    
    //Opening the dialog to register clients
    this._matDialog.open(ClientRegistrationComponent, {
      height: '800px',
      width: '1000px',
      data: true,
    });
  }


  /*  
 @name= getDataList()
 @author= Jimmy Tonato, Ankush Rana
 @version= 1.0
 @description= This method is for search clients from clients list, searching by first name, last name, or DNI.
 @date = 09-02-2020
 @params= none
 @return = none
*/
  search(event) {
    var value = event.target.value;
    //Searching by different.
    this.searchClients = this.clients_list.filter(person =>
      person.$dni.toLowerCase().includes(value.toLowerCase()) || //by DNI
      person.$last_name.toLowerCase().includes(value.toLowerCase()) || // by last name
      person.$first_name.toLowerCase().includes(value.toLowerCase()) // by first name
    )

  }


}
