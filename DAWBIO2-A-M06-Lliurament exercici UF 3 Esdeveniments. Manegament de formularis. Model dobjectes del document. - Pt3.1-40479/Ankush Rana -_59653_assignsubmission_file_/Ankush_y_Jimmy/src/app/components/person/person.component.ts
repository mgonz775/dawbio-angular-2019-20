import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Person } from '../../modules/person';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  title = "Information";

  //we issue the data of the parent class
  @Output() doneSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();

  //to receive the data of the parent class
  @Input() person: Person;

  constructor() { }

  ngOnInit() { }

  /*  
    @name= onSubmit()
    @author= Jimmy Tonato, Ankush Rana
    @version= 1.0
    @description=Creates an event with true value and this in turn is picked 
                 up by the child
    @date = 09-02-2020
    @params= none
    @return = none
  */
  onSubmit() {
    this.doneSubmit.emit(true)
  }

  /*  
  @name= trim()
  @author= Jimmy Tonato, Ankush Rana
  @version= 1.0
  @description= function that removes the spaces of the past value 
  @date = 09-02-2020
  @params= value: string
  @return = value: string
*/
  trim(value: string) {
    if (value) {
      return value.trim();
    }
    return value;
  }
}
