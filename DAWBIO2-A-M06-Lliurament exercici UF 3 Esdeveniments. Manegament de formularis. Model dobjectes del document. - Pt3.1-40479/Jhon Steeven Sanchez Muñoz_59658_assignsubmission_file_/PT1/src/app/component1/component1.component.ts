import { Component, OnInit } from '@angular/core';

import { Person } from '../model/Person';
import { Habilities } from '../model/Habilities';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css']
})
export class Component1Component implements OnInit {
  //Properties
  person: Person;
  listPersons: Person[]=[];
  personSexAux: string[] = ["Men","Women","Other"];
  habilities: Habilities[] = [];
  toList: boolean = false;

  constructor() {
    

   }


  ngOnInit() {

    this.createPersonHabilities();

    this.person = new Person();
    this.person.habilities = new Habilities();

  }


  
  createPersonHabilities(){
    let habilitiesAux: string[] = ["Noob","Junior","Master"];
    let personHability: Habilities;

    for(let i:number = 0; i<habilitiesAux.length;i++){
      personHability = new Habilities(habilitiesAux[i]);
      this.habilities.push(personHability);
    }    
  }



  onSubmit(){
    this.listPersons.push(this.person);
    console.log(this.person);
    console.log(this.listPersons);
    
  }

}
