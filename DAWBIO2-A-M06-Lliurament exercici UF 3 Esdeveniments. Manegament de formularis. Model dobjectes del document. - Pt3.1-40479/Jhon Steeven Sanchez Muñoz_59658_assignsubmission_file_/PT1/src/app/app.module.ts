import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'; //test form
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';

registerLocaleData(localeES);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { Component3Component } from './component3/component3.component';
import { Component4Component } from './component4/component4.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const appRoutes: Routes = [
  {path: 'component1', component: Component1Component},
  {path: 'component2', component: Component2Component},
  {path: 'component3', component: Component3Component},
  {path: 'component4', component: Component4Component},
  {path: '', redirectTo: '/', pathMatch:'full'},
  {path: '**', redirectTo: '/component1', pathMatch:'full'},
  // {path: '**', component: PageNotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    Component1Component,
    Component2Component,
    Component3Component,
    Component4Component,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: false} // True for debugging purposes only
    )
  ],
  providers: [
    {provide: LOCALE_ID,
    useValue: 'es'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
