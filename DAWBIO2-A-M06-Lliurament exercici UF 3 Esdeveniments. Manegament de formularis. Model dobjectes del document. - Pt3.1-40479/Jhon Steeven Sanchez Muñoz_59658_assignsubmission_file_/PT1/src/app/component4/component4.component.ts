import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Materials } from "../model/Materials";

@Component({
  selector: "app-component4",
  templateUrl: "./component4.component.html",
  styleUrls: ["./component4.component.css"]
})
export class Component4Component implements OnInit {
  signupForm: FormGroup;

  msj: String = null;

  material: Materials = new Materials();

  constructor(private _builder: FormBuilder) {}

  ngOnInit() {

    //this.createItems();
    //this.createMachine();


    this.signupForm = this._builder.group({
      _items: ["", [Validators.required]],
      _itemsOthers: ["", [Validators.required]],
      _location: ["", [Validators.required]],
      _machines: ["", [Validators.required]],
      _machinesOthers: ["", [Validators.required]],
      _numberzone: ["", [Validators.required]],
      _help: ["", [Validators.required]]
    });
  }

  createItems(){
    let itemsAux: string[] = ["Test tubes","Rack","Rosary coolant","Crystalizer","Flask","Watch glass","Pizeta"];
    for(let i:number = 0; i<itemsAux.length;i++){
      Object.assign(this.material.items);
    }    
  }

  createMachine(){
    let machineAux: string[] = ["cryogenic storage","steam sterilizers","Ultrasonic Baths","Cabins of biosecurity","climate chambers"];
    for(let i:number = 0; i<machineAux.length;i++){
      Object.assign(this.material.machines);
    }    
  }

  onSubmit() {
    Object.assign(this.material, this.signupForm.value);
    console.log(this.material);

    this.msj = "Data recorded succesfully!";
  }
}
