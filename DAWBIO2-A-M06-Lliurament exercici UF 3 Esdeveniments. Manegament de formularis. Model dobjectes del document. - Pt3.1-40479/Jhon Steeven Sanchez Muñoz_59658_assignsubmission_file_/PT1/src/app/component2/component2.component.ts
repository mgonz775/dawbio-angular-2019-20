import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../model/Person';


@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {

  listFilas: String[]=["Name","Surname","Email","Phone","Sex","Habilities"];
  @Input()objeto: Person[] = [];


  constructor() { }

  ngOnInit() {
  }

}