export class Materials{
    private _items: String[];
    private _itemsOthers: String;
    private _location: String;
    private _machines: String[];
    private _machinesOthers: String;
    private _numberzone: number;
    private _help: String;



	constructor(items: String[]=null, itemsOthers: String=null, location: String=null, machines: String[]=null, machinesOthers: String=null, numberzone: number=null, help: String=null) {
		this._items = items;
		this._itemsOthers = itemsOthers;
		this._location = location;
		this._machines = machines;
		this._machinesOthers = machinesOthers;
		this._numberzone = numberzone;
		this._help = help;
	}

    /**
     * Getter items
     * @return {String[]}
     */
	public get items(): String[] {
		return this._items;
	}

    /**
     * Getter itemsOthers
     * @return {String}
     */
	public get itemsOthers(): String {
		return this._itemsOthers;
	}

    /**
     * Getter location
     * @return {String}
     */
	public get location(): String {
		return this._location;
	}

    /**
     * Getter machines
     * @return {String[]}
     */
	public get machines(): String[] {
		return this._machines;
	}

    /**
     * Getter machinesOthers
     * @return {String}
     */
	public get machinesOthers(): String {
		return this._machinesOthers;
	}

    /**
     * Getter numberzone
     * @return {number}
     */
	public get numberzone(): number {
		return this._numberzone;
	}

    /**
     * Getter help
     * @return {String}
     */
	public get help(): String {
		return this._help;
	}

    /**
     * Setter items
     * @param {String[]} value
     */
	public set items(value: String[]) {
		this._items = value;
	}

    /**
     * Setter itemsOthers
     * @param {String} value
     */
	public set itemsOthers(value: String) {
		this._itemsOthers = value;
	}

    /**
     * Setter location
     * @param {String} value
     */
	public set location(value: String) {
		this._location = value;
	}

    /**
     * Setter machines
     * @param {String[]} value
     */
	public set machines(value: String[]) {
		this._machines = value;
	}

    /**
     * Setter machinesOthers
     * @param {String} value
     */
	public set machinesOthers(value: String) {
		this._machinesOthers = value;
	}

    /**
     * Setter numberzone
     * @param {number} value
     */
	public set numberzone(value: number) {
		this._numberzone = value;
	}

    /**
     * Setter help
     * @param {String} value
     */
	public set help(value: String) {
		this._help = value;
	}
	
	
}