export class Worker{
    _nameworker: string;
    _nif: string;
    _namemanager: string;
    _hours : number;
    _time: string;
    _clothes: string;
    _level: string;


	constructor(nameworker: string=null, nif: string=null, namemanager: string=null, hours: number=null, time: string=null, clothes: string=null, level: string=null) {
		this._nameworker = nameworker;
		this._nif = nif;
		this._namemanager = namemanager;
		this._hours = hours;
		this._time = time;
		this._clothes = clothes;
		this._level = level;
	}

    /**
     * Getter nameworker
     * @return {string}
     */
	public get nameworker(): string {
		return this._nameworker;
	}

    /**
     * Getter nif
     * @return {string}
     */
	public get nif(): string {
		return this._nif;
	}

    /**
     * Getter namemanager
     * @return {string}
     */
	public get namemanager(): string {
		return this._namemanager;
	}

    /**
     * Getter hours
     * @return {number}
     */
	public get hours(): number {
		return this._hours;
	}

    /**
     * Getter time
     * @return {string}
     */
	public get time(): string {
		return this._time;
	}

    /**
     * Getter clothes
     * @return {string}
     */
	public get clothes(): string {
		return this._clothes;
	}

    /**
     * Getter level
     * @return {string}
     */
	public get level(): string {
		return this._level;
	}

    /**
     * Setter nameworker
     * @param {string} value
     */
	public set nameworker(value: string) {
		this._nameworker = value;
	}

    /**
     * Setter nif
     * @param {string} value
     */
	public set nif(value: string) {
		this._nif = value;
	}

    /**
     * Setter namemanager
     * @param {string} value
     */
	public set namemanager(value: string) {
		this._namemanager = value;
	}

    /**
     * Setter hours
     * @param {number} value
     */
	public set hours(value: number) {
		this._hours = value;
	}

    /**
     * Setter time
     * @param {string} value
     */
	public set time(value: string) {
		this._time = value;
	}

    /**
     * Setter clothes
     * @param {string} value
     */
	public set clothes(value: string) {
		this._clothes = value;
	}

    /**
     * Setter level
     * @param {string} value
     */
	public set level(value: string) {
		this._level = value;
	}


}