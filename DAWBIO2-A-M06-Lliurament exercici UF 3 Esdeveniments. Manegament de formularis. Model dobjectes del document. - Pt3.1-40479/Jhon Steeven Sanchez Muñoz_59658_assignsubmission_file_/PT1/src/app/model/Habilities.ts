export class Habilities{
    private hability: string;


	constructor($hability: string= null) {
		this.hability = $hability;
	}
    

    /**
     * Getter $hability
     * @return {string}
     */
	public get $hability(): string {
		return this.hability;
	}

    /**
     * Setter $hability
     * @param {string} value
     */
	public set $hability(value: string) {
		this.hability = value;
	}
    

}