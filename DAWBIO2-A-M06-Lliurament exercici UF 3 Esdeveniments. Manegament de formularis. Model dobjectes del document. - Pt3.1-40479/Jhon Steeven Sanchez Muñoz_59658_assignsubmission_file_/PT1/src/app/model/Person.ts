import { Habilities } from "./Habilities";

export class Person {
  private _name: string;
  private _surname: string;
  private _email: string;
  private _phone: string;
  private _sex: string;
  private _habilities: Habilities;

  constructor(
    name: string = null,
    surname: string = null,
    email: string = null,
    phone: string = null,
    sex: string = null,
    habilities: Habilities = null
  ) {
    this._name = name;
    this._surname = surname;
    this._email = email;
    this._phone = phone;
    this._sex = sex;
    this._habilities = habilities;
  }

  /**
   * Getter name
   * @return {string}
   */
  public get name(): string {
    return this._name;
  }

  /**
   * Getter surname
   * @return {string}
   */
  public get surname(): string {
    return this._surname;
  }

  /**
   * Getter email
   * @return {string}
   */
  public get email(): string {
    return this._email;
  }

  /**
   * Getter phone
   * @return {string}
   */
  public get phone(): string {
    return this._phone;
  }

  /**
   * Getter sex
   * @return {string}
   */
  public get sex(): string {
    return this._sex;
  }

  /**
   * Getter habilities
   * @return {Habilities}
   */
  public get habilities(): Habilities {
    return this._habilities;
  }

  /**
   * Setter name
   * @param {string} value
   */
  public set name(value: string) {
    this._name = value;
  }

  /**
   * Setter surname
   * @param {string} value
   */
  public set surname(value: string) {
    this._surname = value;
  }

  /**
   * Setter email
   * @param {string} value
   */
  public set email(value: string) {
    this._email = value;
  }

  /**
   * Setter phone
   * @param {string} value
   */
  public set phone(value: string) {
    this._phone = value;
  }

  /**
   * Setter sex
   * @param {string} value
   */
  public set sex(value: string) {
    this._sex = value;
  }

  /**
   * Setter habilities
   * @param {Habilities} value
   */
  public set habilities(value: Habilities) {
    this._habilities = value;
  }
}
