import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Worker } from "../model/Worker";

@Component({
  selector: "app-component3",
  templateUrl: "./component3.component.html",
  styleUrls: ["./component3.component.css"]
})

export class Component3Component implements OnInit {
  signupForm: FormGroup;

  msj: String = null;

  list: string[] = [
    "no",
    "glove",
    "lab coat + glove",
    "ABOVE + protective glasses"
  ];

  worker: Worker = new Worker();

  constructor(private _builder: FormBuilder) {}

  ngOnInit() {
    this.signupForm = this._builder.group({
      _nameworker: ["", [Validators.required, Validators.minLength(5)]],
      _nif: [ "", [Validators.required, Validators.minLength(9), Validators.pattern("d{8}[A-Za-z]")]],
      _namemanager: ["", [Validators.required, Validators.minLength(10)]],
      hours: [ "", [ Validators.required, Validators.maxLength(2), Validators.pattern("d{9}")]],
      _clothes: ["", [Validators.required]],
      level: ["", [Validators.required]],
      time: ["", [Validators.required]]
    });
  }


  onSubmit() {
    Object.assign(this.worker, this.signupForm.value);
    console.log(this.worker);

    this.msj = "Data recorded succesfully!";
  }
}
