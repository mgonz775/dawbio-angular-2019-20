import { Component, OnInit, ViewChild } from '@angular/core';
import { Reservation } from '../model/Reservation';
import {ReservationService} from '../services/reservation.service';


@Component({
  selector: 'app-reservation-management',
  templateUrl: './reservation-management.component.html',
  styleUrls: ['./reservation-management.component.css']
})
export class ReservationManagementComponent implements OnInit {

  reservation: Reservation;

  @ViewChild('reservationEntryForm', null) reservationEntryForm: //Accedeme a esta variable y pon el tipo HTMLFormElement.
  HTMLFormElement;

  constructor(private reservationService: ReservationService) {  }

  ngOnInit() {
    this.initializeForm();
  }

  reservationEntry(): void {
    console.log(this.reservation);
  }

  initializeForm(){
    this.reservationEntryForm.reset(); //(en teoria)Cada vez que le demos a recargar se borran los campos pero no las cookies.
    this.reservationEntryForm.form.markAsPristine();
    this.reservationEntryForm.form.markAsUntouched();

    this.reservation = new Reservation();
  }
}
