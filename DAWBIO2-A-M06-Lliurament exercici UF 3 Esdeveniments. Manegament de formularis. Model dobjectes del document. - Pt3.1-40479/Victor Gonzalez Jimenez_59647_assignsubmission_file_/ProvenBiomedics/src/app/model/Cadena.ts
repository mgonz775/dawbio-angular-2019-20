export class Cadena{
    private id: number;
    private cadena: string;


	constructor($id: number, $cadena: string) {
		this.id = $id;
		this.cadena = $cadena;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $cadena
     * @return {string}
     */
	public get $cadena(): string {
		return this.cadena;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $cadena
     * @param {string} value
     */
	public set $cadena(value: string) {
		this.cadena = value;
	}

}