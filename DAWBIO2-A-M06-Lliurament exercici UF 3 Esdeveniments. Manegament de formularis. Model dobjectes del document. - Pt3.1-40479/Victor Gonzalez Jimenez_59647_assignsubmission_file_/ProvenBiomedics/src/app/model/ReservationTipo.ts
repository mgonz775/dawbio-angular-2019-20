export class ReservationTipo {
    private id: number;
    private tipo: string;


	constructor($id: number, $tipo: string) {
		this.id = $id;
		this.tipo = $tipo;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $tipo
     * @return {string}
     */
	public get $tipo(): string {
		return this.tipo;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $tipo
     * @param {string} value
     */
	public set $tipo(value: string) {
		this.tipo = value;
	}


}