import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationTime } from '../model/reservationTime';
import { TablePreference } from '../model/tablePreference';
import { SpecialRequests } from '../model/specialRequests';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css']
})
export class ReservationEntryComponent implements OnInit {

  //Properties
  objReservation: Reservation;
  recommendations: String[][]=[["Restaurants", "Rates"],["El celler", "***"],["Diverxo","**"]];
  reservationTimes: ReservationTime[]=[];
  tablePreferences: TablePreference[]=[];
  specialRequests: SpecialRequests[]=[];
  
  constructor(private cookieService: CookieService) { 
    
  }

  ngOnInit() {
    this.createReservationTimes();
    this.createTablePreferences();
    this.createSpecialRequests();
    this.objReservation = new Reservation();
    this.objReservation.reservationDate = new Date();
    this.objReservation.reservationTime = this.reservationTimes[0];
    this.objReservation.tablePreference = this.tablePreferences[0];
    this.objReservation.specialRequests = [];
    this.objReservation.totalPrice = 18;
  }

  createReservationTimes(): void{
    let reservationTimesAux: string[]=["12:00","13:00","14:00","15:00"];

    for(let i:number=0; i<reservationTimesAux.length;i++){
      this.reservationTimes.push(new ReservationTime
          (i,reservationTimesAux[i]));
    }
  }

  createTablePreferences(){
    let tablePreferencesAux: string[]=["Next to the window",
    "Next to the door","Private room"];

    for(let i:number=0; i<tablePreferencesAux.length;i++){
      this.tablePreferences.push(new TablePreference
          (i,tablePreferencesAux[i], i*2+3));
    }
  }

  createSpecialRequests(): void{
    let specialRequestsAux: string[]=["Vegetarian menu",
    "Lactose intolerance","Celiac"];

    for(let i:number=0; i<specialRequestsAux.length;i++){
      this.specialRequests.push(new SpecialRequests
          (i,specialRequestsAux[i], i*3+1));
    }
  }

  addRemoveSpecialRequest(spReq: SpecialRequests): void{
    //indexOf() method returns the first index at which a given 
    //element can be found in the array, or -1 if it is not present.

    let myIndex: number = this.objReservation.specialRequests.indexOf(spReq);

    if(myIndex==-1){
      this.objReservation.specialRequests.push(spReq);
    }else{
      this.objReservation.specialRequests.splice(myIndex,1);
    }

    this.calculateTotalPrice();
  }

  calculateTotalPrice(){
    this.objReservation.totalPrice=15;

    for(let spReq of this.objReservation.specialRequests){
      this.objReservation.totalPrice += spReq.$price;
    }

    this.objReservation.totalPrice += this.objReservation.tablePreference.$price;

  }

  reservationEntry(): void{
    this.cookieService.set("reservation", JSON.stringify(this.objReservation));
    console.log(this.objReservation);

  }
}
