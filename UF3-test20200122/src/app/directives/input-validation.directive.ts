import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl} 
  from '@angular/forms';

@Directive({
  selector: '[inputMinLength]',
  providers: [{provide: NG_VALIDATORS, 
    useExisting: InputValidationDirective,
    multi: true }]
})
export class InputValidationDirective implements Validator{

  constructor() { }
  
  /**
     * @description
     * Method that performs synchronous validation against the provided control.
     *
     * @param control The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */
  validate(formFieldToValidate: AbstractControl): {[key:string]: any}{
    let validInput: boolean = false;

    if(formFieldToValidate && formFieldToValidate.value 
      && formFieldToValidate.value.length > 6){
      validInput = true;
    }

    return validInput? null: {'isNotCorrect':true};
  }
}
